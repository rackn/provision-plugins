package utils

import (
	"archive/zip"
	"github.com/klauspost/compress/zstd"
	"io"
	"io/fs"
	"log"
	"os"
	"strings"
)

func Package(target, src string) error {
	os.Remove(target)
	fi, err := os.Create(target)
	if err != nil {
		log.Fatalf("Error creating %s: %v", target, err)
	}
	defer fi.Close()
	zw := zip.NewWriter(fi)
	zw.RegisterCompressor(zstd.ZipMethodWinZip, zstd.ZipCompressor(zstd.WithEncoderLevel(zstd.SpeedBestCompression)))
	srcFS := os.DirFS(src)
	err = fs.WalkDir(srcFS, ".", func(p string, ent fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if ent.Type().IsRegular() {
			zh := &zip.FileHeader{
				Name:   strings.TrimPrefix(p, "./"),
				Method: zstd.ZipMethodWinZip,
			}
			mode, _ := ent.Info()
			zh.Modified = mode.ModTime()
			zh.SetMode(mode.Mode())
			tc, tcErr := srcFS.Open(p)
			if tcErr != nil {
				return tcErr
			}
			defer tc.Close()
			w, wErr := zw.CreateHeader(zh)
			if wErr != nil {
				return wErr
			}
			if _, cpErr := io.Copy(w, tc); cpErr != nil {
				return cpErr
			}
		}
		return nil
	})
	if err == nil {
		err = zw.Close()
	}
	return err
}
