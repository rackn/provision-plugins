package utils

import (
	"sync"
	"time"

	"gitlab.com/rackn/provision/v4/models"
)

const (
	CleanupInterval = 24 * time.Hour
)

type secret struct {
	Value     interface{}
	ExpiresAt int64
}

type Cache struct {
	secretLookup map[string]map[string]*secret
	mu           *sync.Mutex

	secretTtl time.Duration

	cleaner *time.Ticker
	done    chan bool
	getData func(string, string) (interface{}, *models.Error)
}

func CreateCache(secretTtl time.Duration, getData func(string, string) (interface{}, *models.Error)) *Cache {
	cache := &Cache{
		secretTtl:    secretTtl,
		secretLookup: map[string]map[string]*secret{},
		mu:           &sync.Mutex{},
		cleaner:      time.NewTicker(CleanupInterval),
		done:         make(chan bool),
		getData:      getData,
	}
	// Start cleaner
	cache.startCleaner()

	return cache
}

// Assumes locket is held
func (c *Cache) writeSecret(machineId string, lookupUri string, value interface{}, ttl time.Duration) {
	// time now
	now := time.Now()

	// check to see if machine exists in the cache already
	machineMap, ok := c.secretLookup[machineId]
	if !ok {
		secretMap := map[string]*secret{}
		secretMap[lookupUri] = &secret{Value: value, ExpiresAt: now.Add(ttl).Unix()}
		c.secretLookup[machineId] = secretMap
	} else {
		machineMap[lookupUri] = &secret{
			Value:     value,
			ExpiresAt: now.Add(ttl).Unix(),
		}
	}
}

func (c *Cache) Get(machineId string, lookupUri string) (interface{}, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	now := time.Now().Unix()
	var r interface{}
	it, ok := c.secretLookup[machineId][lookupUri]
	if !ok || ok && now >= it.ExpiresAt {
		response, err := c.getData(lookupUri, machineId)
		if err != nil {
			return nil, err
		}
		// write secret
		c.writeSecret(machineId, lookupUri, response, c.secretTtl)
		r = response
	} else {
		r = it.Value
	}
	return r, nil
}

func (c *Cache) startCleaner() {
	go func() {
		for {
			select {
			case <-c.done:
				return
			case <-c.cleaner.C:
				c.clean()
			}
		}
	}()
}

// clean: cleans up the expired secrets from the map
func (c *Cache) clean() {
	// current time
	now := time.Now().Unix()

	// lock for cleanup
	c.mu.Lock()
	defer c.mu.Unlock()

	// iterating over the secrets
	for uuid, secretMap := range c.secretLookup {
		for uri, secret := range secretMap {
			if now >= secret.ExpiresAt {
				delete(secretMap, uri)
			}
		}
		if len(secretMap) == 0 {
			delete(c.secretLookup, uuid)
		}
	}
}

// Empty: empties the contents of the cache
func (c *Cache) Empty() {
	// lock for cleanup
	c.mu.Lock()
	defer c.mu.Unlock()

	// iterating over the secrets
	for key, _ := range c.secretLookup {
		delete(c.secretLookup, key)
	}
}

func (c *Cache) ClearMachineCache(machineId string) {
	// lock for cleanup
	c.mu.Lock()
	defer c.mu.Unlock()

	// delete all secrets for th
	for uuid, _ := range c.secretLookup {
		if uuid == machineId {
			delete(c.secretLookup, uuid)
		}
	}
}
