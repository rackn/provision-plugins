package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml Version=$version"
//go:generate sh -c "drpcli contents document content.yaml > vault.rst"

import (
	_ "embed"
	"fmt"
	"os"
	"time"

	utils2 "github.com/VictorLowther/jsonpatch2/utils"
	vaultapi "github.com/hashicorp/vault/api"
	"gitlab.com/rackn/logger"
	v4 "gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

const (
	VaultV2PathPrefix   = "secret/data"
	VaultV1PathPrefix   = "kv"
	DefaultCacheTimeout = 5 * time.Minute
	VaultKvVersion1     = "v1"
	VaultKvVersion2     = "v2"
)

var (
	//go:embed content.yaml
	content string
	version = v4.RSVersion
	def     = models.PluginProvider{
		Name:          "vault",
		Version:       version,
		AutoStart:     true,
		PluginVersion: 4,
		AvailableActions: []models.AvailableAction{
			{Command: "decrypt",
				Model: "machines",
				RequiredParams: []string{
					"decrypt/lookup-uri",
					"vault/address",
					"vault/token",
				},
				OptionalParams: []string{
					"vault/cache-timeout",
					"vault/kv-version",
				},
			},
			{Command: "clearMachineCache",
				Model: "machines",
			},
			{Command: "clearCache",
				Model: "system",
			},
		},
		Content: content,
	}
)

type Plugin struct {
	// client represents the vault API client used to interact with vault
	client *vaultapi.Client

	// vaultToken represents a unique token that will be used to authenticate
	// to the vault instance
	vaultToken string

	// vaultAddr represents the dddress of the Vault server expressed as a URL and
	// port. for example: https://127.0.0.1:8200/
	vaultAddr string

	// vaultVersion represents the Vault KV secrets engine while running in versioned mode
	// can be either v1 or v2. defaults to v2
	vaultVersion string

	// cache represents the temporary vault cache
	cache *utils.Cache

	// cacheTimeout represents the amount of time for which the value retrieved
	// from vault would be cached in memory
	cacheTimeout time.Duration
}

// VaultOptions is a set of options that can be passed in as
// query parameters into the lookup URI
type VaultOptions struct {
	// format is the format in which the secret is stored
	// for example the lookup uri could be
	// vault://key-to-lookup?path=path-to-secret&format=<format>
	// this would mean the secret is stored as json and the plugin
	// would read it and convert it before returning the secret
	// default which nothing is passed is just `string`
	format string

	// path denotes the path to the secret
	// it should not include `/secret` in it
	// so if the secret is stored at `/secret/foo/creds` then you
	// would just pass in `/foo/creds`
	// example: vault://key-to-lookup?path=<path>
	path string

	// key denotes the key to lookup at the location (determined by path)
	// example: vault://<key>?path=path-to-secret
	key string
}

func (p *Plugin) Config(l logger.Logger, session *api.Client, config map[string]interface{}) *models.Error {
	err := &models.Error{Type: "plugin", Model: "", Key: "vault"}
	var ok bool
	p.vaultToken, ok = config["vault/token"].(string)
	if !ok {
		err.Code = 400
		err.Errorf("Plugin %s is missing vault/token", p.vaultToken)
		return err
	}
	p.vaultAddr, ok = config["vault/address"].(string)
	if !ok {
		err.Code = 400
		err.Errorf("Plugin %s is missing vault/address", p.vaultAddr)
		return err
	}
	timeoutSeconds, ok := config["vault/cache-timeout"].(int)
	if !ok {
		p.cacheTimeout = DefaultCacheTimeout
	} else {
		p.cacheTimeout = time.Duration(timeoutSeconds) * time.Second
	}
	p.vaultVersion, ok = config["vault/kv-version"].(string)
	if !ok {
		p.vaultVersion = VaultKvVersion2
	}

	vaultConfig := &vaultapi.Config{
		Address: p.vaultAddr,
	}
	var verr error
	p.client, verr = vaultapi.NewClient(vaultConfig)
	if verr != nil {
		l.Errorf(verr.Error())
		err.Errorf(verr.Error())
		return err
	}
	p.client.SetToken(p.vaultToken)

	// Set up Cache
	p.cache = utils.CreateCache(p.cacheTimeout, p.fetchSecretFromVault)

	// Validate the token
	// Making a call to get a "" path basically
	// Should not error out
	_, verr = p.client.Logical().Read(VaultV2PathPrefix)
	if verr != nil {
		err.Code = 400
		err.Errorf("Invalid vault configuration. Please check vault address and token.")
		return err
	}

	return nil
}

func (p *Plugin) Action(l logger.Logger, ma *models.Action) (answer interface{}, err *models.Error) {
	switch ma.Command {
	case "decrypt":
		answer, err = p.decryptAction(l, ma.Model, ma.Params)
	case "clearMachineCache":
		err = p.clearMachineCache(l, ma.Model)
	case "clearCache":
		err = p.clearCache(l)
	default:
		err = &models.Error{Code: 404,
			Model:    "Plugin",
			Key:      "vault",
			Type:     "rpc",
			Messages: []string{fmt.Sprintf("Unknown command: %s", ma.Command)}}
	}
	return
}

func (p *Plugin) decryptAction(l logger.Logger, model interface{}, params map[string]interface{}) (interface{}, *models.Error) {
	lookupUri := utils.GetParamOrString(params, "decrypt/lookup-uri", "")
	l.Infof("Decrypting vault secret")

	// Get the machine details
	machine := &models.Machine{}
	machine.Fill()
	if err := utils2.Remarshal(model, &machine); err != nil {
		return "", utils.ConvertError(400, err)
	}

	// Check the cache
	secret, err := p.cache.Get(machine.Uuid.String(), lookupUri)
	if err != nil {
		return nil, utils.ConvertError(500, err)
	}

	return secret, nil
}

func (p *Plugin) clearMachineCache(l logger.Logger, model interface{}) *models.Error {
	l.Infof("Clearing machine cache")
	// Grab the machine
	machine := &models.Machine{}
	machine.Fill()
	if err := utils2.Remarshal(model, &machine); err != nil {
		return utils.ConvertError(400, err)
	}

	// clear all machine data from cache
	p.cache.ClearMachineCache(machine.UUID())

	return nil
}

func (p *Plugin) clearCache(l logger.Logger) *models.Error {
	l.Infof("Clearing system cache")

	// clear all machine data from cache
	p.cache.Empty()

	return nil
}

func main() {
	plugin.InitApp("vault", "Retrieve secret from vault", version, &def, &Plugin{})
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
