package main

import (
	"encoding/json"
	"net/url"
	"path"

	"gitlab.com/rackn/provision/v4/models"
)

func (p *Plugin) getVaultPrefix() string {
	if p.vaultVersion == VaultKvVersion1 {
		return VaultV1PathPrefix
	} else {
		return VaultV2PathPrefix
	}
}

func (p *Plugin) isKvVersion1() bool {
	return p.vaultVersion == VaultKvVersion1
}

func (p *Plugin) fetchSecretFromVault(lookupUri string, machineId string) (interface{}, *models.Error) {
	err := &models.Error{Type: "plugin", Model: "", Key: "vault"}

	// Parse the lookup URI and get the scheme and host
	// example: "plugin-name://secret-key?path=path-to-secret"
	uri, verr := url.ParseRequestURI(lookupUri)
	if verr != nil {
		err.Code = 400
		err.Errorf("fetch secret: invalid lookupUri provided")
		return "", err
	}

	var vault VaultOptions
	vault.key = uri.Host + uri.Path
	vault.path = path.Join(p.getVaultPrefix(), uri.Query()["path"][0])

	if uri.Query()["format"] != nil {
		vault.format = uri.Query()["format"][0]
	}

	secret, verr := p.client.Logical().Read(vault.path)
	if verr != nil {
		err.Code = 500
		err.Errorf(err.Error())
		return "", err
	}

	if secret == nil {
		err.Code = 404
		err.Errorf("Secret %s not found in vault. Check the value of vault/kv-version.", uri.Query()["path"][0])
		return "", err
	}

	var vaultResult string
	var ok bool
	if p.isKvVersion1() {
		vaultResult, ok = secret.Data[vault.key].(string)
		if !ok {
			err.Code = 400
			err.Errorf("fetch secret: key %s does not exist in v1 kv engine", vault.key)
			return "", err
		}
	} else {
		m, v2ok := secret.Data["data"].(map[string]interface{})
		if !v2ok {
			err.Code = 400
			err.Errorf("fetch secret: unable to fetch secret data")
			return "", err
		}

		vaultResult, ok = m[vault.key].(string)
		if !ok {
			err.Code = 400
			err.Errorf("fetch secret: key %s does not exist in v2 kv engine", vault.key)
			return "", err
		}
	}

	var finalResult interface{}
	// If a format is specified, then we will also need convert data
	switch vault.format {
	case "json":
		verr = json.Unmarshal([]byte(vaultResult), &finalResult)
		if verr != nil {
			// There was an error converting the value to json, most like it is not valid json so just return
			return vaultResult, nil
		}
	default:
		finalResult = vaultResult
	}

	return finalResult, nil
}
