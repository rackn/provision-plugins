package main

import (
	"bufio"
	"bytes"
	"encoding/xml"
	"fmt"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/tftp/v3"
	"io"
	"log"
	"os"
	"path"
	"sort"
	"strconv"
	"strings"
	"time"
)

type loginReq struct {
	XMLName  xml.Name `xml:"aaaLogin"`
	Name     string   `xml:"inName,attr"`
	Password string   `xml:"inPassword,attr"`
}

type loginResp struct {
	XMLName  xml.Name `xml:"aaaLogin"`
	Response string   `xml:"response,attr"`
	Cookie   string   `xml:"outCookie,attr"`
	Refresh  int      `xml:"outRefreshPeriod,attr"`
	Priv     string   `xml:"outPriv,attr"`
}

type refreshReq struct {
	XMLName  xml.Name `xml:"aaaRefresh"`
	Cookie   string   `xml:"cookie,attr"`
	InCookie string   `xml:"inCookie,attr"`
	Name     string   `xml:"inName,attr"`
	Password string   `xml:"inPassword,attr"`
}

type refreshResp struct {
	XMLName  xml.Name `xml:"aaaRefresh"`
	Response string   `xml:"response,attr"`
	Cookie   string   `xml:"outCookie,attr"`
	Refresh  int      `xml:"outRefreshPeriod,attr"`
	Priv     string   `xml:"outPriv,attr"`
}

type logoutReq struct {
	XMLName  xml.Name `xml:"aaaLogout"`
	Cookie   string   `xml:"cookie,attr"`
	InCookie string   `xml:"inCookie,attr"`
}

type logoutResp struct {
	XMLName  xml.Name `xml:"aaaLogout"`
	Response string   `xml:"response,attr"`
	Cookie   string   `xml:"outCookie,attr"`
	Status   string   `xml:"outStatus,attr"`
}

type inCfg struct {
	XMLName xml.Name `xml:"inConfig"`
	Val     any      `xml:",any"`
}

type outCfg struct {
	XMLName xml.Name `xml:"outConfig"`
	Val     any      `xml:",any"`
}

type outCfgs struct {
	XMLName xml.Name `xml:"outConfigs"`
	Val     any      `xml:",any"`
}

type configResolveClass struct {
	XMLName      xml.Name `xml:"configResolveClass"`
	Cookie       string   `xml:"cookie,attr"`
	Hierarchical bool     `xml:"inHierarchical,attr,omitempty"`
	Response     string   `xml:"response,attr,omitempty"`
	ClassId      string   `xml:"classId,attr"`
	OutConfigs   *outCfgs `xml:"outConfigs,omitempty"`
}

type configOp struct {
	XMLName      xml.Name `xml:"configConfMo"`
	Cookie       string   `xml:"cookie,attr"`
	Hierarchical bool     `xml:"inHierarchical,attr,omitempty"`
	DN           string   `xml:"dn,attr,omitempty"`
	InConfig     *inCfg   `xml:"inConfig,omitempty"`
	OutConfig    *outCfg  `xml:"outConfig,omitempty"`
	Response     string   `xml:"response,omitempty"`
}

type mgmtOp struct {
	DN               string `xml:"dn,attr"`
	AdminState       string `xml:"adminState,attr"`
	Hostname         string `xml:"hostname,attr"`
	Proto            string `xml:"proto,attr,omitempty"`
	User             string `xml:"user,attr,omitempty"`
	Pass             string `xml:"pwd,attr,omitempty"`
	RemoteFile       string `xml:"remoteFile,attr"`
	Stage            string `xml:"fsmStageDescr,attr,omitempty"`
	RemoteErrorCode  string `xml:"fsmRmtInvErrCode,attr,omitempty"`
	RemoteErrorDescr string `xml:"fsmRmtInvErrDescr,attr,omitempty"`
	Status           string `xml:"status,attr,omitempty"`
	Passphrase       string `xml:"passphrase,attr"`
}

type mgmtImport struct {
	XMLName xml.Name `xml:"mgmtImporter"`
	mgmtOp
}

type mgmtBackup struct {
	XMLName xml.Name `xml:"mgmtBackup"`
	mgmtOp
}

type ciscoCimcConfig struct {
	auth
	source  io.ReadSeeker
	token   string
	timeout time.Time
	current map[string]Entry
}

var client = api.ProxyEnvClient(true)

func (c *ciscoCimcConfig) do(in, out any) error {
	inBuf, err := xml.Marshal(in)
	if err != nil {
		return err
	}
	resp, err := client.Post(
		"https://"+c.addr+"/nuova",
		"application/x-www-form-urlencoded",
		bytes.NewReader(inBuf))
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		log.Printf("Request was %s", string(inBuf))
		return err
	}
	buf, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if resp.StatusCode >= 300 {
		log.Printf("Request was %s", string(inBuf))
		log.Printf("Response was %s", string(buf))
		return fmt.Errorf("Bad status %s", resp.Status)
	}
	if err = xml.Unmarshal(buf, out); err != nil {
		log.Printf("Request was %s", string(inBuf))
		log.Printf("Response was %s", string(buf))
	}
	return err
}

func (c *ciscoCimcConfig) login() error {
	l := &loginReq{
		Name:     c.user,
		Password: c.pass,
	}
	r := &loginResp{}
	if err := c.do(l, r); err != nil {
		return err
	}
	c.token = r.Cookie
	c.timeout = time.Now().Add(time.Second * time.Duration(int64(r.Refresh/2)))
	return nil
}

func (c *ciscoCimcConfig) logout() error {
	l := &logoutReq{Cookie: c.token, InCookie: c.token}
	r := &logoutResp{}
	if err := c.do(l, r); err != nil {
		return err
	}
	c.token = ""
	return nil
}

func (c *ciscoCimcConfig) maybeRefresh(in, out any) error {
	if time.Now().Before(c.timeout) {
		return c.do(in, out)
	}
	if err := c.logout(); err != nil {
		return err

	}
	if err := c.login(); err != nil {
		return err
	}
	return c.do(in, out)
}

func (c *ciscoCimcConfig) SetCurrent(current map[string]Entry) {
	c.current = current
}

func (c *ciscoCimcConfig) Source(src io.ReadSeeker) {
	c.source = src
}

func (c *ciscoCimcConfig) checkPendingJobs() error {
	ib := &mgmtImport{}
	crr := &configResolveClass{Cookie: c.token, ClassId: "mgmtImporter"}
	crc := &configResolveClass{OutConfigs: &outCfgs{Val: ib}}
	if err := c.maybeRefresh(crr, crc); err != nil {
		return err
	}
	mb := &mgmtBackup{}
	crr = &configResolveClass{Cookie: c.token, ClassId: "mgmtBackup"}
	crc = &configResolveClass{OutConfigs: &outCfgs{Val: mb}}
	if err := c.maybeRefresh(crr, crc); err != nil {
		return err
	}
	return nil
}

func (c *ciscoCimcConfig) setRemote(fi *os.File) error {
	if err := c.oobOK(); err != nil {
		return err
	}
	log.Printf("Starting remote config import")
	tftpClient, err := tftp.NewClient(c.tftpServer)
	if err != nil {
		return err
	}
	if _, err := fi.Seek(0, io.SeekStart); err != nil {
		return err
	}
	remoteFile := path.Join("/jobs", c.job, "update.xml")
	tftpClient.SetBlockSize(9000)
	log.Printf("Uploading to %s on %s", remoteFile, c.tftpServer)
	target, err := tftpClient.Send(remoteFile, "octal")
	if err != nil {
		return err
	}
	if _, err = target.ReadFrom(fi); err != nil {
		return err
	}
	log.Printf("Config upload to %s on %s", remoteFile, c.tftpServer)
	im := &mgmtImport{}
	im.DN = "sys/import-config"
	im.AdminState = "enabled"
	im.Hostname = strings.SplitN(c.tftpServer, ":", 2)[0]
	im.Proto = "tftp"
	im.RemoteFile = remoteFile
	im.Passphrase = "passphrase"
	op := &configOp{
		Cookie:       c.token,
		Hierarchical: false,
		DN:           im.DN,
		InConfig:     &inCfg{Val: im},
	}
	ib := &mgmtImport{}
	outOp := &configOp{OutConfig: &outCfg{Val: ib}}
	if err := c.maybeRefresh(op, outOp); err != nil {
		return err
	}
	log.Printf("Waiting for job to finish")
	finished := false
	for tries := 240; ib != nil && tries >= 0; tries-- {
		time.Sleep(5 * time.Second)
		ib = &mgmtImport{}
		crr := &configResolveClass{Cookie: c.token, ClassId: "mgmtImporter"}
		crc := &configResolveClass{OutConfigs: &outCfgs{Val: ib}}
		if err := c.maybeRefresh(crr, crc); err != nil {
			return err
		}
		log.Printf("import status: %s", ib.Status)
		log.Printf("import stage: %s", ib.Stage)
		log.Printf("import remote status: %s", ib.RemoteErrorDescr)
		if ib.Stage == "Completed successfully" {
			finished = true
			break
		}
	}
	if !finished {
		return fmt.Errorf("Import not completed after %d seconds, giving up", 120*5)
	}
	return nil
}

func (c *ciscoCimcConfig) remoteCfg() (io.ReadSeekCloser, error) {
	if err := c.oobOK(); err != nil {
		return nil, err
	}
	bk := &mgmtBackup{}
	bk.DN = "sys/export-config"
	bk.AdminState = "enabled"
	bk.Hostname = strings.SplitN(c.tftpServer, ":", 2)[0]
	bk.Proto = "tftp"
	bk.RemoteFile = path.Join("/jobs", c.job, "config.xml")
	bk.Passphrase = "passphrase"
	log.Printf("Starting remote config backup from %s to %s on %s", c.addr, bk.RemoteFile, bk.Hostname)
	op := &configOp{
		Cookie:       c.token,
		Hierarchical: false,
		DN:           bk.DN,
		InConfig:     &inCfg{Val: bk},
	}
	mb := &mgmtBackup{}
	out := &configOp{OutConfig: &outCfg{Val: mb}}
	if err := c.maybeRefresh(op, out); err != nil {
		return nil, err
	}
	log.Printf("Waiting for job to finish")
	finished := false
	for tries := 60; mb != nil && tries >= 0; tries-- {
		time.Sleep(5 * time.Second)
		mb = &mgmtBackup{}
		crr := &configResolveClass{Cookie: c.token, ClassId: "mgmtBackup"}
		crc := &configResolveClass{OutConfigs: &outCfgs{Val: mb}}
		if err := c.maybeRefresh(crr, crc); err != nil {
			return nil, err
		}
		log.Printf("backup status: %s", mb.Status)
		log.Printf("backup stage: %s", mb.Stage)
		log.Printf("backup remote status: %s", mb.RemoteErrorDescr)
		if mb.Stage == "Completed successfully" {
			finished = true
			break
		}
	}
	if !finished {
		return nil, fmt.Errorf("Backup not taken after %d seconds, giving up", 30*5)
	}
	log.Printf("Backup taken, fetching from %s on %s", bk.RemoteFile, c.tftpServer)
	tftpClient, err := tftp.NewClient(c.tftpServer)
	if err != nil {
		return nil, err
	}
	tftpClient.SetBlockSize(9000)
	fi, err := os.Create("backup.xml")
	if err != nil {
		return nil, err
	}
	src, err := tftpClient.Receive(bk.RemoteFile, "octal")
	if err != nil {
		return nil, err
	}
	if _, err := src.WriteTo(fi); err != nil {
		return nil, err
	}
	if _, err := fi.Seek(0, io.SeekStart); err != nil {
		return nil, err
	}
	log.Printf("Config backup downloaded from %s on %s to backup.xml", bk.RemoteFile, c.tftpServer)
	log.Printf("Sleeping for 30 seconds to let CIMC settle")
	time.Sleep(30 * time.Second)
	return fi, nil
}

type nm struct {
	idx  int
	name string
	subs []nm
	leaf bool
}

func (n *nm) get(parts ...string) (int, bool) {
	for i := range n.subs {
		if n.subs[i].name != parts[0] {
			continue
		}
		return (&n.subs[i]).get(parts[1:]...)
	}
	if n.leaf && len(parts) == 0 {
		return n.idx, true
	}
	return 0, false
}

type ciscoBackup struct {
	doc     []xml.Token
	nameMap nm
	setMap  map[int]string
}

func (c *ciscoBackup) allNames() (names []string) {
	var isLeaf func(*nm, ...string)
	isLeaf = func(n *nm, parts ...string) {
		parts = append(parts, n.name)
		if n.leaf {
			names = append(names, path.Join(parts...))
		}
		for i := range n.subs {
			isLeaf(&n.subs[i], parts...)
		}
	}
	for i := range c.nameMap.subs {
		isLeaf(&c.nameMap.subs[i])
	}
	return
}

func (c *ciscoBackup) validLeaf(idx int) bool {
	idx++
	for {
		switch c.doc[idx].(type) {
		case xml.EndElement:
			return true
		case xml.CharData:
			idx++
		default:
			return false
		}
	}
}

func (c *ciscoBackup) get(k string) (string, bool) {
	idx, ok := c.nameMap.get(strings.Split(k, "/")...)
	if !(ok && c.validLeaf(idx)) {
		return "", false
	}
	if v, ok := c.setMap[idx]; ok {
		return v, true
	}
	idx++
	var res []byte
	for {
		switch v := c.doc[idx].(type) {
		case xml.EndElement:
			return string(res), true
		case xml.CharData:
			res = append(res, v...)
			idx++
		}
	}
}

func (c *ciscoBackup) set(k, v string) {
	if idx, ok := c.nameMap.get(strings.Split(k, "/")...); ok {
		c.setMap[idx] = v
	}
}

func (c *ciscoBackup) mapNames(to *nm, start int) int {
	switch c.doc[start].(type) {
	case xml.StartElement:
	default:
		log.Panicf("element at %d not a start token!", start)
	}

	for i := start + 1; ; i++ {
		elem := c.doc[i]
		switch v := elem.(type) {
		case xml.EndElement:
			to.leaf = c.validLeaf(start)
			names := map[string][]int{}
			for j := range to.subs {
				names[to.subs[j].name] = append(names[to.subs[j].name], j)
			}
			for _, jj := range names {
				if len(jj) == 1 {
					continue
				}
				for j := range jj {
					to.subs[jj[j]].name = to.subs[jj[j]].name + "[" + strconv.Itoa(j+1) + "]"
				}
			}
			return i
		case xml.StartElement:
			to.subs = append(to.subs, nm{idx: i, name: v.Name.Local})
			i = c.mapNames(&to.subs[len(to.subs)-1], i)
		}
	}
}

func (c *ciscoCimcConfig) decodeConfig() (*ciscoBackup, error) {
	var err error
	if c.source == nil {
		c.source, err = c.remoteCfg()
	}
	if err != nil {
		return nil, err
	}
	defer c.source.Seek(0, io.SeekStart)
	res := &ciscoBackup{nameMap: nm{}, setMap: map[int]string{}}
	dec := xml.NewDecoder(c.source)
	for {
		var tok xml.Token
		tok, err = dec.Token()
		if err != nil {
			break
		}
		res.doc = append(res.doc, xml.CopyToken(tok))
	}
	if err != io.EOF {
		return nil, err
	}
	res.mapNames(&res.nameMap, 0)
	return res, nil
}

func (c *ciscoCimcConfig) Current() (map[string]Entry, error) {
	if c.current != nil {
		return c.current, nil
	}
	if c.source == nil {
		if err := c.login(); err != nil {
			return nil, err
		}
		defer c.logout()
	}
	backup, err := c.decodeConfig()
	if err != nil {
		return nil, err
	}
	c.current = map[string]Entry{}
	for _, k := range backup.allNames() {
		v, _ := backup.get(k)
		c.current[k] = Entry{Current: v}
	}
	return c.current, nil
}

func (c *ciscoCimcConfig) Apply(current map[string]Entry, trimmed map[string]string, dryRun bool) (needReboot bool, err error) {
	if c.source == nil || !dryRun {
		if err := c.login(); err != nil {
			return false, err
		}
		defer c.logout()
	}
	var backup *ciscoBackup
	backup, err = c.decodeConfig()
	if err != nil {
		return
	}
	for k, v := range trimmed {
		backup.set(k, v)
	}
	if len(backup.setMap) == 0 {
		log.Printf("No settings changed, skipping API phase")
		return
	}
	toSet := make([]int, 0, len(backup.setMap))
	for i := range backup.setMap {
		toSet = append(toSet, i)
	}
	sort.Ints(toSet)
	for i := len(toSet) - 1; i >= 0; i-- {
		idx := toSet[i] + 1
		endIdx := idx
		for {
			if _, ok := backup.doc[endIdx].(xml.EndElement); ok {
				break
			}
			endIdx++
		}
		newTail := []xml.Token{xml.CharData([]byte(backup.setMap[idx-1]))}
		newTail = append(newTail, backup.doc[endIdx:]...)
		backup.doc = append(backup.doc[:idx], newTail...)
	}
	// TODO implement trying to apply the config
	log.Printf("Apply marshalling changed XML to cisco-config.xml")
	var fi *os.File
	fi, err = os.Create("cisco-config.xml")
	if err != nil {
		return
	}
	defer fi.Close()
	log.Printf("Encoding new XML file")
	buf := &bytes.Buffer{}
	enc := xml.NewEncoder(buf)
	for i := range backup.doc {
		if err = enc.EncodeToken(backup.doc[i]); err != nil {
			return
		}
	}
	if err = enc.Flush(); err != nil {
		return
	}
	bw := bufio.NewWriter(fi)
	sc := bufio.NewScanner(bytes.NewReader(buf.Bytes()))
	for sc.Scan() {
		line := sc.Bytes()
		if bytes.HasPrefix(bytes.TrimSpace(line), []byte("<?")) {
			line[bytes.LastIndex(line, []byte(" "))] = byte('\n')
		}
		if _, err = bw.Write(line); err != nil {
			return
		}
		bw.WriteByte('\n')
	}
	if err = bw.Flush(); err != nil {
		return
	}
	log.Printf("XML encoded and saved to cisco-config.xml")
	if !dryRun {
		err = c.setRemote(fi)
	}
	return
}

func (c *ciscoCimcConfig) FixWanted(wanted map[string]string) map[string]string {
	return wanted
}
