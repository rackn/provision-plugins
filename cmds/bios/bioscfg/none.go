package main

import "io"

type noneConfig struct{ auth }

func (n *noneConfig) Source(r io.ReadSeeker) {}

func (n *noneConfig) Current() (res map[string]Entry, err error) {
	res = map[string]Entry{}
	return
}

func (n *noneConfig) SetCurrent(_ map[string]Entry) {
	return
}

func (n *noneConfig) FixWanted(wanted map[string]string) map[string]string {
	return wanted
}

func (n *noneConfig) Apply(current map[string]Entry, trimmed map[string]string, b bool) (needReboot bool, err error) {
	return
}
