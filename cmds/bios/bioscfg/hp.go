package main

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
)

type hpConfigEnt struct {
	XMLName xml.Name `xml:"Section"`
	Name    string   `xml:"name,attr"`
	Value   string   `xml:",innerxml"`
}

type hpConfig struct {
	auth
	current map[string]Entry
	source  io.ReadSeeker
	XMLName xml.Name      `xml:"Conrep"`
	Ents    []hpConfigEnt `xml:",any"`
}

func (h *hpConfig) SetCurrent(current map[string]Entry) {
	h.current = current
}

func (h *hpConfig) Source(src io.ReadSeeker) {
	h.source = src
}

func (h *hpConfig) Current() (res map[string]Entry, err error) {
	if h.current != nil {
		res = h.current
		return
	}
	if h.oobOK() == nil {
		return nil, fmt.Errorf("Out-of-band access not supported")
	}
	res = map[string]Entry{}
	if h.source == nil {
		cmd := exec.Command("conrep", "-s")
		var out []byte
		out, err = cmd.CombinedOutput()
		os.Stderr.Write(out)
		if err != nil {
			return
		}
		if !cmd.ProcessState.Success() {
			err = errors.New("Error running conrep")
			return
		}
		var fi *os.File
		fi, err = os.Open("conrep.dat")
		if err != nil {
			return
		}
		h.source = fi
	}
	defer h.source.Seek(0, io.SeekStart)
	dec := xml.NewDecoder(h.source)
	cfg := hpConfig{}
	if err = dec.Decode(&cfg); err != nil {
		return
	}
	for i := range cfg.Ents {
		ent := cfg.Ents[i]
		res[ent.Name] = Entry{
			Name:    ent.Name,
			Current: ent.Value,
		}
	}
	if err == nil {
		h.current = res
	}
	return
}

func (h *hpConfig) FixWanted(wanted map[string]string) map[string]string {
	return wanted
}

func (h *hpConfig) Apply(current map[string]Entry, trimmed map[string]string, dryRun bool) (needReboot bool, err error) {
	if h.oobOK() == nil {
		return false, fmt.Errorf("Out-of-band access not supported")
	}
	toAdd := &hpConfig{
		Ents: make([]hpConfigEnt, 0, len(trimmed)),
	}
	for k, v := range trimmed {
		toAdd.Ents = append(toAdd.Ents, hpConfigEnt{Name: k, Value: v})
	}
	var fi *os.File
	fi, err = os.Create("conrep.dat")
	if err != nil {
		return
	}
	defer fi.Close()
	enc := xml.NewEncoder(fi)
	if err = enc.Encode(toAdd); err != nil {
		return
	}
	if dryRun {
		return
	}
	cmd := exec.Command("conrep", "-l")
	var out []byte
	out, err = cmd.CombinedOutput()
	os.Stderr.Write(out)
	if err != nil {
		return
	}
	if !cmd.ProcessState.Success() {
		err = errors.New("Error running conrep")
		return
	}
	needReboot = true
	return
}
