package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml Version=$version"
//go:generate sh -c "drpcli contents document content.yaml > bios.rst"
//go:generate ./build-drp-bioscfg.sh
//go:generate go run ../../tools/package-assets/package-assets.go embedded.zip embedded

import (
	_ "embed"
	"os"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

var (
	//go:embed content.yaml
	content string
	//go:embed embedded.zip
	embedded []byte
	version  = v4.RSVersion
	def      = models.PluginProvider{
		Name:          "bios",
		Version:       version,
		PluginVersion: 4,
		AutoStart:     true,
		Content:       content,
	}
)

type Plugin struct {
}

func (p *Plugin) Config(l logger.Logger, session *api.Client, data map[string]interface{}) *models.Error {
	return nil
}

func (p *Plugin) Unpack(thelog logger.Logger, path string) error {
	return utils.Unpack(embedded, path)
}

func main() {
	plugin.InitApp("raid", "Allows in-band BIOS configuration on a machine", version, &def, &Plugin{})
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
