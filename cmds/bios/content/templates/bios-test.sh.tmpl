#!/usr/bin/env bash

echoerr() { cat <<< "$@" 1>&2; }

echoerr "bios-test command: $@"

INDEX={{.Param "bios/test-index"}}

cat > current-state.json <<EOF
{{ .ParamAsJSON "bios/test-current-state" }}
EOF

cat > apply-state.json <<EOF
{{ .ParamAsJSON "bios/test-apply-state" }}
EOF

cat > test-state.json <<EOF
{{ .ParamAsJSON "bios/test-test-state" }}
EOF

# Transform long options to short ones
for arg in "$@"; do
  shift
  case "$arg" in
    '-help')        set -- "$@" '-h'   ;;
    '-driver')      set -- "$@" '-d'   ;;
    '-index')       set -- "$@" '-i'   ;;
    '-seq')         set -- "$@" '-s'   ;;
    '-operation')   set -- "$@" '-o'   ;;
    '-cc')          set -- "$@" '-c'   ;;
    *)              set -- "$@" "$arg" ;;
  esac
done

operation="current"
index=0
seq=0

# Parse short options
OPTIND=1
while getopts "hc:o:d:i:s:" opt
do
  case "$opt" in
    'h') print_usage; exit 0 ;;
    'd') driver=$OPTARG ;;
    'o') operation=$OPTARG ;;
    'c') cc=$OPTARG ;;
    'i') index=$OPTARG ;;
    's') seq=$OPTARG ;;
    '?') print_usage >&2; exit 1 ;;
  esac
done
shift $(expr $OPTIND - 1) # remove options from positional parameters

echoerr "operation: $operation"
echoerr "index    : $INDEX"
echoerr "seq      : $seq"

#  - # A list of test call objects
#    - { "object": { "pig": "test", "snake": "test" }, "rc": 0 }
#    - { "object": { "pig": "test", "snake": "test" }, "rc": 0 }
cat $operation-state.json | jq ".[$INDEX][$seq].object"
RC=$(cat $operation-state.json | jq ".[$INDEX][$seq].rc")

exit $RC
