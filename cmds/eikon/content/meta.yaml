Author: RackN
CodeSource: https://gitlab.com/rackn/provision-plugins
Copyright: RackN
Description: Provision  images to machines.
DisplayName: Eikon Image Deploy
DocUrl: https://docs.rackn.io/stable/redirect/?ref=rs_content_eikon
Icon: image
Color: blue
License: RackN
Name: eikon
Order: "800"
Tags: experimental,rackn,provision,integration,image
Prerequisites: drp-community-content
Documentation: |-
  # Eikon
  The eikon plugin deploys images to systems.  These images can be tarballs that get untarred onto defined partition-based filesystems or raw storage images that get copied onto defined storage devices.  In either case, additional files can be injected into the images for post-install operations.

  The `eikon-image-deploy` pipeline injects and attempts to start the drpcli agent during deployment.  This allows introspection during deployment under most circumstances.  When not possible, the pipeline injects scripts to allow cloud-init or another process to run on first boot.  For Windows, the script will be `C:\eikon-win-agent.ps1`.  This has all the necessary information to configure and start the agent once run.

  !!! note

      When using the pipeline, make sure the universal content pack is loaded.

  The eikon system is purpose built to deploy images and continue to run further pipelines.  Like image-deploy, Eikon can interpret curtin configurations.  Curtin is driven by a configuration YAML file that is built dynamically based upon the value of parameters.  These parameters drive the actual imaging process.  Eikon also provides a similar configuration schema using parameters that can derive from existing image-deploy parameter values.

  ## Getting Started
  There are several `test-eikon*` profiles that are provided that can be used as a starting point.

  ### Eikon Image Construction
  Eikon requires an image.  This image can be in many different formats.  The format type defines what additional operational parameters that need to be set.  These images can be built using RackN's `image-builder` content pack, using Hashicorp's Packer tool, or other tools that generate images.  The images have some specific requirements that must be met for success.

  - All drivers must be added to the system in the base image or additional tarball packages. Eikon does not handle driver updates or installs.

  - For raw disk images, the image must be constructed for the the boot-style of the target hardware.  If the system is going to boot in UEFI mode, then the disk image must be formatted with EFI partitions that hold EFI bootloaders.  The same applies for legacy systems.  There are disk layouts that can work for both.

  A final consideration during image construction is how the next phase of operation starts. For windows, this could be an `unattend.xml` that is part of the image or is injected by additional parameters.  It could also mean that cloud-init is installed and set to automatically start.  Eikon can also inject scripts to install a runner for the OS type in question.  These could also be called to install the agent as well.  Depending upon the choice of startup method, different things need to be done.  Some involve adding things to the image.

  ## Eikon Operations

  Once an image is built, the system needs to know where it is, what type of image it is, where the image is to be deployed, and optionally what startup process to use.  These are all defined as parameters that can be added to a machine.  Because all of this information is consistent across all machines using this image, it is best practice to create a profile that contains all these parameters.

  To define the location of the image file, Eikon uses the `eikon/image-url` parameter.

    * eikon/image-url - This is a URL that references the image.  This must be an http or https URL that does not require authentication.  You can use template functions such as `{{ .ProvisionerURL }}/path/to/file.tar.zst`

  To define the type and content of the image, all of these values must be specified:
    * eikon/image-type - This is the curtin image type value.  These types are define in the parameter and validated by the systems.  Some examples are: `dd-gz`, `tgz`, and others.

  Additional parameters can be used to override and expand the deployement scenario.

  You can build more complex storage configurations and deploy layers of images using the `eikon/plan` parameter.

  ## Eikon Customizations
  With the basic parameters set in a profile, additional parameters can be added to drive further customization and enhancements.

  ### Partitioning
  When using a raw disk image, partitioning is not used other than to select the hard disk.  By default, this will be specified by the `eikon/install-disk` parameter with a value set by the parameter `operating-system-disk` which in turn defaults to `/dev/sda`.  This value can be overridden to direct the install to a different location.

  When using a rootfs and other tarballs, the system will use the `eikon/install-disk` as the default disk and use a simple GPT-based filesystem layout chosen by curtin.

  A custom partition table can be added by using the `eikon/plan` parameter.  This object-based parameter can understand curtin configurations, has a similar feel to to curtin config, but is more heirarchial.

  The `curtin/partitions` parameter is an object that defines the disks, partitions, filesystems, and mount points for system.  The definition of the object can be found here: <https://curtin.readthedocs.io/en/latest/topics/storage.html>.

  ### Additional Content

  The agent binary (also known as drpcli) will be installed as part of the image deployment.  For linux, the binary is installed to `/usr/local/bin`.  If selinux is available, we make sure the binary is correctly labeled.  For Windows, the binary is installed to `C:\Windows`.  For compatiblity with curtin and most of our existing windows tasks, we create the `C:\curtin` directory and link to the binary.  This also includes the `jq.exe` binary which is a link back to drpcli.exe (the client can run standard jq queries using gojq).

  ## Debug and Troubleshooting

  To debug issues, use the standard debug parameter `rs-debug-enable` by setting it to `true`.  There are also two tasks that pause the deployment as an opportunity to login and troubleshoot using ssh or console login.

  - `eikon/chroot-pause`: This pauses in the last task while tasks are still inside the chroot.  This allows logging in and using `chroot /mnt` to look at the deployed images as the tasks would during install.
  - `eikon/post-deploy-puase`: This pauses after all tasks are complete and out of the chroot, but before the filesystems are unmounted.

  The amount of time that either of those is paused for can be set using the `eikon/pause-time` parameter.  The parameter accepts time in seconds.  If `-1` is given, the task will pause indefinitely.  You can resume before any of the times are met by logging into sledgehammer and touching either `/tmp/eikon-resume` or `/mnt/tmp/eikon-resume` if the task name has `chroot` in it's name. 

  ## Using Ubuntuhammer

  To use Ubuntuhammer as the Eikon deployment OS, first upload the ubuntuhammer iso to the endpoint.

  ```shell
  drpcli bootenvs uploadiso ubuntuhammer
  ```

  Next add the param `eikon/deployment-os` to a profile and set its value to "ubuntuhammer". Then attach that profile to the machine. When you run Eikon it should use ubuntuhammer instead of sledgehammer to deploy the image. This works like other params and can be in a profile directly attached to a machine, or at a global level by placing the param in the global profile. Another helpful global param is the `kexec-ok` param. Adding it to your global profile and setting the value to `true` will speed up the process by reducing unnecessary reboots.

  ## Sledgehammer Override
  Overriding the default sledgehammer may be necessary depending on the target OS.  You can use bootenv override profiles.

  ## Using kexec
  Since deployment is almost always contained within linux, it may be possible to use kexec to save a system reset.  Set the `kexec-ok` param to `true` on the machine. 
