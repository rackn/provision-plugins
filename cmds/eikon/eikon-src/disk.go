package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

// Disk defines a logical disk in the system.
//
// This could be a SSD, Spinning disk, or
// external raid drives.  It is basis for all
// other components.
//
// One Path must be specified in the config section.
type Disk struct {
	// Name of disk for future reference
	Name string `json:"name"`

	// Path specifies the path to disk
	Path string `json:"path,omitempty"`

	// TODO: one of these 4 paths needs to be present if path does not exist
	// PathByID specifies the path to disk by id
	PathByID string `json:"path_by_id,omitempty"`

	// PathByUUID specifies the path to disk by uuid
	PathByUUID string `json:"path_by_uuid,omitempty"`

	// PathByPath specifies the path to disk by pci path
	PathByPath string `json:"path_by_path,omitempty"`

	// Image to apply to the disk.
	// Image needs to be a raw format (not tar/zip)
	Image *Image `json:"image,omitempty"`

	// Wipe indicates if the disk should be wiped first
	// meaning PVs, VGs, and FSes removed.
	//
	// Having an image object implies wipe
	Wipe bool `json:"wipe,omitempty"`

	// Zero indicates that the disk should also be zeroed.
	//
	// Having an image object implies zeros
	Zero bool `json:"zero,omitempty"`

	// Partition to use for this disk (if needed).
	// required: true
	PTable string `json:"ptable,omitempty"`

	// GrubDevice indicates if grub should be placed on this device.
	GrubDevice bool `json:"grub_device,omitempty"`

	// Partitions contain the sub-pieces of the disk
	Partitions Partitions `json:"partitions,omitempty"`

	// Size is the size of this disk in bytes
	// Derived value
	Size string `json:"size,omitempty"`

	// FileSystem if defined
	FileSystem *FileSystem `json:"fs,omitempty"`

	// has the image been applied
	imageApplied bool

	// has the label been applied
	labelApplied bool

	// The next starting point for the partition
	nextStart string

	wiped   bool
	zeroed  bool
	success bool
}

// Disks defines a list of disk
type Disks []*Disk

// LsblkDiskInfo To capture the response from lsblk --bytes --json --output-all --paths
type LsblkDiskInfo struct {
	BlockDevices []BlockDevice `json:"blockdevices"`
}
type BlockDevice struct {
	Name       string        `json:"name"`
	KName      string        `json:"kname"`
	MajMin     string        `json:"maj:min"`
	FSType     *string       `json:"fstype"`
	MountPoint *string       `json:"mountpoint"`
	Label      *string       `json:"label"`
	UUID       *string       `json:"uuid"`
	PartLabel  *string       `json:"partlabel"`
	PartUUID   *string       `json:"partuuid"`
	PtType     *string       `json:"pttype"`
	Model      *string       `json:"model"`
	Serial     *string       `json:"serial"`
	Size       uint64        `json:"size"`
	Type       string        `json:"type"`
	Children   []BlockDevice `json:"children"`
}

func (bd *BlockDevice) UnmarshalJSON(data []byte) error {
	type Alias BlockDevice
	aux := &struct {
		Size interface{} `json:"size"`
		*Alias
	}{
		Alias: (*Alias)(bd),
	}

	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	// Handle the Size field conversion
	switch v := aux.Size.(type) {
	case float64:
		bd.Size = uint64(v)
	case string:
		if v == "" {
			bd.Size = 0 // Default value for empty size
		} else {
			size, err := strconv.ParseUint(v, 10, 64)
			if err != nil {
				return err
			}
			bd.Size = size
		}
	default:
		bd.Size = 0 // Default value if size is missing or of unexpected type
	}

	return nil
}

// SfDiskInfo To capture the response from sfdisk -l <disk name> --bytes -J
type SfDiskInfo struct {
	PartitionTable *SfdiskPartitionTable `json:"partitiontable"`
}

type SfdiskPartitionTable struct {
	Label      string              `json:"label"`
	ID         string              `json:"id"`
	Device     string              `json:"device"`
	Unit       string              `json:"unit"`
	FirstLBA   int                 `json:"firstlba"`
	LastLBA    int                 `json:"lastlba"`
	Partitions []*SfdiskPartitions `json:"partitions"`
}

type SfdiskPartitions struct {
	Node  string `json:"node"`
	Start int    `json:"start"`
	Size  int    `json:"size"`
	// Add more fields here if needed
}

// toCompList converts a list of disks to a list of Comparators
func (ds Disks) toCompList() []Comparator {
	ns := []Comparator{}
	for _, d := range ds {
		if d != nil {
			ns = append(ns, d)
		}
	}
	return ns
}

// FindDiskByPath finds a disk by path
func (ds Disks) FindDiskByPath(path string) *Disk {
	for _, d := range ds {
		if d.Path == path {
			return d
		}
	}
	return nil
}

// FindByName finds a piece by name
func (ds Disks) FindByName(name string) interface{} {
	for _, d := range ds {
		if d.Name == name {
			return d
		}
		if obj := d.Partitions.FindByName(name); obj != nil {
			return obj
		}
	}
	return nil
}

// FindPartitionByPath finds a partition by path
func (ds Disks) FindPartitionByPath(path string) *Partition {
	for _, d := range ds {
		if p := d.Partitions.FindPartitionByPath(path); p != nil {
			return p
		}
	}
	return nil
}

// ToDiskList converts a list of comparators into a disk list.
// If an error is passed in, then return that error.
func ToDiskList(src []Comparator, err error) (Disks, error) {
	if err != nil {
		return nil, err
	}
	ds := Disks{}
	for _, s := range src {
		if s != nil {
			ds = append(ds, s.(*Disk))
		}
	}
	return ds, nil
}

func (d *Disk) StorageDeviceType() string {
	return "Disk"
}

func (d *Disk) GetPath() string {
	return d.Path
}

func (d *Disk) GetName() string {
	return d.Name
}

func (d *Disk) GetPTable() string {
	return d.PTable
}

func (d *Disk) NextStart() string {
	return d.nextStart
}

func (d *Disk) SetNextStart(nextStart string) {
	d.nextStart = nextStart
}

func (d *Disk) GetGrubDevice() bool {
	return d.GrubDevice
}

func (d *Disk) SetGrubDevice(b bool) {
	d.GrubDevice = b
}

// updateBlkInfo updates the disk with key/values from lsblk
func (d *Disk) updateBlkInfo(keys map[string]string) error {

	if fst, ok := keys["FSTYPE"]; ok && fst != "" {
		if d.FileSystem == nil {
			d.FileSystem = &FileSystem{}
		}
		fs := d.FileSystem
		fs.updateBlkInfo(keys)
	}

	return nil
}

// Equal compares to disks for identity equivalence.
// This is used to make sure two disks reference the same
// disk.  This is used for config to actual and actual to actual.
func (d *Disk) Equal(c Comparator) bool {
	nd := c.(*Disk)
	return d.Path == nd.Path || d.PathByPath == nd.Path || d.PathByUUID == nd.Path || d.PathByID == nd.Path
}

// Merge takes the incoming argument and replaces the actual parts.
func (d *Disk) Merge(c Comparator) error {
	nd := c.(*Disk)
	d.PTable = nd.PTable
	d.SetGrubDevice(nd.GrubDevice)
	d.Size = nd.Size
	d.nextStart = nd.nextStart
	if d.FileSystem != nil {
		if err := d.FileSystem.Merge(nd.FileSystem); err != nil {
			return err
		}
	} else {
		d.FileSystem = nd.FileSystem
	}
	var err error
	d.Partitions, err = ToPartitionList(
		MergeList(d.Partitions.toCompList(), nd.Partitions.toCompList()))
	return err
}

// Validate validates a Disk object.
func (d *Disk) Validate() error {
	var allErrors error

	if d.Name == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("disk: name must be specified"))
	}
	if d.Path == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("disk: path must be specified"))
	} else {
		// Evaluate symlinks as needed
		if resolvedPath, err := filepath.EvalSymlinks(d.Path); err == nil {
			d.Path = resolvedPath
		}
	}

	var err error
	if d.Size != "" {
		d.Size, err = sizeToBytes(d.Size, sizeMultipliers)
		if err != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("disk %s has invalid size: %v", d.Name, err))
		}
	}

	if len(d.Partitions.toCompList()) > 0 && d.PTable == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("disk: PTable cannot be empty when partitions are present: %s", d.PTable))
	}

	if d.PTable != "" {
		switch d.PTable {
		case "aix", "amiga", "bsd", "dvh", "gpt", "mac", "pc98", "sun", "loop", "dos", "msdos":
		default:
			allErrors = errors.Join(allErrors, fmt.Errorf("disk: PTable is not valid: %s", d.PTable))
		}
	}

	if newParts, err := validatePartitionIDs(d.Partitions, d.Name, "disk"); err != nil {
		allErrors = errors.Join(allErrors, err)
	} else {
		d.Partitions = newParts
	}

	for _, p := range d.Partitions {
		if e := p.Validate(); e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("partition %d (%s) on disk %s is invalid: %v", p.ID, p.Name, d.Name, e))
		}
	}

	ids := map[int]struct{}{}
	for _, p := range d.Partitions {
		if _, ok := ids[p.ID]; ok {
			allErrors = errors.Join(allErrors, fmt.Errorf("partitions using duplicate IDs on disk %s", d.Name))
		}
		ids[p.ID] = struct{}{}
	}

	if d.FileSystem != nil {
		if e := d.FileSystem.Validate(); e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("disk %s: FileSystem is invalid: %v", d.Name, e))
		}
	}

	if d.Image != nil {
		d.Image.rawOnly = true
		if e := d.Image.Validate(); e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("disk %s: Image is invalid: %v", d.Name, e))
		}
	}

	return allErrors
}

func (d *Disk) Remove() error {
	dmsg(DbgDisk|DbgRemove, "Wiping disk: (%s)\n", d.Name)

	for i := len(d.Partitions) - 1; i >= 0; i-- {
		p := d.Partitions[i]
		if err := p.Remove(); err != nil {
			return err
		}
	}

	if _, err := runCommand("wipefs", "--all", d.Path); err != nil {
		return err
	}

	return nil
}

func (d *Disk) doZero() error {
	dmsg(DbgDisk, "Zeroing disk: (%s) - TO DO\n", d.Name)
	// TODO: Zero disk
	return nil
}

// Action takes a config version of the object and applies it
// to the actual object.  return ResultRescan when a
// re-evaluation is needed.
func (d *Disk) Action(c Comparator) (Result, error) {
	if d.success {
		dmsg(DbgDisk|DbgAction, "Disk %s applied successfully. Not reapplying.\n", d.Name)
		return ResultSuccess, nil
	}

	di := c.(*Disk)
	dmsg(DbgDisk|DbgAction, "Disk (%s,%s) action\n", d.Name, di.Name)
	if !d.imageApplied {
		if (di.Wipe || di.Zero || di.Image != nil) && !d.wiped {
			if err := d.Remove(); err != nil {
				return ResultFailed, err
			}
			d.wiped = true
			return ResultRescan, nil
		}

		if di.Zero && di.Image == nil && !d.zeroed {
			if err := d.doZero(); err != nil {
				return ResultFailed, err
			}
			d.zeroed = true
			return ResultRescan, nil
		}

		if di.Image != nil {
			// Put image in place
			di.Image.Path = d.Path
			if err := di.Image.Action(); err != nil {
				return ResultFailed, err
			}
			d.imageApplied = true
			return ResultRescan, nil
		}
	}

	d.PTable = normalizePTable(d.PTable)
	di.PTable = normalizePTable(di.PTable)

	if di.PTable != "" {
		if !d.labelApplied && di.PTable != d.PTable {
			dmsg(DbgDisk|DbgAction, "Applying partition lable. Input label: %s Scanned label: %s\n", di.PTable, d.PTable)
			d.labelApplied = true
			return ResultRescan, runParted(d.Path, "mklabel", di.PTable)
		}

		// Ensure partition table is correct.
		if di.PTable != d.PTable {
			return ResultFailed, fmt.Errorf("disk, %s, disk label doesn't match (A: %s C: %s)", di.Name, d.PTable, di.PTable)
		}
	}

	// Build the partitions.
	dmsg(DbgDisk, "Building the partitions.\n")
	dmsg(DbgDisk, "Total partitions: %v\n", len(di.Partitions))

	// Process partitions in sorted order
	for _, pi := range di.Partitions {
		dmsg(DbgDisk, "Working on partition: %v \n", pi)
		found := false
		for _, p := range d.Partitions {
			if p.Equal(pi) {
				dmsg(DbgDisk, "Found matching partition. %v, %v\n", p, pi)
				r, e := p.Action(pi)
				if e != nil || r == ResultRescan {
					return r, e
				}
				found = true
				break
			}
		}
		if !found {
			// We just recreated a partition, rescan.
			// This is overkill but safe.
			p := &Partition{parent: d}
			r, e := p.Action(pi)
			if e != nil {
				return r, e
			}
			return ResultRescan, nil
		}
	}

	d.success = true
	return ResultSuccess, nil
}

func isIgnoredDiskType(diskType string) bool {
	ignoreTypes := map[string]bool{
		"rom":  true,
		"loop": true,
		// Add more ignore types here as needed
	}
	_, found := ignoreTypes[diskType]
	return found
}

func getLsblkDiskInfo() (*LsblkDiskInfo, error) {
	out, err := exec.Command("lsblk", "--bytes", "--output-all", "--paths", "--json").CombinedOutput()
	if err != nil {
		dmsg(DbgDisk|DbgScan, "error running lsblk: %v\n", err)
		return nil, err
	}
	var ld LsblkDiskInfo
	if err = json.Unmarshal(out, &ld); err != nil {
		dmsg(DbgDisk|DbgScan, "error parsing lsblk response: %v\n", err)
		return nil, err
	}
	return &ld, nil
}

func getSfdisknfo(diskName string) (*SfDiskInfo, error) {
	out, err := exec.Command("sfdisk", diskName, "--bytes", "-l", "-J").CombinedOutput()
	if err != nil {
		dmsg(DbgDisk|DbgScan, "error running sfdisk on %s: %v\n", diskName, err)
		return nil, err
	}
	var sfd SfDiskInfo
	err = json.Unmarshal(out, &sfd)
	if err != nil {
		dmsg(DbgDisk|DbgScan, "error parsing sfdisk response for %s. %v\n", diskName, err)
		return nil, err
	}
	return &sfd, nil
}

// Because different tools take/return different values for dos/msdos partition table, normalize the values.
func normalizePTable(ptable string) string {
	if ptable == "dos" || ptable == "msdos" {
		return "msdos"
	}
	return ptable
}

// buildSymlinkMap reads the directory at dirPath and builds a map of symlinks
func buildSymlinkMap(dirPath string) map[string]string {
	symlinkMap := make(map[string]string)
	entries, err := os.ReadDir(dirPath)
	if err == nil {
		for _, entry := range entries {
			entryPath := filepath.Join(dirPath, entry.Name())
			info, err := entry.Info()
			if err != nil {
				dmsg(DbgDisk|DbgScan, "Error getting diskBy %s info: %v\n", dirPath, err)
				continue
			}

			if info.Mode()&os.ModeSymlink != 0 {
				linkTarget, err := os.Readlink(entryPath)
				if err != nil {
					dmsg(DbgDisk|DbgScan, "Could not get symlink for diskBy %s info: %v\n", dirPath, err)
					continue
				}
				symlinkMap[filepath.Base(linkTarget)] = entryPath
			}
		}
	}
	return symlinkMap
}

func buildDiskByInfo() (diskByMap map[string]map[string]string) {
	diskByMap = make(map[string]map[string]string)
	diskByMap["byId"] = buildSymlinkMap("/dev/disk/by-id")
	diskByMap["byPath"] = buildSymlinkMap("/dev/disk/by-path")
	diskByMap["byUuid"] = buildSymlinkMap("/dev/disk/by-uuid")
	return
}

// Extracts the last part of the path after the last slash
func extractDiskName(path string) string {
	// Split the path on the slash character
	parts := strings.Split(path, "/")
	// The last part is the device name, return it
	if len(parts) > 0 {
		return parts[len(parts)-1]
	}
	return ""
}

func ScanDisks() (Disks, error) {
	lsblkInfo, err := getLsblkDiskInfo()
	if err != nil {
		return nil, err
	}

	// Build the diskBy map
	diskByInfo := buildDiskByInfo()

	disks := Disks{}
	var currentDisk *Disk
	for _, d := range lsblkInfo.BlockDevices {
		dmsg(DbgDisk|DbgScan, "processing disk line: %+v\n", d)
		if isIgnoredDiskType(d.Type) {
			continue
		}

		var sfDiskInfo *SfDiskInfo
		currentDisk = &Disk{}
		// Run sfdisk on this disk and get the partition info
		sfDiskInfo, err = getSfdisknfo(d.Name)
		if err == nil {
			if sfDiskInfo != nil && sfDiskInfo.PartitionTable != nil {
				currentDisk.PTable = sfDiskInfo.PartitionTable.Label
			}
		}

		diskName := extractDiskName(d.Name)
		currentDisk.Name = diskName
		currentDisk.Path = d.Name
		currentDisk.Size = fmt.Sprintf("%vB", d.Size)

		// Get the Disk by information
		if val, ok := diskByInfo["byId"][diskName]; ok {
			currentDisk.PathByID = val
		}
		if val, ok := diskByInfo["byPath"][diskName]; ok {
			currentDisk.PathByPath = val
		}
		if val, ok := diskByInfo["byUuid"][diskName]; ok {
			currentDisk.PathByUUID = val
		}

		disks = append(disks, currentDisk)

		for _, c := range d.Children {
			dmsg(DbgDisk|DbgScan, "processing part line: %s\n", c.Name)
			if c.Type != "part" {
				continue
			}

			part := &Partition{}
			if pid, err := getPartitionID(c.Name); err == nil {
				part.ID = pid
			}
			part.PType = derefString(c.FSType)
			if c.PartLabel != nil {
				part.Name = nameReplacer.Replace(*c.PartLabel)
			} else {
				part.Name = c.Name
			}
			part.parent = currentDisk

			// Find the matching partition from the sfdisk response
			if sfDiskInfo != nil && sfDiskInfo.PartitionTable != nil {
				for _, p := range sfDiskInfo.PartitionTable.Partitions {
					if p.Node == c.Name {
						size := p.Size * 512
						start := p.Start * 512
						end := start + size - 1
						part.Size = fmt.Sprintf("%dB", size)
						part.Start = fmt.Sprintf("%dB", start)
						part.End = fmt.Sprintf("%dB", end)
						currentDisk.nextStart = fmt.Sprintf("%dB", end+1)
						break
					}
				}
			}
			currentDisk.Partitions = append(currentDisk.Partitions, part)
		}
	}

	return disks, nil
}

func (d *Disk) String() string {
	if d == nil {
		return "unset"
	}
	ps := ""
	comma := ""
	for _, p := range d.Partitions {
		ps += fmt.Sprintf("%s%v", comma, p)
		comma = ","
	}
	return fmt.Sprintf("<name:%s path:%s pathbyid:%s pathbyuuid:%s pathbypath:%s iamge:%v wipe:%v zero:%v ptable:%s grubdev:%v parts:%s size:%s fs:%v imageapplied:%v labelapplied:%v nextStart:%s>",
		d.Name,
		d.Path,
		d.PathByID,
		d.PathByUUID,
		d.PathByPath,
		d.Image,
		d.Wipe,
		d.Zero,
		d.PTable,
		d.GrubDevice,
		ps,
		d.Size,
		d.FileSystem,
		d.imageApplied,
		d.labelApplied,
		d.nextStart)
}
