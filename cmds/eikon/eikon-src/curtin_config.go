package main

import (
	"fmt"
	"strings"
)

const (
	TypeDisk         = "disk"
	TypePartition    = "partition"
	TypeLVMVolGroup  = "lvm_volgroup"
	TypeLVMPartition = "lvm_partition"
	TypeFormat       = "format"
	TypeMount        = "mount"
	TypeRaid         = "raid"
)

var uriTypeMap = map[string]string{
	"file://":  "tar",
	"http://":  "tar",
	"https://": "tar",
}

type CurtinConfig struct {
	Storage *CurtinStorage        `json:"storage"`
	Sources interface{}           `json:"sources"`
	Swap    *CurtinSwapFileConfig `json:"swap"`
}

type CurtinSwapFileConfig struct {
	// Configure the filename of the swap file. Defaults to /swap.img
	Filename string `json:"filename"`
	// Configure the max size of the swapfile, defaults to 8GB
	Maxsize string `json:"maxsize"`
	// Configure the exact size of the swapfile. Setting size to 0 will disable swap.
	Size string `json:"size"`
	// Force the creation of swapfile even if curtin detects it may not work. In some target filesystems,
	// e.g. btrfs, xfs, zfs, the use of a swap file has restrictions. If curtin detects that there may be
	// issues it will refuse to create the swapfile. Users can force creation of a swapfile by passing
	// force: true. A forced swapfile may not be used by the target OS and could log cause an error.
	Force bool `json:"force"`
}

type CurtinStorage struct {
	Config  []*CurtinStorageConfig `json:"config"`
	Version int                    `json:"version"`
}

type CurtinStorageConfig struct {
	ID            string   `json:"id,omitempty"`
	Type          string   `json:"type,omitempty"`
	Wipe          string   `json:"wipe,omitempty"`
	PTable        string   `json:"ptable,omitempty"`
	Path          string   `json:"path,omitempty"`
	PartitionType string   `json:"partition_type,omitempty"  yaml:"partition_type,omitempty"`
	Model         string   `json:"model,omitempty"`
	Serial        string   `json:"serial,omitempty"`
	Size          string   `json:"size,omitempty"`
	RaidLevel     string   `json:"raidlevel,omitempty"`
	Metadata      string   `json:"metadata,omitempty"`
	SpareDevices  []string `json:"spare_devices,omitempty" yaml:"spare_devices,omitempty"`
	Preserve      bool     `json:"preserve,omitempty"`
	GrubDevice    bool     `json:"grub_device,omitempty" yaml:"grub_device,omitempty"`
	Name          string   `json:"name,omitempty"`
	Number        int      `json:"number,omitempty"`
	Flag          string   `json:"flag,omitempty"`
	Devices       []string `json:"devices,omitempty"`
	Device        string   `json:"device,omitempty"`
	VolGroup      string   `json:"volgroup,omitempty"`
	Fstype        string   `json:"fstype,omitempty"`
	Volume        string   `json:"volume,omitempty"`
	Label         string   `json:"label,omitempty"`
	Uuid          string   `json:"uuid,omitempty"`
}

func validatePartitionNumbers(parts []*Partition) error {
	if len(parts) == 0 {
		return nil
	}

	// Count how many partitions have explicit numbers
	numbered := 0
	for _, p := range parts {
		if p.ID != 0 {
			numbered++
		}
	}

	// Either all should have numbers or none should
	if numbered > 0 && numbered != len(parts) {
		return fmt.Errorf("either all partitions must be numbered or none should")
	}

	// If they have numbers, validate uniqueness
	if numbered > 0 {
		seen := make(map[int]bool)
		for _, p := range parts {
			if seen[p.ID] {
				return fmt.Errorf("duplicate partition number %d", p.ID)
			}
			seen[p.ID] = true
		}
	}

	return nil
}

func (c *Config) initializeMaps(cc *CurtinConfig) (diskMap map[string]*Disk, partMap map[string]*Partition, fsMap map[string]*FileSystem, lvMap map[string]*LogicalVolume, vgMap map[string]*VolumeGroup, mountMap map[string]string, raidMap map[string]*SwRaid) {
	// Initialize maps
	diskMap = make(map[string]*Disk)
	partMap = make(map[string]*Partition)
	fsMap = make(map[string]*FileSystem)
	lvMap = make(map[string]*LogicalVolume)
	vgMap = make(map[string]*VolumeGroup)
	raidMap = make(map[string]*SwRaid)
	mountMap = make(map[string]string)

	// Populate maps
	for _, c := range cc.Storage.Config {
		name := c.ID

		switch c.Type {
		case TypeDisk: // Disk
			d := &Disk{
				PTable: c.PTable,
				Path:   c.Path,
				Name:   name,
			}
			if c.Path == "" {
				d.Path = fmt.Sprintf("/dev/%s", c.ID)
			}
			d.SetGrubDevice(c.GrubDevice)
			diskMap[c.ID] = d
		case TypePartition: // Partition
			p := &Partition{
				Size:     c.Size,
				PType:    c.PartitionType,
				Name:     name,
				Flag:     c.Flag,
				parentId: c.Device,
			}
			// Use provided number if it exists
			if c.Number != 0 {
				p.ID = c.Number
			}
			partMap[c.ID] = p
		case TypeLVMVolGroup: // VG
			vg := &VolumeGroup{
				Name:    name,
				Devices: c.Devices,
			}
			vgMap[c.ID] = vg
		case TypeLVMPartition: //
			size := c.Size
			if size == "" {
				size = "REST"
			}
			lv := &LogicalVolume{
				Name:     name,
				parentVg: c.VolGroup,
				Size:     size,
			}
			lvMap[c.ID] = lv
		case TypeFormat: // Filesystem
			fs := &FileSystem{
				Name:   name,
				Type:   c.Fstype,
				Label:  c.Label,
				UUID:   c.Uuid,
				volume: c.Volume,
			}
			fsMap[c.ID] = fs
		case TypeMount: // mount path for FS
			// we only care about path and the fs it maps to
			mountMap[c.Device] = c.Path
		case TypeRaid:
			s := &SwRaid{
				Name:         name,
				RaidLevel:    c.RaidLevel,
				Path:         c.Path,
				Devices:      c.Devices,
				SpareDevices: c.SpareDevices,
				Wipe:         c.Wipe,
				Preserve:     c.Preserve,
				PTable:       c.PTable,
				Metadata:     c.Metadata,
			}
			s.SetGrubDevice(c.GrubDevice)
			raidMap[c.ID] = s
		default:
			dmsg(DbgCurtinConversion, "the given type: %s is not supported. it will not be processed", c.Type)
			continue
		}
	}
	return
}

func getTypeFromURI(uri string) (string, string) {
	if strings.HasPrefix(uri, "dd-") {
		parts := strings.SplitN(uri, ":", 2)
		if len(parts) == 2 {
			return strings.TrimSpace(parts[1]), parts[0]
		}
	}

	for prefix, uriType := range uriTypeMap {
		if strings.HasPrefix(uri, prefix) {
			return uri, uriType
		}
	}
	return uri, ""
}

func (c *Config) convertToEikonPlan(cc *CurtinConfig) error {
	// Create maps for easy lookup
	diskMap, partMap, fsMap, lvMap, vgMap, mountMap, raidMap := c.initializeMaps(cc)

	// Update all the file systems with mount information
	for fsId, fs := range fsMap {
		if _, ok := mountMap[fsId]; ok {
			fs.Mount = mountMap[fsId]
		}
	}

	// Update disks, partitions and logical volumes with filesystems if needed
	for _, fs := range fsMap {
		// See if it is part of a disk
		if _, ok := diskMap[fs.volume]; ok {
			diskMap[fs.volume].FileSystem = fs
		}

		// See if it is part of a partition
		if _, ok := partMap[fs.volume]; ok {
			partMap[fs.volume].FileSystem = fs
		}

		// See if it is part of a logical volume
		if _, ok := lvMap[fs.volume]; ok {
			lvMap[fs.volume].FileSystem = fs
		}
	}

	// Add partitions to disks as needed and add disks back to partitions
	for _, part := range partMap {
		// Parent could be a disk or a raid device
		if _, ok := diskMap[part.parentId]; ok {
			diskMap[part.parentId].Partitions = append(diskMap[part.parentId].Partitions, part)
			part.parent = diskMap[part.parentId]
		}
		if _, ok := raidMap[part.parentId]; ok {
			raidMap[part.parentId].Partitions = append(raidMap[part.parentId].Partitions, part)
			part.parent = raidMap[part.parentId]
		}
	}

	// Validate partition numbers for both disks and raids
	for _, disk := range diskMap {
		if err := validatePartitionNumbers(disk.Partitions); err != nil {
			return fmt.Errorf("invalid curtin config for disk %s: %v", disk.Name, err)
		}
		if len(disk.Partitions) > 0 && disk.Partitions[0].ID == 0 {
			for i, p := range disk.Partitions {
				p.ID = i + 1
			}
		}
	}

	for _, raid := range raidMap {
		if err := validatePartitionNumbers(raid.Partitions); err != nil {
			return fmt.Errorf("invalid curtin config for swraid %s: %v", raid.Name, err)
		}
		if len(raid.Partitions) > 0 && raid.Partitions[0].ID == 0 {
			for i, p := range raid.Partitions {
				p.ID = i + 1
			}
		}
	}

	// Add logical volumes to volume groups
	for _, lv := range lvMap {
		if _, ok := vgMap[lv.parentVg]; ok {
			vgMap[lv.parentVg].LogicalVolumes = append(vgMap[lv.parentVg].LogicalVolumes, lv)
		}
	}

	// Add disks/partitions to volume groups as physical volumes
	for _, vg := range vgMap {
		for _, d := range vg.Devices {
			if _, ok := diskMap[d]; ok {
				pv := &PhysicalVolume{
					Name:       diskMap[d].Name,
					Size:       diskMap[d].Size,
					parentPath: diskMap[d].Path,
					usingVG:    vg,
				}
				vg.physicalVolumes = append(vg.physicalVolumes, pv)
				continue
			}

			if _, ok := partMap[d]; ok {
				pv := &PhysicalVolume{
					Name:       partMap[d].Name,
					Size:       partMap[d].Size,
					parentPath: GeneratePartitionPath(partMap[d].parent.GetPath(), partMap[d].ID),
					usingVG:    vg,
				}
				vg.physicalVolumes = append(vg.physicalVolumes, pv)
			}
		}
	}

	// Now add raids, disks and vgs to the config
	for _, raid := range raidMap {
		c.SwRaids = append(c.SwRaids, raid)
	}
	for _, disk := range diskMap {
		c.Disks = append(c.Disks, disk)
	}
	for _, vg := range vgMap {
		c.VolumeGroups = append(c.VolumeGroups, vg)
	}

	// Handle sources based on type
	switch sources := cc.Sources.(type) {
	case []interface{}: // String list case
		for _, s := range sources {
			if str, ok := s.(string); ok {
				uri, uriType := getTypeFromURI(str)
				c.Images = append(c.Images, &Image{URL: uri, Type: uriType})
			}
		}
	case map[interface{}]interface{}: // YAML case
		for _, source := range sources {
			if sourceMap, ok := source.(map[interface{}]interface{}); ok {
				if uri, ok := sourceMap["uri"].(string); ok {
					if sourceType, ok := sourceMap["type"].(string); ok {
						c.Images = append(c.Images, &Image{URL: uri, Type: sourceType})
					}
				}
			}
		}
	case map[string]interface{}: // JSON case
		for _, source := range sources {
			if sourceMap, ok := source.(map[string]interface{}); ok {
				if uri, ok := sourceMap["uri"].(string); ok {
					if sourceType, ok := sourceMap["type"].(string); ok {
						c.Images = append(c.Images, &Image{URL: uri, Type: sourceType})
					}
				}
			}
		}
	}

	// Convert swap configuration
	if cc.Swap != nil {
		c.SwapFile = &SwapFile{
			Filename: cc.Swap.Filename,
			Maxsize:  cc.Swap.Maxsize,
			Size:     cc.Swap.Size,
			Force:    cc.Swap.Force,
		}
	}
	return nil
}
