package main

import (
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"syscall"
)

const (
	mdBasePath = "/sys/class/block"
)

var (
	readWriteRaidStates = []string{"read-auto", "clean", "active", "active-idle", "write-pending"}
	nospareRaidLevels   = []string{"linear", "raid0", "0", "container"}
	spareRaidLevels     = []string{"raid1", "stripe", "mirror", "1", "raid4", "4", "raid5", "5", "raid6", "6", "raid10", "10"}
	validRaidLevels     = append(nospareRaidLevels, spareRaidLevels...)
)

// SwRaid defines a software raid system
type SwRaid struct {
	// Name indicates the name of the md device
	// required: true
	Name string `json:"name"`

	// Path specifies the path to the device
	Path string `json:"path,omitempty"`

	// Devices indicates the list of devices that will be included in the raid array.
	// Can be disks or partitions.
	// These devices must already be defined in the eikon plan.
	// required: true
	Devices []string `json:"devices"`

	// GrubDevice indicates if grub should be placed on this device.
	GrubDevice bool `json:"grub_device,omitempty"`

	// SpareDevices is an optional list of spare devices for the raid array.
	// Can be disks or partitions and must be defined in the eikon plan.
	SpareDevices []string `json:"spare_devices,omitempty"`

	// RaidLevel indicates the raif level of the array.
	// 0, 1, 5, 6, 10
	// required: true
	RaidLevel string `json:"raid_level"`

	// Wipe can be
	// superblock, superblock-recursive, pvremove, zero, random
	Wipe string `json:"wipe,omitempty"`

	// Preserve: when set to true we will attempt reuse the existing storage device
	// If current configuration is invalid new swraid will be created
	Preserve bool `json:"preserve,omitempty"`

	// PTable needs to be defined to partition the array rather than mounting it directly.
	// msdos, gpt
	// required: true
	PTable string `json:"ptable"`

	// Metadata indicates the metadata (superblock) style to be used when creating the array.
	// Metadata defaults to the string “default” and is passed to mdadm.
	// default, 1.2, 1.1, 0.90, ddf, imsm
	Metadata string `json:"metadata"`
	// images
	// curitn does not support swraid on raw images - look into it

	// Partitions contain the sub-pieces of the disk
	Partitions Partitions `json:"partitions,omitempty"`

	// FileSystem if defined
	FileSystem *FileSystem `json:"fs,omitempty"`

	// Image to apply to the raid.
	// Image needs to be a raw format (not tar/zip)
	Image *Image `json:"image,omitempty"`

	// has the image been applied
	imageApplied bool

	// has the label been applied
	labelApplied bool

	// The next starting point for the partition
	nextStart string

	// Size is the size of this device in bytes
	// Derived value
	Size string `json:"size,omitempty"`

	success bool
}

// SwRaids defines list of SwRaid
type SwRaids []*SwRaid

// raidLevelMap is a map of valid raid levels to whether spare devices are supported
var raidLevelMap = map[interface{}]bool{
	"linear": false,
	"raid0":  false,
	0:        false,
	"0":      false,
	"stripe": false,
	"raid1":  true,
	1:        true,
	"1":      true,
	"mirror": true,
	"raid4":  true,
	4:        true,
	"4":      true,
	"raid5":  true,
	5:        true,
	"5":      true,
	"raid6":  true,
	6:        true,
	"6":      true,
	"raid10": true,
	10:       true,
	"10":     true,
}

// toCompList converts a list of SwRaid to a list of Comparator
func (sws SwRaids) toCompList() []Comparator {
	ns := []Comparator{}
	for _, sw := range sws {
		ns = append(ns, sw)
	}
	return ns
}

// FindPartitionByPath finds a partition by path
func (sws SwRaids) FindPartitionByPath(path string) *Partition {
	for _, s := range sws {
		if p := s.Partitions.FindPartitionByPath(path); p != nil {
			return p
		}
	}
	return nil
}

// FindBySwName finds a raid by path
func (sws SwRaids) FindBySwName(name string) *SwRaid {
	for _, s := range sws {
		if s.Name == name {
			return s
		}
	}
	// If no name match was found, then the name in lsblk does not match the name id mdadm
	// So first get the device UUID using the name provided
	mdDetails := mdQueryDetail(name)
	givenUuid, ok := mdDetails["MD_UUID"]
	if !ok {
		return nil
	}
	for _, s := range sws {
		mdDetails := mdQueryDetail(s.Path)
		uuid, found := mdDetails["MD_UUID"]
		if found && uuid == givenUuid {
			return s
		}
	}
	return nil
}

// FindByName finds a raid by name
func (sws SwRaids) FindByName(name string) interface{} {
	for _, s := range sws {
		if s.Name == name {
			return s
		}
		if obj := s.Partitions.FindByName(name); obj != nil {
			return obj
		}
	}
	return nil
}

// ToSwRaidList converts a list of Comparator to a list of SwRaid
func ToSwRaidList(src []Comparator, err error) (SwRaids, error) {
	if err != nil {
		return nil, err
	}
	sws := SwRaids{}
	for _, s := range src {
		sws = append(sws, s.(*SwRaid))
	}
	return sws, nil
}

// updateBlkInfo updates the swraid with key/values from lsblk
func (sw *SwRaid) updateBlkInfo(keys map[string]string) error {
	if fst, ok := keys["FSTYPE"]; ok && fst != "" {
		if sw.FileSystem == nil {
			sw.FileSystem = &FileSystem{}
		}
		fs := sw.FileSystem
		fs.updateBlkInfo(keys)
	}

	return nil
}

func (sw *SwRaid) StorageDeviceType() string {
	return "SwRaid"
}

func (sw *SwRaid) GetPath() string {
	return sw.Path
}

func (sw *SwRaid) GetName() string {
	return sw.Name
}

func (sw *SwRaid) GetPTable() string {
	return sw.PTable
}

func (sw *SwRaid) NextStart() string {
	return sw.nextStart
}

func (sw *SwRaid) SetNextStart(nextStart string) {
	sw.nextStart = nextStart
}

func (sw *SwRaid) GetGrubDevice() bool {
	return sw.GrubDevice
}

func (sw *SwRaid) SetGrubDevice(b bool) {
	sw.GrubDevice = b
}

// Equal tests identity equivalence for SwRaid pieces.
func (sw *SwRaid) Equal(c Comparator) bool {
	nsw := c.(*SwRaid)

	// We need to find the raid path if given a name
	mdDetails := mdQueryDetail(sw.Path)
	devName, _ := mdDetails["MD_DEVNAME"]
	return sw.Name == nsw.Name || sw.Path == nsw.Path || devName == nsw.Name
}

// Merge takes the incoming argument and replaces the actual parts.
func (sw *SwRaid) Merge(c Comparator) error {
	nd := c.(*SwRaid)
	sw.PTable = nd.PTable
	sw.Size = nd.Size
	sw.SetGrubDevice(nd.GrubDevice)
	sw.nextStart = nd.nextStart
	if sw.FileSystem != nil {
		if err := sw.FileSystem.Merge(nd.FileSystem); err != nil {
			return err
		}
	} else {
		sw.FileSystem = nd.FileSystem
	}
	var err error
	sw.Partitions, err = ToPartitionList(
		MergeList(sw.Partitions.toCompList(), nd.Partitions.toCompList()))
	return err
}

// validateRAIDName checks if the given RAID name is valid.
func validateRAIDName(name string) bool {
	allStringPattern := `^[a-zA-Z0-9]+$`         // Condition 1: All string (alphabetic)
	devNamePattern := `^/dev/md/[a-zA-Z0-9-_]+$` // Condition 3: /dev/md/somename where "somename" is a valid string
	devNumberPattern := `^/dev/md\d{1,3}$`       // Condition 2: /dev/mdX where X is a number from 0 to 255 or 4095

	allStringRegex := regexp.MustCompile(allStringPattern)
	devNumberRegex := regexp.MustCompile(devNumberPattern)
	devNameRegex := regexp.MustCompile(devNamePattern)

	// Check if the name is a simple all-string RAID name
	if allStringRegex.MatchString(name) || devNameRegex.MatchString(name) {
		return true
	}
	if devNumberRegex.MatchString(name) {
		re := regexp.MustCompile(`\d+`)
		num := re.FindString(name)
		if numInt, err := strconv.Atoi(num); err == nil && numInt >= 0 && numInt <= 255 { // Change to 4095 for extended limit
			return true
		}
	}

	return false
}

// Validate a SwRaid object.
func (sw *SwRaid) Validate() error {
	var allErrors error

	if sw.Name == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("raid: name must be specified"))
	} else if !validateRAIDName(sw.Name) {
		allErrors = errors.Join(allErrors, fmt.Errorf("raid: invalid raid name %s", sw.Name))
	}

	if len(sw.Devices) < 1 {
		allErrors = errors.Join(allErrors, fmt.Errorf("raid: name: %s is not valid", sw.Name))
	} else {

	}

	if _, ok := raidLevelMap[sw.RaidLevel]; !ok {
		allErrors = errors.Join(allErrors, fmt.Errorf("raid: invalid or unsupported raid level: %s", sw.RaidLevel))
	}

	if spareSupported, ok := raidLevelMap[sw.RaidLevel]; ok && !spareSupported && len(sw.SpareDevices) > 0 {
		allErrors = errors.Join(allErrors, fmt.Errorf("raid: level: %s does not support spare devices", sw.RaidLevel))
	}

	var err error
	if sw.Size != "" {
		sw.Size, err = sizeToBytes(sw.Size, sizeMultipliers)
		if err != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("raid: %s has invalid size: %v", sw.Name, err))
		}
	}

	if len(sw.Partitions.toCompList()) > 0 && sw.PTable == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("raid: PTable cannot be empty when partitions are present: %s", sw.PTable))
	}

	if sw.PTable != "" {
		switch sw.PTable {
		case "aix", "amiga", "bsd", "dvh", "gpt", "mac", "pc98", "sun", "loop", "dos", "msdos":
		default:
			allErrors = errors.Join(allErrors, fmt.Errorf("raid: PTable is not valid: %s", sw.PTable))
		}
	}

	if newParts, err := validatePartitionIDs(sw.Partitions, sw.Name, "raid"); err != nil {
		allErrors = errors.Join(allErrors, err)
	} else {
		sw.Partitions = newParts
	}

	for _, p := range sw.Partitions {
		if e := p.Validate(); e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("raid: partition %d (%s) on raid %s is invalid: %v", p.ID, p.Name, sw.Name, e))
		}
	}

	ids := map[int]struct{}{}
	for _, p := range sw.Partitions {
		if _, ok := ids[p.ID]; ok {
			allErrors = errors.Join(allErrors, fmt.Errorf("raid: partitions using duplicate IDs on raid %s", sw.Name))
		}
		ids[p.ID] = struct{}{}
	}

	if sw.FileSystem != nil {
		if e := sw.FileSystem.Validate(); e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("raid: %s: FileSystem is invalid: %v", sw.Name, e))
		}
	}

	if sw.Image != nil {
		sw.Image.rawOnly = true
		if e := sw.Image.Validate(); e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("raid: %s: Image is invalid: %v", sw.Name, e))
		}
	}

	return allErrors
}

// getRaidLevel extracts the RAID level from a raid type string.
func getRaidLevel(raidType string) string {
	matches := raidPattern.FindStringSubmatch(raidType)
	if len(matches) != 2 {
		return "invalid"
	}
	return matches[1]
}

func getPtable(path string) string {
	out, err := exec.Command("blkid", path).CombinedOutput()
	if err != nil {
		dmsg(DbgDisk|DbgScan, "error running blkid on %s. %v\n", path, err)
		return ""
	}
	output := string(out)
	re := regexp.MustCompile(`PTTYPE="([^"]+)"`)
	matches := re.FindStringSubmatch(output)
	if len(matches) > 1 {
		return matches[1]
	}
	return ""
}

// processRaid processes RAID details from a BlockDevice and populates the swRaidMap.
func processRaid(d BlockDevice, swRaidMap map[string]*SwRaid) {
	// Find the raid device details
	for _, c := range d.Children {
		// Run sfdisk on this disk and get the partition info
		sfDiskInfo, _ := getSfdisknfo(c.Name)
		// Get the friendly name of the raid instead of using path
		if raidPattern.MatchString(c.Type) {
			dmsg(DbgSwRaid|DbgScan, "found a raid device: %+v\n", c)
			queryDetails := mdQueryDetail(c.Name)
			devName, _ := queryDetails["MD_DEVNAME"]
			if devName == "" {
				devName = c.Name
			}
			// Check if this RAID device is already in the map
			if raid, ok := swRaidMap[devName]; !ok {
				r := &SwRaid{
					Name:      devName,
					Path:      c.Name,
					Devices:   []string{d.Name},
					PTable:    derefString(c.PtType),
					RaidLevel: getRaidLevel(c.Type),
					Size:      fmt.Sprintf("%vB", c.Size),
				}

				// If Ptable is not set, get it blkid just to be sure
				if r.PTable == "" {
					r.PTable = getPtable(r.Path)
				}

				// Process partitions of the RAID device
				for _, cc := range c.Children {
					dmsg(DbgSwRaid|DbgScan, "this raid device has partitions: %+v\n", cc)
					part := &Partition{}
					if pid, err := getPartitionID(cc.Name); err == nil {
						part.ID = pid
					}
					part.PType = derefString(cc.FSType)
					if cc.PartLabel != nil {
						part.Name = nameReplacer.Replace(derefString(cc.PartLabel))
					} else {
						part.Name = cc.Name
					}
					part.parent = r // Assign parent as the RAID device

					// Find the matching partition from the sfdisk response
					if sfDiskInfo != nil && sfDiskInfo.PartitionTable != nil {
						for _, p := range sfDiskInfo.PartitionTable.Partitions {
							if p.Node == cc.Name {
								size := p.Size * 512   // convert sectors to bytes
								start := p.Start * 512 // todo: is it always going to be 512?
								end := start + size - 1
								part.Size = fmt.Sprintf("%dB", size)
								part.Start = fmt.Sprintf("%dB", start)
								part.End = fmt.Sprintf("%dB", end)
								r.nextStart = fmt.Sprintf("%dB", end+1)
								break
							}
						}
					}
					r.Partitions = append(r.Partitions, part)
				}
				swRaidMap[devName] = r
			} else {
				raid.Devices = append(raid.Devices, d.Name)
				swRaidMap[devName] = raid
			}
		}
	}
}

// ScanRaids scans the system for RAID devices and returns a list of SwRaids.
func ScanRaids() (SwRaids, error) {
	swRaids := SwRaids{}

	// Get all disk Info
	lsblkInfo, err := getLsblkDiskInfo()
	if err != nil {
		return nil, err
	}

	// Map to hold swraids
	swRaidMap := make(map[string]*SwRaid)

	// Loop through all the disks and partitions and find all linux_raid_member
	// These will be disks and partitions that are a part of a raid array
	for _, d := range lsblkInfo.BlockDevices {
		// Process RAID members in the disk
		if derefString(d.FSType) == "linux_raid_member" {
			processRaid(d, swRaidMap)
		}
		// Process RAID members in the partitions of the disk
		for _, c := range d.Children {
			if derefString(c.FSType) == "linux_raid_member" {
				processRaid(c, swRaidMap)
			}
		}
	}

	// Convert the map to a slice of SWRaids
	for _, raid := range swRaidMap {
		swRaids = append(swRaids, raid)
	}

	dmsg(DbgSwRaid|DbgScan, "raids have been scanned: %v\n", swRaids)
	return swRaids, nil
}

// mdQueryDetail queries RAID details for a given RAID device path.
func mdQueryDetail(raidDevicePath string) map[string]string {
	out, err := runCommand("mdadm", "--query", "--detail", "--export", raidDevicePath)
	if err != nil {
		dmsg(DbgSwRaid|DbgScan, "error running mdadm query: %v\n", err)
		return nil
	}
	result := make(map[string]string)
	lines := strings.Split(out, "\n")
	for _, line := range lines {
		if strings.TrimSpace(line) == "" {
			continue
		}
		parts := strings.SplitN(line, "=", 2)
		if len(parts) == 2 {
			key := strings.TrimSpace(parts[0])
			value := strings.TrimSpace(parts[1])
			result[key] = value
		}
	}
	return result
}

// mdExamine examines a device and returns its properties.
func mdExamine(devicePath string, export bool) (map[string]string, error) {
	if export {
		properties := make(map[string]string)
		out, err := runCommand("mdadm", "--examine", "--export", devicePath)
		if err != nil {
			dmsg(DbgSwRaid|DbgAction, "error running mdadm examine export on device %s: %v\n", devicePath, err)
			return properties, err
		}
		lines := strings.Split(out, "\n")
		for _, line := range lines {
			parts := strings.SplitN(line, "=", 2)
			if len(parts) == 2 {
				properties[parts[0]] = parts[1]
			}
		}
		return properties, nil
	}

	data := make(map[string]string)
	out, err := runCommand("mdadm", "--examine", devicePath)
	if err != nil {
		dmsg(DbgSwRaid|DbgAction, "error running mdadm examine on device %s: %v\n", devicePath, err)
		return data, err
	}

	// Split the input into lines
	lines := strings.Split(strings.TrimSpace(out), "\n")
	if len(lines) == 0 {
		return nil, fmt.Errorf("no data returned from mdadm examine for device %s\n", devicePath)
	}

	// First line should contain the device name
	device := strings.TrimSuffix(lines[0], ":")
	if device == "" {
		return nil, fmt.Errorf("failed to determine device from response: %s\n", out)
	}

	// Start processing the rest of the lines
	remainder := out[strings.Index(out, "\n")+1:]

	// Regular expression to match the key-value pairs
	re := regexp.MustCompile(`(\w+(?: \w+){0,2})\s*:\s*([a-zA-Z0-9\-.,: ()=']+)`)
	matches := re.FindAllStringSubmatch(remainder, -1)
	for _, match := range matches {
		if len(match) == 3 {
			key := strings.ToLower(strings.ReplaceAll(match[1], " ", "_"))
			val := match[2]
			if _, exists := data[key]; exists {
				return nil, fmt.Errorf("duplicate key in mdadm regex parsing: %s", key)
			}
			data[key] = val
		}
	}

	return data, nil
}

// normalizeRaidLevel normalizes the RAID level string.
func normalizeRaidLevel(raidLevel string) string {
	switch raidLevel {
	case "stripe":
		return "raid0"
	case "mirror":
		return "raid1"
	}
	if len(raidLevel) <= 2 || raidLevel[0] == '0' || raidLevel[0] == '1' || raidLevel[0] == '4' || raidLevel[0] == '5' || raidLevel[0] == '6' || raidLevel[0] == '9' {
		return "raid" + raidLevel
	}
	return raidLevel
}

// checkRaidLevel checks if the RAID level in detail matches the given RAID level.
func checkRaidLevel(detail map[string]string, raidLevel string) error {
	isValidLevel := false
	for _, level := range validRaidLevels {
		if level == raidLevel {
			isValidLevel = true
			break
		}
	}
	if !isValidLevel {
		return fmt.Errorf("invalid raidlevel: %s. Must be one of: %v", raidLevel, validRaidLevels)
	}

	raidLevel = normalizeRaidLevel(raidLevel)

	actualLevel, exists := detail["MD_LEVEL"]
	if !exists {
		return fmt.Errorf("MD_LEVEL not found in detail")
	}

	if actualLevel != raidLevel {
		return fmt.Errorf("raid device should have level %s but has level %s", raidLevel, actualLevel)
	}

	return nil

}

// checkRaidUuid checks if the RAID UUID matches the expected UUID.
func checkRaidUuid(detail map[string]string, raidDevicePath string) error {
	mdUuid, ok := detail["MD_UUID"]
	if !ok {
		return fmt.Errorf("MD_UUID not found in detail")
	}

	mdPathById := fmt.Sprintf("/dev/disk/by-id/md-uuid-%s", mdUuid)
	resolvedByIdPath, err := filepath.EvalSymlinks(mdPathById)
	if err != nil {
		return fmt.Errorf("unable to resolve md path by id symlink for %s", mdPathById)
	}

	resolvedDevicePath, err := filepath.EvalSymlinks(raidDevicePath)
	if err != nil {
		return fmt.Errorf("unable to resolve md path by id symlink for %s", raidDevicePath)
	}

	if filepath.Clean(resolvedByIdPath) != filepath.Clean(resolvedDevicePath) {
		return fmt.Errorf("mismatch between passed in raid device path: %s and path resolved by symlink: %s", resolvedDevicePath, resolvedByIdPath)
	}

	return nil
}

// checkRaidState checks if the RAID array is in a writable state.
func checkRaidState(raidName string) error {
	arrayState, err := readMdFile(getSysBlockPath(raidName, "md", "array_state"), "")
	if err != nil {
		return err
	}
	for _, state := range readWriteRaidStates {
		if arrayState == state {
			return nil
		}
	}
	return fmt.Errorf("array not in writable state")
}

// checkRaidDegraded checks if the RAID array is in a degraded state.
func checkRaidDegraded(raidName string) error {
	degraded, err := readMdFile(getSysBlockPath(raidName, "md", "degraded"), "")
	if err != nil {
		return err
	}
	if degraded != "" && degraded != "0" {
		return fmt.Errorf("array in degraded state")
	}
	return nil
}

// checkRaidSyncAction checks if the RAID array is in an idle state.
func checkRaidSyncAction(raidName string) error {
	syncAction, err := readMdFile(getSysBlockPath(raidName, "md", "sync_action"), "")
	if err != nil {
		return err
	}
	if syncAction != "" && syncAction != "idle" && syncAction != "resync" {
		return fmt.Errorf("array is not idle. array state: %s", syncAction)
	}
	return nil
}

// checkRaidArray checks various states of the RAID array.
func checkRaidArray(raidName string) error {
	var allErrors error

	if err := checkRaidState(raidName); err != nil {
		allErrors = errors.Join(allErrors, err)
	}

	if err := checkRaidDegraded(raidName); err != nil {
		allErrors = errors.Join(allErrors, err)
	}

	if err := checkRaidSyncAction(raidName); err != nil {
		allErrors = errors.Join(allErrors, err)
	}

	return allErrors
}

// readMdFile reads the content of a given file path, returning a default value if the file does not exist.
func readMdFile(pathToFile, defaultVal string) (string, error) {
	// Check if the file exists and read its content
	if fileExists(pathToFile) {
		attrData, err := os.ReadFile(pathToFile)
		if err != nil {
			return "", err
		}
		return strings.TrimSpace(string(attrData)), nil
	}
	return defaultVal, nil
}

// getSysBlockPath returns the path to a device
func getSysBlockPath(device, additions, attributeName string) string {
	kname, _ := pathToKname(device)
	path := filepath.Join(mdBasePath, kname)
	if additions != "" {
		path = filepath.Join(path, additions)
	}
	if attributeName != "" {
		path = filepath.Join(path, attributeName)
	}
	return filepath.Clean(path)
}

// pathToKname converts a path in /dev or a path in /sys/block to the device kname,
// taking special devices and unusual naming schemes into account
func pathToKname(path string) (string, error) {
	// Check if the path contains a path separator (indicating it's a path, not just a name)
	if strings.Contains(path, string(os.PathSeparator)) {
		// Resolve the real path (resolving symlinks)
		realPath, err := filepath.EvalSymlinks(path)
		if err != nil {
			return "", err
		}
		path = realPath
	}
	// Get the basename of the path (similar to os.path.basename in Python)
	devKname := filepath.Base(path)
	// Check for /dev/cciss and prepend "cciss!" if needed
	if strings.HasPrefix(path, "/dev/cciss") {
		devKname = "cciss!" + devKname
	}
	return devKname, nil
}

// devPath converts a device name to a path in /dev
func devPath(devName string) string {
	if strings.HasPrefix(devName, "/dev/") {
		realPath, err := filepath.EvalSymlinks(devName)
		if err != nil {
			return devName // if resolving symlink fails, return the original path
		}
		return realPath
	}
	return "/dev/" + devName
}

// isSpareDevice checks if the device state indicates a spare device.
func isSpareDevice(deviceState string) bool {
	return deviceState == "spare"
}

// getDevicesOnRaid returns the active and spare devices for a RAID array
func getDevicesOnRaid(raidName string) ([]string, []string, error) {
	var devices []string
	var spareDevices []string
	sysfsMd := getSysBlockPath(raidName, "md", "")

	files, err := os.ReadDir(sysfsMd)
	if err != nil {
		return nil, nil, err
	}

	for _, file := range files {
		name := file.Name()
		if strings.HasPrefix(name, "dev-") {
			statePath := filepath.Join(sysfsMd, name, "state")
			state, err := readMdFile(statePath, "")
			if err != nil {
				return nil, nil, err
			}
			devicePath := devPath(name[4:]) // Strip 'dev-' prefix
			if isSpareDevice(state) {
				spareDevices = append(spareDevices, devicePath)
			} else {
				devices = append(devices, devicePath)
			}
		}
	}
	return devices, spareDevices, nil
}

// checkArrayMembership checks if devices are members of a RAID array.
func checkArrayMembership(detail map[string]string, raidName string, devices, spareDevices []string) error {
	mdUuid, ok := detail["MD_UUID"]
	if !ok {
		return fmt.Errorf("MD_UUID not found in detail")
	}

	var allErrors error
	for _, device := range append(devices, spareDevices...) {
		devExamine, err := mdExamine(device, true)
		if err != nil {
			return err
		}
		devUUID, ok := devExamine["MD_UUID"]
		if !ok {
			dmsg(DbgSwRaid|DbgAction, "device %s is not part of an array\n", device)
			allErrors = errors.Join(allErrors, fmt.Errorf("device is not part of an array: %s", device))
		}
		if devUUID != mdUuid {
			dmsg(DbgSwRaid|DbgAction, "Device %s is not part of %s array. MD_UUID mismatch: device:%s != array:%s\n", device, raidName, devUUID, mdUuid)
			allErrors = errors.Join(allErrors, fmt.Errorf("device %s is not part of %s array. MD_UUID mismatch: device:%s != array:%s", device, raidName, devUUID, mdUuid))
		}
	}

	return allErrors
}

func udevadmSettle() error {
	_, err := runCommand("udevadm", "settle")
	return err
}

func mdadmAssemble(mdDevName string, devices, spares []string) error {
	args := []string{"--assemble"}
	args = append(args, mdDevName, "--run")
	args = append(args, devices...)
	if len(spares) > 0 {
		args = append(args, spares...)
	}

	output, err := runCommand("mdadm", args...)
	if err != nil {
		return err
	}
	dmsg(DbgSwRaid|DbgAction, "mdadm assemble results:\n%s\n", output)

	output, err = runCommand("mdadm", "--detail", "--scan", "-v")
	if err != nil {
		return err
	}
	dmsg(DbgSwRaid|DbgAction, "mdadm detail scan after assemble:\n%s\n", output)

	return udevadmSettle()
}

// mdCheck validates the RAID configuration of the SwRaid instance.
// It performs a series of checks to ensure the RAID array is correctly configured and operational.
// The method checks the following aspects:
// - RAID array state to ensure it is writable.
// - RAID array degraded state to ensure it is not degraded.
// - RAID array sync action to ensure it is idle or resyncing.
// - RAID level to ensure it matches the expected level.
// - RAID UUID to ensure it matches the expected UUID.
// - Membership of devices and spare devices to ensure they are part of the RAID array.
//
// Returns an error if any of the checks fail, or nil if all checks pass.
func (sw *SwRaid) mdCheck() error {
	dmsg(DbgSwRaid|DbgAction, "MD Check happening on: %s, path: %s\n", sw.Name, sw.Path)
	var allErrors error
	if sw.Path == "" {
		return fmt.Errorf("cannot do md check because path is empty")
	}

	detail := mdQueryDetail(sw.Path)
	dmsg(DbgSwRaid|DbgAction, "Got the query details: %v\n", detail)

	if err := checkRaidArray(sw.Path); err != nil {
		allErrors = errors.Join(allErrors, err)
	}

	if err := checkRaidLevel(detail, sw.RaidLevel); err != nil {
		allErrors = errors.Join(allErrors, err)
	}

	if err := checkRaidUuid(detail, sw.Path); err != nil {
		allErrors = errors.Join(err)
	}

	devices, spareDevices, err := getDevicesOnRaid(sw.Path)
	if err != nil {
		allErrors = errors.Join(allErrors, err)
	}
	sw.Devices = devices
	for _, spare := range spareDevices {
		sw.Devices = append(sw.Devices, spare)
	}

	if err := checkArrayMembership(detail, sw.Path, devices, spareDevices); err != nil {
		allErrors = errors.Join(allErrors, err)
	}

	return allErrors
}

func (sw *SwRaid) verifyMdComponents() error {
	var allErrors error
	err := sw.mdCheck()
	if err == nil {
		dmsg(DbgSwRaid|DbgAction, "Verified %s raid composition, raid is OK\n", sw.Name)
		return nil
	}

	allErrors = errors.Join(allErrors, err)
	dmsg(DbgSwRaid|DbgAction, "Assembling preserved raid for %s\n", sw.Name)
	if err = mdadmAssemble(sw.Name, sw.Devices, sw.SpareDevices); err != nil {
		allErrors = errors.Join(allErrors, err)
	} else {
		err = sw.mdCheck()
		if err == nil {
			dmsg(DbgSwRaid|DbgAction, "Verified %s raid composition, raid is OK\n", sw.Name)
			return nil
		}
		allErrors = errors.Join(allErrors, err)
	}

	dmsg(DbgSwRaid|DbgAction, "Verified %s raid composition. Errors: %v\n", sw.Name, allErrors)
	return allErrors
}

func isValidRaidLevel(raidLevel string) bool {
	for _, level := range validRaidLevels {
		if level == raidLevel {
			return true
		}
	}
	return false
}

func isSpareRaidLevel(raidLevel string) bool {
	for _, level := range spareRaidLevels {
		if level == raidLevel {
			return true
		}
	}
	return false
}

func mdMinimumDevices(raidLevel string) int {
	switch raidLevel {
	case "0", "1", "linear", "stripe", "container":
		return 2
	case "5":
		return 3
	case "6", "10":
		return 4
	default:
		return -1
	}
}

func getHolders(device string) ([]string, error) {
	sysfsPath := getSysBlockPath(device, "", "")
	holders, err := os.ReadDir(filepath.Join(sysfsPath, "holders"))
	if err != nil {
		return nil, nil // we don't care if the dir does not exist
	}
	holderNames := make([]string, len(holders))
	for i, holder := range holders {
		holderNames[i] = holder.Name()
	}
	return holderNames, nil
}

func zeroDevice(devPath string) error {
	metadata, _ := mdExamine(devPath, false)

	if metadata == nil {
		dmsg(DbgSwRaid|DbgAction, "%s not mdadm member, force=False so skipping zeroing\n", devPath)
		return nil
	}

	version := metadata["version"]

	var offsets []int64
	if version == "1.1" || version == "1.2" {
		dmsg(DbgSwRaid|DbgAction, "mdadm %s has metadata version=%s, extracting offsets\n", devPath, version)
		for _, field := range []string{"super_offset", "data_offset"} {
			offset, unit := parseOffset(metadata[field])
			if unit == "sectors" {
				offsets = append(offsets, offset*512)
			} else {
				dmsg(DbgSwRaid|DbgAction, "Unexpected offset unit: %s\n", unit)
			}
		}
	}

	if len(offsets) == 0 {
		offsets = []int64{0, -1024 * 1024}
	}

	dmsg(DbgSwRaid|DbgAction, "mdadm: wiping md member %s @ offsets %v\n", devPath, offsets)
	err := zeroFileAtOffsets(devPath, offsets, 1024, 1024, true)
	if err != nil {
		return fmt.Errorf("failed to wipe %s: %v", devPath, err)
	}

	dmsg(DbgSwRaid|DbgAction, "mdadm: successfully wiped %s\n", devPath)
	return nil
}

// zeroFileAtOffsets is directly from the curtin code base just converted to go.
// https://github.com/canonical/curtin/blob/70b56fdf282600c7662da2599e70b6e2955ae680/curtin/block/__init__.py#L1269
func zeroFileAtOffsets(path string, offsets []int64, bufLen int, count int, strict bool) error {
	buf := make([]byte, bufLen)
	total := bufLen * count

	// Open file with exclusive lock
	fp, err := exclusiveOpen(path, true)
	if err != nil {
		return err
	}
	defer fp.Close()

	// Get file size
	size, err := fp.Seek(0, io.SeekEnd)
	if err != nil {
		return fmt.Errorf("failed to seek file: %v", err)
	}

	for _, offset := range offsets {
		pos := offset
		if offset < 0 {
			pos = size + offset
		}

		if pos > size || pos < 0 {
			msg := fmt.Sprintf("%s (size=%d): invalid offset %d.", path, size, offset)
			if strict {
				return fmt.Errorf(msg)
			} else {
				fmt.Println(msg + " Skipping.")
				continue
			}
		}

		if pos+int64(total) > size {
			msg := fmt.Sprintf("%s (size=%d): %d bytes from %d > size.", path, size, total, offset)
			if strict {
				return fmt.Errorf(msg)
			} else {
				fmt.Println(msg+" Shortened to", size-pos, "bytes.")
			}
		}

		_, err := fp.Seek(pos, io.SeekStart)
		if err != nil {
			return fmt.Errorf("failed to seek to position: %v", err)
		}

		for i := 0; i < count; i++ {
			pos, err := fp.Seek(0, io.SeekCurrent)
			if err != nil {
				return fmt.Errorf("failed to get current position: %v", err)
			}

			if pos+int64(bufLen) > size {
				_, err = fp.Write(buf[:size-pos])
			} else {
				_, err = fp.Write(buf)
			}

			if err != nil {
				return fmt.Errorf("failed to write to file: %v", err)
			}
		}
	}
	return nil
}

func exclusiveOpen(path string, exclusive bool) (*os.File, error) {
	flags := os.O_RDWR
	if exclusive {
		flags |= syscall.O_EXCL
	}

	fp, err := os.OpenFile(path, flags, 0600)
	if err != nil {
		if exclusive && err == syscall.EBUSY {
			return nil, fmt.Errorf("failed to exclusively open path: %s, %v", path, err)
		}
		return nil, fmt.Errorf("failed to open file: %v", err)
	}

	return fp, nil
}

func parseOffset(offsetStr string) (int64, string) {
	parts := strings.Split(offsetStr, " ")
	offset, _ := strconv.Atoi(parts[0])
	return int64(offset), parts[1]
}

func (sw *SwRaid) getDevicePathsFromConfig(inputDisks Disks) ([]string, []string, error) {
	devicePathMap := make(map[string]string)
	for _, disk := range inputDisks {
		devicePathMap[disk.Name] = disk.Path
		devicePathMap[disk.Path] = disk.Path
		for _, part := range disk.Partitions {
			devicePathMap[part.Name] = GeneratePartitionPath(disk.Path, part.ID)
		}
	}

	var deviceList []string
	var spareDeviceList []string

	for _, d := range sw.Devices {
		if path, ok := devicePathMap[d]; ok {
			deviceList = append(deviceList, path)
		} else {
			return deviceList, spareDeviceList, fmt.Errorf("device %s which is a part of the swraid %s does not have a configuration section defined", d, sw.Name)
		}
	}

	for _, d := range sw.SpareDevices {
		if path, ok := devicePathMap[d]; ok {
			spareDeviceList = append(deviceList, path)
		} else {
			return deviceList, spareDeviceList, fmt.Errorf("device %s which is a part of the swraid spare devices %s does not have a configuration section defined", d, sw.Name)
		}
	}

	return deviceList, spareDeviceList, nil
}

func (sw *SwRaid) mdCreate(inputDisks Disks) error {
	dmsg(DbgSwRaid|DbgAction, "mdCreate: name=%s raidLevel=%s devices=%v spares=%v name=%s\n", sw.Path, sw.RaidLevel, sw.Devices, sw.SpareDevices, sw.Name)
	if sw.Metadata == "" {
		sw.Metadata = "default"
	}

	if !isValidRaidLevel(sw.RaidLevel) {
		return fmt.Errorf("invalid raid level: [%s]", sw.RaidLevel)
	}

	minDevices := mdMinimumDevices(sw.RaidLevel)
	if len(sw.Devices) < minDevices {
		return fmt.Errorf("not enough devices (%d) for raid level: %s, minimum devices needed: %d", len(sw.Devices), sw.RaidLevel, minDevices)
	}

	if len(sw.SpareDevices) > 0 && !isSpareRaidLevel(sw.RaidLevel) {
		return fmt.Errorf("raid level %s does not support spare devices", sw.RaidLevel)
	}

	// Get all the device information from the input config
	// if we only have the names, we want to get the paths
	// if we already have the path then we can just use those
	deviceList, spareDeviceList, err := sw.getDevicePathsFromConfig(inputDisks)
	if err != nil {
		return fmt.Errorf("found a device on raid which has configuration missing. %v", err)
	}

	hostname, err := runCommand("hostname", "-s")
	if err != nil {
		return err
	}
	hostname = strings.TrimSpace(hostname)

	args := []string{"--create", sw.Name, "--run", "--homehost=" + hostname}
	args = append(args, "--raid-devices="+fmt.Sprintf("%d", len(deviceList)))
	args = append(args, "--metadata="+sw.Metadata)
	args = append(args, "--level="+sw.RaidLevel)

	args = append(args, deviceList...)
	if len(spareDeviceList) > 0 {
		args = append(args, "--spare-devices="+fmt.Sprintf("%d", len(spareDeviceList)))
		args = append(args, spareDeviceList...)
	}

	for _, device := range append(deviceList, spareDeviceList...) {
		holders, err := getHolders(device)
		if err != nil {
			return err
		}
		if len(holders) > 0 {
			return fmt.Errorf("device %s has holders: %v", device, holders)
		}
		if err := zeroDevice(device); err != nil {
			return err
		}
	}

	if err := udevadmSettle(); err != nil {
		return err
	}
	if _, err := runCommand("udevadm", "control", "--stop-exec-queue"); err != nil {
		return err
	}

	if _, err := runCommand("mdadm", args...); err != nil {
		dmsg(DbgSwRaid|DbgAction, "mdadm_create failed - extra debug regarding md modules. error: %v", err)

		out, _ := runCommand("lsmod")
		dmsg(DbgSwRaid|DbgAction, "modules loaded: \n%s\n", out)

		kernelVersion, _ := getKernelVersion()
		raidModulePath := fmt.Sprintf("/lib/modules/%s/kernel/drivers/md", kernelVersion)
		out, _ = runCommand("find", raidModulePath)
		if out != "" {
			dmsg(DbgSwRaid|DbgAction, "available md modules: \n%s", out)
		} else {
			dmsg(DbgSwRaid|DbgAction, "no available md modules found")
		}

		for _, device := range append(sw.Devices, sw.SpareDevices...) {
			holders, _ := getHolders(device)
			dmsg(DbgSwRaid|DbgAction, "Device %s has holders: %v", device, holders)
		}
		return err
	}
	runCommand("udevadm", "control", "--start-exec-queue")
	udevadmSettle()

	// Raid has been created. We can determine the path by the name
	// If name starts with /dev/md (it can be /dev/md<n> or /dev/md/abc) then that will be the path
	// If the name was a string - then we need to append /dev/md/<string>
	if strings.HasPrefix("/dev/md", sw.Name) {
		sw.Path = sw.Name
	} else {
		sw.Path = fmt.Sprintf("/dev/md/%s", sw.Name)
	}

	return nil
}

// Action applies the configuration in the comparator to the
// actual SwRaid.
func (sw *SwRaid) Action(c Comparator, inputDisks Disks) (Result, error) {
	if sw.success {
		dmsg(DbgSwRaid|DbgAction, "SW Raid %s applied successfully. Not reapplying.\n", sw.Name)
		return ResultSuccess, nil
	}
	swi := c.(*SwRaid)
	dmsg(DbgSwRaid|DbgAction, "SWRaid (%s,%s) action\n", sw.Name, swi.Name)

	if !sw.imageApplied {
		if swi.Image != nil {
			// Put image in place
			swi.Image.Path = sw.Path
			if err := swi.Image.Action(); err != nil {
				return ResultFailed, err
			}
			sw.imageApplied = true
			return ResultRescan, nil
		}
	}

	// Normalize partition table
	sw.PTable = normalizePTable(sw.PTable)
	swi.PTable = normalizePTable(swi.PTable)

	// If 'Preserve' is set to true in the incoming configuration, verify the new RAID components
	if swi.Preserve {
		dmsg(DbgSwRaid|DbgAction, "Doing verify on: %s\n", swi.Name)
		if err := swi.verifyMdComponents(); err != nil {
			return ResultFailed, fmt.Errorf("preserve is set but the RAID verify failed: %v", err)
		}
		// If preservation is successful, no need to create the RAID array again
		return ResultSuccess, nil
	}

	// If 'Preserve' is not set or verification failed, create the RAID array
	dmsg(DbgSwRaid|DbgAction, "Doing mdcreate on: %s\n", swi.Name)

	if err := swi.mdCheck(); err == nil {
		dmsg(DbgSwRaid|DbgAction, "Verified %s raid composition, raid is OK, not creating\n", swi.Name)
	} else {
		dmsg(DbgSwRaid|DbgAction, "MD Check failed on: %s due to %v. Creating raid...\n", swi.Name, err)
		if err := swi.mdCreate(inputDisks); err != nil {
			return ResultFailed, fmt.Errorf("failed to create RAID array: %v", err)
		}
	}

	if swi.PTable != "" {
		if !sw.labelApplied && swi.PTable != sw.PTable {
			dmsg(DbgSwRaid|DbgAction, "Applying partition lable. Input label: %s Scanned label: %s\n", swi.PTable, sw.PTable)
			sw.labelApplied = true
			return ResultRescan, runParted(swi.Path, "mklabel", swi.PTable)
		}

		// Ensure partition table is correct.
		if swi.PTable != sw.PTable {
			return ResultFailed, fmt.Errorf("swraid, %s, disk label doesn't match (A: %s C: %s)", swi.Name, sw.PTable, swi.PTable)
		}
	}

	// Handle partition creation if specified
	for i, pi := range swi.Partitions {
		dmsg(DbgSwRaid, "Working on %d partition: %v\n", i, pi)
		found := false
		for _, p := range sw.Partitions {
			dmsg(DbgSwRaid, "on scanned partition: %v\n", p)
			if p.Equal(pi) {
				dmsg(DbgSwRaid, "Found matching partition. %v, %v\n", p, pi)
				r, e := p.Action(pi)
				if e != nil || r == ResultRescan {
					return r, e
				}
				found = true
				break
			}
		}
		if !found {
			// We just recreated a partition, rescan.
			// This is overkill but safe.
			p := &Partition{parent: swi}
			r, e := p.Action(pi)
			if e != nil {
				return r, e
			}
			return ResultRescan, nil
		}
	}
	sw.success = true
	return ResultSuccess, nil
}

func (sw *SwRaid) String() string {
	if sw == nil {
		return "unset"
	}
	ps := ""
	comma := ""
	for _, p := range sw.Partitions {
		ps += fmt.Sprintf("%s%v", comma, p)
		comma = ","
	}
	return fmt.Sprintf("<name:%s path:%s image:%v wipe:%v grubdev:%v ptable:%s parts:%s size:%s fs:%v imageapplied:%v labelapplied:%v nextStart:%s>",
		sw.Name,
		sw.Path,
		sw.Image,
		sw.Wipe,
		sw.GrubDevice,
		sw.PTable,
		ps,
		sw.Size,
		sw.FileSystem,
		sw.imageApplied,
		sw.labelApplied,
		sw.nextStart)
}
