package main

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestAllConversions(t *testing.T) {
	input := `{
        "storage": {
            "config": [
                {"id": "sda", "type": "disk", "ptable": "gpt", "name": "main_disk", "path": "/dev/sda"},
                {"id": "sdb", "type": "disk", "ptable": "gpt", "name": "second_disk", "path": "/dev/sdb"},
                {"id": "sda1", "type": "partition", "size": "4GB", "device": "sda", "number": 1},
                {"id": "sda2", "type": "partition", "size": "2GB", "device": "sda", "number": 2},
                {"id": "md0", "type": "raid", "name": "raid0", "raidlevel": "raid1", "devices": ["sdb", "sda2"]},
                {"id": "vg0", "type": "lvm_volgroup", "name": "vg0", "devices": ["sda1", "md0"]},
                {"id": "vg0-lv1", "type": "lvm_partition", "name": "root", "size": "3GB", "volgroup": "vg0"},
                {"id": "vg0-lv2", "type": "lvm_partition", "name": "home", "size": "1GB", "volgroup": "vg0"},
                {"id": "vg0-lv1_fs", "type": "format", "fstype": "ext4", "volume": "vg0-lv1", "label": "root"},
                {"id": "vg0-lv2_fs", "type": "format", "fstype": "ext4", "volume": "vg0-lv2", "label": "home"},
                {"id": "vg0-lv1_mount", "type": "mount", "path": "/", "device": "vg0-lv1_fs"},
                {"id": "vg0-lv2_mount", "type": "mount", "path": "/home", "device": "vg0-lv2_fs"}
            ],
            "version": 1
        },
        "sources": [
            "http://example.com/ubuntu.squashfs",
            "dd-img: https://example.com/centos.img",
            "dd-bz2: https://example.com/data.img.bz2"
        ],
        "swap": {
            "filename": "/swap.img",
            "size": "4GB",
            "maxsize": "8GB",
            "force": true
        }
    }`

	expected := `{
        "disks": [
            {
                "name": "main_disk",
                "path": "/dev/sda",
                "ptable": "gpt",
                "partitions": [
                    {
                        "name": "sda1",
                        "size": "4294967296B"
                    },
                    {
                        "name": "sda2",
                        "size": "2147483648B"
                    }
                ]
            },
            {
                "name": "second_disk",
                "path": "/dev/sdb",
                "ptable": "gpt"
            }
        ],
        "swraids": [
            {
                "name": "raid0",
                "raid_level": "raid1",
                "devices": ["sdb", "sda2"],
                "metadata": "",
                "ptable": ""
            }
        ],
        "vgs": [
            {
                "name": "vg0",
                "pvs": ["sda1", "md0"],
                "lvs": [
                    {
                        "name": "root",
                        "size": "3221225472B",
                        "fs": {
                            "fstype": "ext4",
                            "label": "root",
                            "mount": "/"
                        }
                    },
                    {
                        "name": "home",
                        "size": "1073741824B",
                        "fs": {
                            "fstype": "ext4",
                            "label": "home",
                            "mount": "/home"
                        }
                    }
                ]
            }
        ],
        "images": [
            {
                "url": "http://example.com/ubuntu.squashfs",
                "type": "tar"
            },
            {
                "url": "https://example.com/centos.img",
                "type": "dd-img"
            },
            {
                "url": "https://example.com/data.img.bz2",
                "type": "dd-bz2"
            }
        ],
        "swap": {
            "filename": "/swap.img",
            "size": "4GB",
            "maxsize": "8GB",
            "force": true
        }
    }`

	var cc CurtinConfig
	if err := json.Unmarshal([]byte(input), &cc); err != nil {
		t.Fatalf("Failed to parse input JSON: %v", err)
	}

	config := &Config{}
	config.convertToEikonPlan(&cc)

	// Convert actual result to map
	got, err := json.Marshal(config)
	if err != nil {
		t.Fatalf("Failed to marshal result: %v", err)
	}
	var gotMap map[string]interface{}
	if err := json.Unmarshal(got, &gotMap); err != nil {
		t.Fatalf("Failed to unmarshal result into map: %v", err)
	}

	// Convert expected to map
	var expectedMap map[string]interface{}
	if err := json.Unmarshal([]byte(expected), &expectedMap); err != nil {
		t.Fatalf("Failed to unmarshal expected into map: %v", err)
	}

	// Compare the maps
	if !reflect.DeepEqual(gotMap, expectedMap) {
		// For debugging, print the differences
		gotJSON, _ := json.MarshalIndent(gotMap, "", "    ")
		expectedJSON, _ := json.MarshalIndent(expectedMap, "", "    ")
		t.Errorf("\nexpected:\n%s\n\ngot:\n%s", string(expectedJSON), string(gotJSON))
	}
}
