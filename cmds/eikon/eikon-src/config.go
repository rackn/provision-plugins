package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	yaml "github.com/ghodss/yaml"
)

var format = "yaml"
var nameReplacer = strings.NewReplacer(" ", "-", "\t", "-")

// Config contains both the actual scan and the desired config.
// These are held one or the other.  The system will difference
// the two and generate call actions on the parts that are different.
type Config struct {
	// SwaRaids define the sw raid config
	SwRaids SwRaids `json:"swraids,omitempty"`

	// Disks define the disks and partitions in the system.
	Disks Disks `json:"disks,omitempty"`

	// VolumeGroups define the LVM components in the system
	VolumeGroups VolumeGroups `json:"vgs,omitempty"`

	// Images define what should be added to the system after the
	// construction of everything.
	Images Images `json:"images,omitempty"`

	SwapFile *SwapFile `json:"swap,omitempty"`
}

// ScanSystem scans all the components of the system and rebuilds this
// config object with that data.
func (c *Config) ScanSystem() error {
	if e := udevUpdate(); e != nil {
		return fmt.Errorf("error: udev %w", e)
	}

	disks, e := ScanDisks()
	if e != nil {
		return fmt.Errorf("error: disk %w", e)
	}

	srs, e := ScanRaids()
	if e != nil {
		return fmt.Errorf("error: swraid %w", e)
	}

	vgs, e := ScanLVM()
	if e != nil {
		return fmt.Errorf("error: lvm %w", e)
	}

	// Create a mapping of disk path and partition path to disk
	//
	// Then we can find the corresponding pv.Name matching
	// and get the pv.parentPath
	diskMap := make(map[string]string)
	for _, d := range disks {
		diskMap[d.Path] = d.Name
		for _, p := range d.Partitions {
			partitionPath := GeneratePartitionPath(d.Path, p.ID)
			diskMap[partitionPath] = p.Name
		}
	}

	raidMap := make(map[string]string)
	for _, r := range srs {
		raidMap[r.Path] = r.Name
		for _, p := range r.Partitions {
			partitionPath := GeneratePartitionPath(r.Path, p.ID)
			diskMap[partitionPath] = p.Name
		}
	}

	for _, vg := range vgs {
		var devices []string
		for _, pv := range vg.physicalVolumes {
			if d, exists := diskMap[pv.Name]; exists {
				devices = append(devices, d)
				pv.parentPath = pv.Name
			} else if r, exists := raidMap[pv.Name]; exists {
				devices = append(devices, r)
				pv.parentPath = pv.Name
			}
		}
		vg.Devices = devices
	}

	if e := ScanFS(disks, vgs, srs); e != nil {
		return fmt.Errorf("error: fs %w", e)
	}

	dmsg(DbgScan, "Disks:\n")
	for i, d := range disks {
		dmsg(DbgScan, "Disk number %d: %+v\n", i, *d)
	}
	dmsg(DbgScan, "vgs:\n")
	for i, v := range vgs {
		dmsg(DbgScan, "VG number %d: %+v\n", i, *v)
	}
	dmsg(DbgScan, "SWRaids:\n")
	for i, r := range srs {
		dmsg(DbgScan, "Raid number %d: %+v\n", i, *r)
	}
	return c.Import(disks, vgs, srs)
}

// Import merges the scanned data into the current config object.
func (c *Config) Import(disks Disks, vgs VolumeGroups, swRaids SwRaids) error {
	dmsg(DbgScan, "Import:")
	var err error
	c.Disks, err = ToDiskList(MergeList(c.Disks.toCompList(), disks.toCompList()))
	if err != nil {
		return fmt.Errorf("error merging disks: %w", err)
	}
	c.VolumeGroups, err = ToVolumeGroupList(MergeList(c.VolumeGroups.toCompList(), vgs.toCompList()))
	if err != nil {
		return fmt.Errorf("error merging volume groups: %w", err)
	}
	c.SwRaids, err = ToSwRaidList(MergeList(c.SwRaids.toCompList(), swRaids.toCompList()))
	if err != nil {
		return fmt.Errorf("error merging swraids: %w", err)
	}

	// Hook'em together - try to find the devices that become pvs.
	for _, vg := range c.VolumeGroups {
		dmsg(DbgScan, "VolumeGroup Hook Up: %s\n", vg.Name)

		for _, pv := range vg.physicalVolumes {
			p := disks.FindPartitionByPath(pv.Name)
			if p == nil {
				p = swRaids.FindPartitionByPath(pv.Name)
				if p == nil {
					return fmt.Errorf("pv: cannot find %s in the parts", pv)
				}
			}

			pvNew := &PhysicalVolume{
				Name:       p.Name,
				parentPath: GeneratePartitionPath(p.parent.GetPath(), p.ID),
				usingVG:    vg,
			}
			p.physicalVolume = pvNew
		}
	}
	return nil
}

// isCurtinConfig checks to see if there is a `storage` key in the config passed in.
// that is only present in a curtin config and not an eikon plan.
func isCurtinConfig(data []byte) (bool, error) {
	var parsedData map[string]interface{}
	// Try parsing as YAML (handles both YAML and JSON)
	if err := yaml.Unmarshal(data, &parsedData); err != nil {
		return false, fmt.Errorf("file is not YAML or JSON: %w", err)
	}
	if _, exists := parsedData["storage"]; exists {
		return true, nil
	}
	return false, nil
}

// ReadConfig process the file referenced by filename and validates
// its contents for correctness.
func (c *Config) ReadConfig(filename string) error {
	byteValue, err := os.ReadFile(filename)
	if err != nil {
		return fmt.Errorf("error reading file: %s. %w", filename, err)
	}

	curtinConfig, err := isCurtinConfig(byteValue)
	if err != nil {
		return fmt.Errorf("file %s. %w", filename, err)
	}

	// Check if the config received is a curtin config; if yes, convert to eikon plan
	if curtinConfig {
		cc := &CurtinConfig{}
		if err = yaml.Unmarshal(byteValue, &cc); err != nil {
			return fmt.Errorf("error unmarshalling YAML from %s: %w", filename, err)
		}
		dmsg(DbgCurtinConversion, "A curtin config has been provided\n")
		// This is a valid curtin config, so we are going to try to convert it
		err := c.convertToEikonPlan(cc)
		if err != nil {
			return err
		}
	} else {
		dmsg(DbgCurtinConversion, "An eikon/plan has been provided.\n")
		if err = yaml.Unmarshal(byteValue, c); err != nil {
			return fmt.Errorf("error unmarshalling YAML from %s: %w", filename, err)
		}

		// Set parents on partitions
		for _, d := range c.Disks {
			for _, p := range d.Partitions {
				p.parent = d
			}
		}
		for _, r := range c.SwRaids {
			for _, p := range r.Partitions {
				p.parent = r
			}
		}

		// Hook'em together - try to find the devices that become pvs.
		for _, vg := range c.VolumeGroups {
			for _, dev := range vg.Devices {
				obj := c.Disks.FindByName(dev)
				if obj == nil {
					obj = c.SwRaids.FindByName(dev)
					if obj == nil {
						return fmt.Errorf("sw: cannot find %s in the parts", dev)
					}
				}

				p, ok := obj.(*Partition)
				if !ok {
					return fmt.Errorf("PhysicaVolume only allowed on partitions: %v", obj)
				}

				pv := &PhysicalVolume{Name: p.Name, usingVG: vg, parentPath: GeneratePartitionPath(p.parent.GetPath(), p.ID)}
				p.physicalVolume = pv
				vg.physicalVolumes = append(vg.physicalVolumes, pv)
			}
		}
	}

	err = c.Validate()
	if err != nil {
		return fmt.Errorf("error validating current config. %w", err)
	}

	return nil
}

// Validate iteratively calls Validate on all the objects in the
// config structure.  Returns errors as appropriate.
func (c *Config) Validate() error {
	var allErrors error
	for _, d := range c.Disks {
		if err := d.Validate(); err != nil {
			allErrors = errors.Join(allErrors, err)
		}
	}
	for _, sw := range c.SwRaids {
		if err := sw.Validate(); err != nil {
			allErrors = errors.Join(allErrors, err)
		}
	}
	for _, vg := range c.VolumeGroups {
		if err := vg.Validate(); err != nil {
			allErrors = errors.Join(allErrors, err)
		}
	}
	for _, img := range c.Images {
		img.tarOnly = true
		if err := img.Validate(); err != nil {
			allErrors = errors.Join(allErrors, err)
		}
	}
	if c.SwapFile != nil {
		dmsg(DbgConfig|DbgSwap, "Loaded swap config: filename=%s size=%s maxsize=%s force=%v\n",
			c.SwapFile.Filename, c.SwapFile.Size, c.SwapFile.Maxsize, c.SwapFile.Force)
		if err := c.SwapFile.Validate(); err != nil {
			allErrors = errors.Join(allErrors, err)
		}
	}

	return allErrors
}

// Helper function to find the parent path for a partition
func (c *Config) findParentPath(partitionName string) string {
	// Search in SwRaids
	for _, r := range c.SwRaids {
		for _, p := range r.Partitions {
			if partitionName == p.Name {
				return GeneratePartitionPath(p.parent.GetPath(), p.ID)
			}
		}
	}
	// Search in Disks
	for _, d := range c.Disks {
		for _, p := range d.Partitions {
			if partitionName == p.Name {
				return GeneratePartitionPath(p.parent.GetPath(), p.ID)
			}
		}
	}
	return ""
}

// Apply config file to a discovered config (ic is config file)
func (c *Config) Apply(ic *Config) (Result, error) {
	dmsg(DbgConfig, "Applying disk configuration\n")
	for _, nd := range ic.Disks {
		found := false
		for _, d := range c.Disks {
			if d.Equal(nd) {
				if rs, err := d.Action(nd); err != nil {
					return rs, err
				} else if rs == ResultRescan {
					return rs, nil
				}
				found = true
				break
			}
		}
		if !found {
			return ResultFailed, fmt.Errorf("failed to find disk %v", nd)
		}
	}

	dmsg(DbgConfig, "Applying sw raid configuration\n")
	for _, nsw := range ic.SwRaids {
		found := false
		for _, sw := range c.SwRaids {
			if sw.Equal(nsw) {
				if rs, err := sw.Action(nsw, ic.Disks); err != nil {
					return rs, err
				} else if rs == ResultRescan {
					return rs, nil
				}
				found = true
				break
			}
		}
		if !found {
			// Create the raid
			sw := &SwRaid{}
			if rs, err := sw.Action(nsw, ic.Disks); err != nil {
				return rs, err
			} else if rs == ResultRescan {
				return rs, nil
			}
		}
	}

	// TODO: Do luks devices like swraid here
	// TODO: luks devices don't exist just like swraid devices and will need to be created

	// TODO: vgcreate and pvs need to find the other items by name

	dmsg(DbgConfig, "Applying volume group configuration\n")
	for _, nvg := range ic.VolumeGroups {
		var vg *VolumeGroup
		for _, t := range c.VolumeGroups {
			if t.Equal(nvg) {
				vg = t
				break
			}
		}
		if vg == nil {
			vg = &VolumeGroup{}
		}
		// Make sure the parent paths are all present
		for _, pv := range nvg.physicalVolumes {
			if pv.parentPath == "" {
				pv.parentPath = ic.findParentPath(pv.Name)
			}
		}

		if rs, err := vg.Action(nvg); err != nil {
			return rs, err
		} else if rs == ResultRescan {
			return rs, nil
		}
	}

	dmsg(DbgConfig, "Mounting all to \"\" \n")
	_, baseDir, err := c.MountAll("")
	if err != nil {
		return ResultFailed, err
	}

	dmsg(DbgConfig, "Applying image configuration\n")
	for _, image := range ic.Images {
		image.Path = fmt.Sprintf("%s%s", baseDir, image.Path)
		if err := image.Action(); err != nil {
			return ResultFailed, err
		}
	}

	return c.UnmountAll(baseDir)
}

func (c *Config) MountAll(dirPath string) (Result, string, error) {
	answer, err := mountAll(dirPath, c)
	if err != nil {
		return ResultFailed, "", err
	}
	return ResultSuccess, answer, nil
}

func (c *Config) UnmountAll(dirPath string) (Result, error) {
	err := unmountAll(dirPath, c)
	if err != nil {
		return ResultFailed, err
	}
	return ResultSuccess, nil
}

// Dump prints to stdout a yaml or json dump of the config object.
func (c *Config) Dump() {
	switch format {
	case "json":
		js, err := json.MarshalIndent(c, "", "  ")
		if err != nil {
			fmt.Printf("%v\n", err)
			return
		}
		fmt.Println(string(js))
	case "yaml", "yml":
		y, err := yaml.Marshal(c)
		if err != nil {
			fmt.Printf("%v\n", err)
			return
		}
		fmt.Println(string(y))
	default:
		fmt.Printf("Unsupported format\n")
	}
}
