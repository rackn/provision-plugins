package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

// FileSystem defines a File System in the system.
// Either the actual or the configured choice.
type FileSystem struct {
	Name string `json:"name,omitempty"`
	Type string `json:"fstype,omitempty"`

	Label string `json:"label,omitempty"`
	Force bool   `json:"force,omitempty"`
	Quick bool   `json:"quick,omitempty"`
	Quiet bool   `json:"quiet,omitempty"`
	UUID  string `json:"uuid,omitempty"`

	Mount string `json:"mount,omitempty"`

	Resize bool `json:"resize,omitempty"`

	Image *Image `json:"image,omitempty"`

	parentPath   string
	imageApplied bool
	// volume has been added for ease with converting a curtin
	// config to an eikon plan
	volume string
}

var fsCount = 0

// updateBlkInfo updates the filesystem with info from lsblk
func (fs *FileSystem) updateBlkInfo(keys map[string]string) error {
	if val, ok := keys["FSTYPE"]; ok {
		fs.Type = val
	}
	if val, ok := keys["UUID"]; ok {
		fs.UUID = val
	}
	if val, ok := keys["MOUNTPOINT"]; ok {
		fs.Mount = val
	}
	if val, ok := keys["LABEL"]; ok {
		fs.Label = val
	}
	if fs.Type == "swap" {
		fs.Mount = ""
	}
	if fs.Name == "" {
		if fs.Mount != "" {
			fs.Name = strings.ReplaceAll(fs.Mount, " ", "-")
			if fs.Name == "/" {
				fs.Name = "root"
			} else {
				fs.Name = strings.ReplaceAll(fs.Name, "/", "-")
			}
			fs.Name = "fs-" + fs.Name
		} else {
			fs.Name = fmt.Sprintf("fs-%d", fsCount)
			fsCount++
		}
	}

	return nil
}

// Equal tests identity equivalence
func (fs *FileSystem) Equal(c Comparator) bool {
	nfs := c.(*FileSystem)
	return fs.Name == nfs.Name
}

// Merge merges the actual comparator into the current FileSystem
func (fs *FileSystem) Merge(c Comparator) error {
	nfs, _ := c.(*FileSystem)

	if nfs == nil {
		return nil
	}

	fs.Name = nfs.Name
	fs.Type = nfs.Type
	fs.Label = nfs.Label
	fs.UUID = nfs.UUID
	if fs.parentPath == "" {
		fs.parentPath = nfs.parentPath
	}

	return nil
}

// Validate a FileSystem object.
func (fs *FileSystem) Validate() error {
	var allErrors error

	if fs.Name == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("filesystem %s must have a name", fs.Name))
	}
	if fs.Type == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("filesystem %s must have a type", fs.Name))
	}

	if fs.Type != "swap" && fs.Mount != "" && !strings.HasPrefix(fs.Mount, "/") {
		allErrors = errors.Join(allErrors, fmt.Errorf("filesystem %s mount %s must start with /", fs.Name, fs.Mount))
	}

	if fs.Image != nil {
		fs.Image.tarOnly = true
		if e := fs.Image.Validate(); e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("filesystem %s: Image is invalid: %v", fs.Name, e))
		}
	}

	return allErrors
}

// MkfsData defines the args and options for running the various mkfs commands
type MkfsData struct {
	Command        string
	Family         string
	LabelLen       int
	ExtraArgs      []string
	LabelArgs      []string
	ForceArgs      []string
	QuietArgs      []string
	UUIDArgs       []string
	SectorSizeArgs []string
	QuickArgs      []string
	ResizeCommand  string
}

var mkfsdata = map[string]MkfsData{
	"btrfs": {
		Command:        "mkfs.btrfs",
		Family:         "",
		LabelLen:       256,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"--force"},
		LabelArgs:      []string{"--label"},
		QuietArgs:      []string{},
		UUIDArgs:       []string{"--uuid"},
		SectorSizeArgs: []string{"--sector-size"},
		QuickArgs:      []string{},
		ResizeCommand:  "",
	},
	"ext2": {
		Command:        "mkfs.ext2",
		Family:         "ext",
		LabelLen:       16,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"-F"},
		LabelArgs:      []string{"-L"},
		QuietArgs:      []string{"-q"},
		UUIDArgs:       []string{"-U"},
		SectorSizeArgs: []string{"-b"},
		QuickArgs:      []string{},
		ResizeCommand:  "resize2fs",
	},
	"ext3": {
		Command:        "mkfs.ext3",
		Family:         "ext",
		LabelLen:       16,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"-F"},
		LabelArgs:      []string{"-L"},
		QuietArgs:      []string{"-q"},
		UUIDArgs:       []string{"-U"},
		SectorSizeArgs: []string{"-b"},
		QuickArgs:      []string{},
		ResizeCommand:  "resize2fs",
	},
	"ext4": {
		Command:        "mkfs.ext4",
		Family:         "ext",
		LabelLen:       16,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"-F"},
		LabelArgs:      []string{"-L"},
		QuietArgs:      []string{"-q"},
		UUIDArgs:       []string{"-U"},
		SectorSizeArgs: []string{"-b"},
		QuickArgs:      []string{},
		ResizeCommand:  "resize2fs",
	},
	"fat": {
		Command:        "mkfs.vfat",
		Family:         "fat",
		LabelLen:       11,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"-I"},
		LabelArgs:      []string{"-n"},
		QuietArgs:      []string{},
		UUIDArgs:       []string{},
		SectorSizeArgs: []string{"-S"},
		QuickArgs:      []string{},
		ResizeCommand:  "fatresize",
	},
	"fat12": {
		Command:        "mkfs.vfat",
		Family:         "fat",
		LabelLen:       11,
		ExtraArgs:      []string{"-F", "12"},
		ForceArgs:      []string{"-I"},
		LabelArgs:      []string{"-n"},
		QuietArgs:      []string{},
		UUIDArgs:       []string{},
		SectorSizeArgs: []string{"-S"},
		QuickArgs:      []string{},
		ResizeCommand:  "fatresize",
	},
	"fat16": {
		Command:        "mkfs.vfat",
		Family:         "fat",
		LabelLen:       11,
		ExtraArgs:      []string{"-F", "16"},
		ForceArgs:      []string{"-I"},
		LabelArgs:      []string{"-n"},
		QuietArgs:      []string{},
		UUIDArgs:       []string{},
		SectorSizeArgs: []string{"-S"},
		QuickArgs:      []string{},
		ResizeCommand:  "fatresize",
	},
	"fat32": {
		Command:        "mkfs.vfat",
		Family:         "fat",
		LabelLen:       11,
		ExtraArgs:      []string{"-F", "32"},
		ForceArgs:      []string{"-I"},
		LabelArgs:      []string{"-n"},
		QuietArgs:      []string{},
		UUIDArgs:       []string{},
		SectorSizeArgs: []string{"-S"},
		QuickArgs:      []string{},
		ResizeCommand:  "fatresize",
	},
	"vfat": {
		Command:        "mkfs.vfat",
		Family:         "fat",
		LabelLen:       11,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"-I"},
		LabelArgs:      []string{"-n"},
		QuietArgs:      []string{},
		UUIDArgs:       []string{},
		SectorSizeArgs: []string{"-S"},
		QuickArgs:      []string{},
		ResizeCommand:  "fatresize",
	},
	"jfs": {
		Command:        "jfs_mkfs",
		Family:         "",
		LabelLen:       16,
		ExtraArgs:      []string{},
		LabelArgs:      []string{"-L"},
		QuietArgs:      []string{},
		UUIDArgs:       []string{},
		SectorSizeArgs: []string{},
		QuickArgs:      []string{},
		ResizeCommand:  "mount -o remount,resize /mount/point; umount /mount/point", // TODO: !!
	},
	"ntfs": {
		Command:        "mkntfs",
		Family:         "",
		LabelLen:       32,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"--force"},
		LabelArgs:      []string{"--label"},
		QuietArgs:      []string{"-q"},
		UUIDArgs:       []string{},
		SectorSizeArgs: []string{"--sector-size"},
		QuickArgs:      []string{"-Q"},
		ResizeCommand:  "ntfsresize -x path",
	},
	"swap": {
		Command:        "mkswap",
		Family:         "",
		LabelLen:       15,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"--force"},
		LabelArgs:      []string{"--label"},
		QuietArgs:      []string{},
		UUIDArgs:       []string{"--uuid"},
		SectorSizeArgs: []string{},
		QuickArgs:      []string{},
		ResizeCommand:  "", // TODO: !!
	},
	"xfs": {
		Command:        "mkfs.xfs",
		Family:         "",
		LabelLen:       12,
		ExtraArgs:      []string{},
		ForceArgs:      []string{"-f"},
		LabelArgs:      []string{"-L"},
		QuietArgs:      []string{"--quiet"},
		UUIDArgs:       []string{"-m"},
		SectorSizeArgs: []string{"-s"},
		QuickArgs:      []string{},
		ResizeCommand:  "xfs_growfs /mount/point", // TODO: !!
	},
}

// Action applies the configuration object to the actual object.
func (fs *FileSystem) Action(nfs *FileSystem) (Result, error) {
	dmsg(DbgFs|DbgAction, "FileSystem Action %v %v\n", fs, nfs)
	if nfs.Type == "LVM2_member" || nfs.Type == "linux_raid_member" || nfs.Type == "linux_raid" {
		return ResultSuccess, nil
	}

	// Make sure the actual can mount later
	fs.Mount = nfs.Mount

	// We are creating
	if fs.Type == "" {
		data, ok := mkfsdata[nfs.Type]
		if !ok {
			return ResultFailed, fmt.Errorf("%s: Unknown type: %s", nfs.Name, nfs.Type)
		}

		args := []string{}

		args = append(args, data.ExtraArgs...)
		if nfs.Force && len(data.ForceArgs) > 0 {
			args = append(args, data.ForceArgs...)
		}
		if nfs.Quiet && len(data.QuietArgs) > 0 {
			args = append(args, data.QuietArgs...)
		}
		if nfs.Quick && len(data.QuickArgs) > 0 {
			args = append(args, data.QuickArgs...)
		}
		if nfs.Label != "" && len(data.LabelArgs) > 0 {
			args = append(args, data.LabelArgs...)
			args = append(args, nfs.Label)
		}
		if nfs.UUID != "" && len(data.UUIDArgs) > 0 {
			args = append(args, data.UUIDArgs...)
			val := nfs.UUID
			if len(data.UUIDArgs) == 1 && data.UUIDArgs[0] == "-m" {
				val = "uuid=" + val
			}
			args = append(args, val)
		}

		// TODO: Figure out sectorsize and send that arg.

		args = append(args, fs.parentPath)

		_, err := runCommand(data.Command, args...)
		if err != nil {
			return ResultFailed, fmt.Errorf("file system action run command failed. %v", err)
		}
		return ResultRescan, nil
	}

	if fs.Type != nfs.Type {
		return ResultFailed, fmt.Errorf("FileSystem's don't match: A: %s C: %s", fs.Type, nfs.Type)
	}

	// Try to resize
	if nfs.Resize {
		// TODO: Resize the FS
	}

	if nfs.Image != nil && !fs.imageApplied {
		fs.imageApplied = true

		tmpMount, err := mountTmpFS(fs.parentPath)
		if err != nil {
			return ResultFailed, fmt.Errorf("mount tmp fs call failed. %v", err)
		}

		nfs.Image.Path = tmpMount
		nfs.Image.tarOnly = true
		err = nfs.Image.Action()
		if err != nil {
			return ResultFailed, fmt.Errorf("image.action failed. %v", err)
		}

		err = unmountTmpFS(tmpMount)
		if err != nil {
			return ResultFailed, fmt.Errorf("unmountTmpFS call failed. %v", err)
		}
	}

	return ResultSuccess, nil
}

// ScanFS scans the system for filesystems and other info.
// It updates the pieces.
func ScanFS(disks Disks, vgs VolumeGroups, swRaids SwRaids) error {
	args := []string{"--bytes", "--json", "--output-all", "--paths"}

	out, err := runCommand("lsblk", args...)
	if err != nil {
		return err
	}

	var output LsblkDiskInfo
	if err = json.Unmarshal([]byte(out), &output); err != nil {
		return err
	}

	for _, device := range output.BlockDevices {
		if !isIgnoredDiskType(device.Type) {
			if err = processBlockDevice(device, disks, vgs, swRaids); err != nil {
				return err
			}
		}
		for _, child := range device.Children {
			if !isIgnoredDiskType(child.Type) {
				if err = processBlockDevice(child, disks, vgs, swRaids); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func processBlockDevice(device BlockDevice, disks Disks, vgs VolumeGroups, swRaids SwRaids) error {
	keys := map[string]string{
		"UUID":       derefString(device.UUID),
		"LABEL":      derefString(device.Label),
		"FSTYPE":     derefString(device.FSType),
		"MOUNTPOINT": derefString(device.MountPoint),
	}

	switch device.Type {
	case "raid0", "raid1", "raid4", "raid5", "raid6", "raid10":
		r := swRaids.FindBySwName(device.Name)
		if r == nil {
			return fmt.Errorf("couldn't find swraid: %s", device.Name)
		}
		r.updateBlkInfo(keys)
	case "disk":
		d := disks.FindDiskByPath(device.Name)
		if d == nil {
			return fmt.Errorf("couldn't find disk: %s", device.Name)
		}
		d.updateBlkInfo(keys)
	case "part", "md":
		p := disks.FindPartitionByPath(device.Name)
		if p == nil {
			// Also check raid parts
			p = swRaids.FindPartitionByPath(device.Name)
			if p == nil {
				return fmt.Errorf("couldn't find part: %s", device.Name)
			}
		}
		p.updateBlkInfo(keys)
	case "lvm":
		lv := vgs.FindLvByName(device.Name)
		if lv == nil {
			return fmt.Errorf("couldn't find lvm: %s", device.Name)
		}
		lv.updateBlkInfo(keys)
	default:
		dmsg(DbgScan|DbgFs, "Skipping %s because it is a %s\n", device.Name, device.Type)
	}
	// Recursively process children
	for _, child := range device.Children {
		if err := processBlockDevice(child, disks, vgs, swRaids); err != nil {
			return err
		}
	}
	return nil
}

func (fs *FileSystem) String() string {
	if fs == nil {
		return "unset"
	}
	return fmt.Sprintf("<name:%s type:%s label:%s force:%v quick:%v quiet:%v uuid:%s mount:%s resize:%v image:%v parentpath:%s imageapplied:%v>",
		fs.Name, fs.Type, fs.Label, fs.Force, fs.Quick, fs.Quiet, fs.UUID, fs.Mount, fs.Resize, fs.Image, fs.parentPath, fs.imageApplied)
}
