package main

type StorageDevice interface {
	StorageDeviceType() string
	GetPath() string
	GetName() string
	GetPTable() string
	NextStart() string
	SetNextStart(ns string)
	GetGrubDevice() bool
	SetGrubDevice(b bool)
}
