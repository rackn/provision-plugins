package main

import (
	"errors"
	"reflect"
	"testing"
)

// Test for FindDiskByPath method
func TestFindDiskByPath(t *testing.T) {
	disks := Disks{
		&Disk{Name: "Disk1", Path: "/dev/sda"},
		&Disk{Name: "Disk2", Path: "/dev/sdb"},
	}

	tests := []struct {
		path string
		want *Disk
	}{
		{"/dev/sda", disks[0]},
		{"/dev/sdb", disks[1]},
		{"/dev/sdc", nil},
	}

	for _, tt := range tests {
		t.Run(tt.path, func(t *testing.T) {
			got := disks.FindDiskByPath(tt.path)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindDiskByPath(%q) = %v, want %v", tt.path, got, tt.want)
			}
		})
	}
}

// Test for FindByName method
func TestFindByName(t *testing.T) {
	partitions := Partitions{
		&Partition{Name: "Partition1"},
	}
	disks := Disks{
		&Disk{Name: "Disk1", Path: "/dev/sda", Partitions: partitions},
	}

	tests := []struct {
		name string
		want interface{}
	}{
		{"Disk1", disks[0]},
		{"Partition1", partitions[0]},
		{"NonExistent", nil},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := disks.FindByName(tt.name)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindByName(%q) = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}

// Test for Disk.Validate method
func TestDiskValidate(t *testing.T) {
	tests := []struct {
		disk Disk
		want error
	}{
		{Disk{Name: "Disk1", Path: "/dev/sda", PTable: "gpt"}, nil},
		{Disk{Name: "", Path: "/dev/sda", PTable: "gpt"}, errors.New("name must be specified")},
		{Disk{Name: "Disk2", Path: "", PTable: "gpt"}, errors.New("path must be specified")},
		{Disk{Name: "Disk3", Path: "/dev/sda", PTable: "invalid"}, errors.New("PTable is not valid: invalid")},
	}

	for _, tt := range tests {
		t.Run(tt.disk.Name, func(t *testing.T) {
			got := tt.disk.Validate()
			if !equalError(got, tt.want) {
				t.Errorf("Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}
