package main

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

// Image defines an image to put on the system
type Image struct {
	//
	// Path is used to define the target of the action.
	// When used stand-alone this is an optional field that
	// represents the path in the chroot where the image will be
	// placed.
	//
	// When used as part of another object, this is overridden by
	// the container's path.
	//
	// optional: true
	//
	Path string `json:"path,omitempty"`
	//
	// URL is the path to the image
	//
	// required: true
	//
	URL string `json:"url"`
	//
	// Type of image
	//
	// required: true
	//
	Type string `json:"type"`
	//
	// Checksum is an optional value that can be set
	// if the user wants checksum validation to be done
	//
	// optional: true
	//
	Checksum string `json:"checksum,omitempty"`
	//
	// Used by calling containers to indicate restricted types.
	// Should be config validated.
	rawOnly bool
	tarOnly bool
}

// Images defines an array of Image
type Images []*Image

// NewImage creates a new Image
func NewImage(url, itype string) *Image {
	return &Image{URL: url, Type: itype, Path: "/"}
}

var extractors = map[string]string{
	"dd-tgz":  "| tar -xOzf -",
	"dd-txz":  "| tar -xOJf -",
	"dd-tbz":  "| tar -xOjf -",
	"dd-tar":  "| tar -xOf -",
	"dd-bz2":  "| bzcat",
	"dd-gz":   "| zcat",
	"dd-xz":   "| xzcat",
	"dd-zst":  "| unzstd",
	"dd-raw":  "",
	"tgz":     "| tar -Sxpzf - --numeric-owner --xattrs --xattrs-include=* --acls --selinux -C",
	"txz":     "| tar -SxpJf - --numeric-owner --xattrs --xattrs-include=* --acls --selinux -C",
	"tbz":     "| tar -Sxpjf - --numeric-owner --xattrs --xattrs-include=* --acls --selinux -C",
	"tar":     "| tar -Sxpf - --numeric-owner --xattrs --xattrs-include=* --acls --selinux -C",
	"tar-zst": "| tar --use-compress-program=zstd -Sxpf - --numeric-owner --xattrs --xattrs-include=* --acls --selinux -C",
}

// Validate validates an Image object.
func (i *Image) Validate() error {
	var allErrors error
	if i.Path != "" && !strings.HasPrefix(i.Path, "/") {
		allErrors = errors.Join(allErrors, fmt.Errorf("path must start with /"))
	}

	_, ok := extractors[i.Type]
	if !ok {
		allErrors = errors.Join(allErrors, fmt.Errorf("unknown image type for container: %s", i.Type))
	}

	if i.rawOnly && !strings.HasPrefix(i.Type, "dd-") {
		allErrors = errors.Join(allErrors, fmt.Errorf("bad image type for raw image: %s", i.Type))
	}
	if i.tarOnly && strings.HasPrefix(i.Type, "dd-") {
		allErrors = errors.Join(allErrors, fmt.Errorf("bad image type for rootfs image: %s", i.Type))
	}

	return allErrors
}

// Check if file exists based on URL type
func doesFileExist(fileUrl string) error {
	switch {
	case strings.HasPrefix(fileUrl, "file://"):
		// Parse the file:// URL to handle special characters and encoding
		u, err := url.Parse(fileUrl)
		if err != nil {
			return fmt.Errorf("invalid file URL: %s", fileUrl)
		}
		localPath := u.Path
		if _, err := os.Stat(localPath); os.IsNotExist(err) {
			return fmt.Errorf("local file does not exist: %s", localPath)
		}
	case strings.HasPrefix(fileUrl, "http://"), strings.HasPrefix(fileUrl, "https://"):
		// Set an HTTP client with timeout
		client := &http.Client{Timeout: 10 * time.Second}
		resp, err := client.Head(fileUrl)
		if err != nil {
			return fmt.Errorf("failed to check remote file: %s %w", fileUrl, err)
		}
		defer resp.Body.Close()

		// Check for valid HTTP status codes (2xx range)
		if resp.StatusCode < 200 || resp.StatusCode >= 400 {
			return fmt.Errorf("remote file not accessible: %s (status: %d)", fileUrl, resp.StatusCode)
		}
	default:
		// Assume local file path if it's not file:// or http(s)://
		if _, err := os.Stat(fileUrl); os.IsNotExist(err) {
			return fmt.Errorf("local file does not exist: %s", fileUrl)
		}
	}
	return nil
}

// Action puts the image in place using the type to define how.
//
// The Image action assumes that the image can be streamed.
func (i *Image) Action() error {
	dmsg(DbgImage, "Image Action: type: %s url: %s path: %s checksum: %s\n", i.Type, i.URL, i.Path, i.Checksum)

	// Check if the file exists
	if err := doesFileExist(i.URL); err != nil {
		return err
	}
	dmsg(DbgImage, "Image Action: verified that file at url: %s exists\n", i.URL)

	command := "bash"

	fileAccess := ""
	if strings.HasPrefix(i.URL, "http://") || strings.HasPrefix(i.URL, "https://") {
		fileAccess = fmt.Sprintf(`curl -gs "%s"`, i.URL)
	} else if strings.HasPrefix(i.URL, "file://") {
		fileAccess = fmt.Sprintf(`cat "%s"`, i.URL[7:])
	} else {
		fileAccess = fmt.Sprintf(`cat "%s"`, i.URL)
	}

	extractor, _ := extractors[i.Type]
	dd := ""
	if i.rawOnly {
		dd = fmt.Sprintf(`| dd bs=4M of="%s"`, i.Path)
	} else {
		dd = i.Path
	}

	shaFileName := "imageChecksum.sha256"
	shaFileCommand := fmt.Sprintf("| tee >(sha256sum > %s)", shaFileName)

	bout, err := runCommand(command, "-c", fmt.Sprintf("%s %s %s %s", fileAccess, shaFileCommand, extractor, dd))
	if err != nil {
		return err
	}
	dmsg(DbgImage, "Deployed image: %s\n%v\n", bout, err)

	// Start a rescan.
	if i.rawOnly {
		runCommand("partprobe", i.Path)
		runCommand("pvscan", "--cache", "--activate=ay")
	}

	shaBytes, err := os.ReadFile(shaFileName)
	if err != nil {
		return fmt.Errorf("failed to read SHA file: %s due to: %w", shaFileName, err)
	}
	shaContent := string(shaBytes)

	parts := strings.Split(shaContent, " ")
	shaSum := ""
	if len(parts) > 0 {
		shaSum = parts[0]
		dmsg(DbgImage, "SHA checksum: %s\n", shaSum)
	} else {
		return fmt.Errorf("unexpected SHA file format")
	}

	if i.Checksum != "" && i.Checksum != shaSum {
		return fmt.Errorf("checksum does not match. input: %s calculated: %s", i.Checksum, shaSum)
	}

	return nil
}

func (i *Image) String() string {
	if i == nil {
		return "unset"
	}
	return fmt.Sprintf("<path:%s url:%s type:%s rawonly:%v taronly:%v>", i.Path, i.URL, i.Type, i.rawOnly, i.tarOnly)
}
