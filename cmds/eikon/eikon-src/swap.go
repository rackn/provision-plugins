package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
)

type SwapFile struct {
	Filename    string `json:"filename,omitempty"`
	Maxsize     string `json:"maxsize,omitempty"`
	Size        string `json:"size,omitempty"`
	Force       bool   `json:"force,omitempty"`
	target      string // For mounting swap
	sizeInBytes uint64 // Calculated size
}

func (sf *SwapFile) Equal(c Comparator) bool {
	nsf := c.(*SwapFile)
	return sf.Filename == nsf.Filename
}

func (sf *SwapFile) Merge(c Comparator) error {
	nsf := c.(*SwapFile)
	sf.Size = nsf.Size
	sf.Maxsize = nsf.Maxsize
	sf.Force = nsf.Force
	return nil
}

func (sf *SwapFile) Validate() error {
	var allErrors error

	// Filename must be absolute path if specified
	if sf.Filename == "" {
		sf.Filename = "/swap.img"
	} else if !strings.HasPrefix(sf.Filename, "/") {
		allErrors = errors.Join(allErrors, fmt.Errorf("swap filename must be absolute path"))
	}

	// Validate size format if specified (empty means use calculated size, 0 means disable swap)
	if sf.Size != "" {
		if _, err := sizeParser(sf.Size, sizeMultipliers); err != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("invalid swap size format: %w", err))
		}
	}

	// Validate maxsize format if specified
	if sf.Maxsize != "" {
		if _, err := sizeParser(sf.Maxsize, sizeMultipliers); err != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("invalid swap maxsize format: %w", err))
		}
	}

	// If both size and maxsize specified, check size <= maxsize
	if sf.Size != "" && sf.Size != "0" && sf.Maxsize != "" {
		size, _ := sizeParser(sf.Size, sizeMultipliers)
		maxsize, _ := sizeParser(sf.Maxsize, sizeMultipliers)
		if size > maxsize {
			allErrors = errors.Join(allErrors, fmt.Errorf("swap size exceeds maxsize"))
		}
	}

	return allErrors
}

// calculateSwapSize implements Curtin's swap sizing logic
func (sf *SwapFile) calculateSwapSize() error {
	// If size specified directly, use that
	if sf.Size != "" {
		if sf.Size == "0" {
			sf.sizeInBytes = 0
			return nil
		}
		size, err := sizeParser(sf.Size, curtinSizeMultiplier)
		if err != nil {
			return fmt.Errorf("invalid size format: %w", err)
		}
		sf.sizeInBytes = size
		return nil
	}

	// Get memory info for calculations
	memTotal, err := getTotalMemory()
	if err != nil {
		return fmt.Errorf("failed to get system memory: %w", err)
	}

	// Default max is 8GB
	const GB = uint64(1 << 30)
	suggMax := 8 * GB
	var maxsize uint64

	// Convert maxsize if specified
	if sf.Maxsize != "" {
		maxsize, err = sizeParser(sf.Maxsize, curtinSizeMultiplier)
		if err != nil {
			return fmt.Errorf("invalid maxsize format: %w", err)
		}
	}

	// Calculate based on filesystem availability
	var fsAvail uint64
	if sf.target != "" {
		avail, err := getFilesystemAvailable(sf.target)
		if err != nil {
			return fmt.Errorf("failed to get filesystem space: %w", err)
		}
		fsAvail = avail

		if maxsize == 0 {
			// Set to 25% of filesystem space
			maxsize = min(fsAvail/4, suggMax)
		} else if maxsize > uint64(float64(fsAvail)*0.9) {
			// Cap at 90% of available space
			maxsize = uint64(float64(fsAvail) * 0.9)
		}
	} else if maxsize == 0 {
		maxsize = suggMax
	}

	// Implement Curtin's sizing logic
	var calculatedSize uint64
	switch {
	case memTotal <= 1*GB:
		calculatedSize = memTotal * 2
	case memTotal <= 2*GB:
		calculatedSize = 2 * GB
	case memTotal <= 4*GB:
		calculatedSize = memTotal
	case memTotal <= 16*GB:
		calculatedSize = 4 * GB
	default:
		calculatedSize = memTotal / 2
	}

	// Special case: if swap would be less than half RAM and less than 4GB
	if calculatedSize < (memTotal/2) && calculatedSize < 4*GB {
		sf.sizeInBytes = 0
		return nil
	}

	// Respect maxsize
	if calculatedSize > maxsize {
		calculatedSize = maxsize
	}

	sf.sizeInBytes = calculatedSize
	return nil
}

func (sf *SwapFile) createSwapFile() error {
	targetPath := filepath.Join(sf.target, sf.Filename)
	dmsg(DbgSwap, "creating swapfile '%s' of %sMB", targetPath, sf.sizeInBytes)

	// Ensure target directory exists
	if err := os.MkdirAll(targetPath, 0755); err != nil {
		return fmt.Errorf("failed to create target directory: %w", err)
	}

	// Remove existing file if present
	if err := os.Remove(targetPath); err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("failed to remove existing swap file: %w", err)
	}

	fstype, err := getFilesystemType(filepath.Dir(targetPath))
	if err != nil {
		return err
	}

	// Use dd for problematic filesystems, fallocate for others
	var cmd string
	var args []string

	switch fstype {
	case "btrfs", "ext2", "ext3", "xfs":
		cmd = "dd"
		args = []string{
			"if=/dev/zero",
			fmt.Sprintf("of=%s", sf.Filename),
			"bs=1M",
			fmt.Sprintf("count=%d", sf.sizeInBytes>>20),
		}
	default:
		cmd = "fallocate"
		args = []string{
			"-l", fmt.Sprintf("%d", sf.sizeInBytes),
			targetPath,
		}
	}

	if _, err := runCommand(cmd, args...); err != nil {
		return fmt.Errorf("failed to allocate swap file: %w", err)
	}

	// Set correct permissions
	if err := os.Chmod(targetPath, 0600); err != nil {
		return fmt.Errorf("failed to set swap file permissions: %w", err)
	}

	return nil
}

func (sf *SwapFile) initializeSwap() error {
	_, err := runCommand("mkswap", filepath.Join(sf.target, sf.Filename))
	return err
}

func (sf *SwapFile) activateSwap() error {
	_, err := runCommand("swapon", filepath.Join(sf.target, sf.Filename))
	return err
}

func (sf *SwapFile) deActivateSwap() error {
	_, err := runCommand("swapoff", filepath.Join(sf.target, sf.Filename))
	return err
}

func (sf *SwapFile) validateFilesystemSupport(fstype string) error {
	switch fstype {
	case "btrfs":
		// Check kernel version for btrfs swap support
		ver, err := getKernelVersion()
		if err != nil {
			return err
		}
		if !isBtrfsSwapSupported(ver) {
			return fmt.Errorf("btrfs requires kernel version 5.0+ for swapfiles")
		}
	case "tmpfs", "overlay", "zfs":
		return fmt.Errorf("cannot create swap file on %s", fstype)
	}
	return nil
}

func getTotalMemory() (uint64, error) {
	content, err := os.ReadFile("/proc/meminfo")
	if err != nil {
		return 0, err
	}

	lines := strings.Split(string(content), "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "MemTotal:") {
			fields := strings.Fields(line)
			if len(fields) < 2 {
				continue
			}
			// Convert from KB to bytes
			kb, err := strconv.ParseUint(fields[1], 10, 64)
			if err != nil {
				return 0, err
			}
			return kb * 1024, nil
		}
	}
	return 0, fmt.Errorf("could not find MemTotal in /proc/meminfo")
}

func getFilesystemAvailable(path string) (uint64, error) {
	var stat syscall.Statfs_t
	if err := syscall.Statfs(path, &stat); err != nil {
		return 0, err
	}
	return stat.Bavail * uint64(stat.Bsize), nil
}

func isBtrfsSwapSupported(kernelVer string) bool {
	ver := strings.Split(kernelVer, ".")
	if len(ver) < 2 {
		return false
	}
	major, err := strconv.Atoi(ver[0])
	if err != nil {
		return false
	}
	return major >= 5
}

func (sf *SwapFile) Setup() error {
	dmsg(DbgSwap, "swap file action on %s, %s, %s\n", sf.Filename, sf.Size, sf.Maxsize)
	targetPath := filepath.Join(sf.target, sf.Filename)

	if err := sf.calculateSwapSize(); err != nil {
		return err
	}
	dmsg(DbgSwap, "swap file size calculated %d\n", sf.sizeInBytes)

	// If size is 0, swapfile is disabled
	if sf.sizeInBytes == 0 {
		dmsg(DbgSwap, "Swap file disabled - size is 0")
		return nil
	}

	// Check filesystem compatibility
	fstype, err := getFilesystemType(filepath.Dir(targetPath))
	if err != nil {
		return fmt.Errorf("failed to determine filesystem type: %w", err)
	}

	if !sf.Force {
		if err := sf.validateFilesystemSupport(fstype); err != nil {
			return fmt.Errorf("invalid filesystem type: %w. file: %s", err, sf.Filename)
		}

		dmsg(DbgSwap, "File system supported %s \n", fstype)
	}

	// Create the swap file
	if err := sf.createSwapFile(); err != nil {
		return fmt.Errorf("failed to create swap file: %w", err)
	}

	// Initialize
	if err := sf.initializeSwap(); err != nil {
		os.Remove(targetPath)
		return fmt.Errorf("failed to initialize swap: %w", err)
	}

	// Activate it
	if err := sf.activateSwap(); err != nil {
		dmsg(DbgSwap, "There was an error activating swap %w \n", err)
		return nil
	}

	// It was activated successfully, now de-activate it
	if err := sf.deActivateSwap(); err != nil {
		dmsg(DbgSwap, "There was an error de-activating swap %w \n", err)
		return nil
	}

	return nil
}
