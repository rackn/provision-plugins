package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

// DebugLevel is an enum that defines log level groups
type DebugLevel int

// These are the debug groups
const (
	DbgScan    DebugLevel = 1 << iota // 1
	DbgDisk                           // 2
	DbgPart                           // 4
	DbgSwRaid                         // 8
	DbgPv                             // 16
	DbgVg                             // 32
	DbgLv                             // 64
	DbgFs                             // 128
	DbgImage                          // 256
	DbgConfig                         // 512
	DbgAction                         // 1024
	DbgCmd                            // 2048
	DbgRemove                         // 4096
	DbgMount                          // 8192
	DbgUnmount                        // 16384
	DbgRescan
	DbgSwap
	DbgCurtinConversion
)

// Define a map for size suffixes to their multipliers
var sizeMultipliers = map[string]float64{
	"PB":  1000 * 1000 * 1000 * 1000 * 1000,
	"TB":  1000 * 1000 * 1000 * 1000,
	"GB":  1000 * 1000 * 1000,
	"MB":  1000 * 1000,
	"mB":  1000 * 1000,
	"mb":  1000 * 1000,
	"KB":  1000,
	"kB":  1000,
	"kb":  1000,
	"KiB": 1024,
	"K":   1024,
	"MiB": 1024 * 1024,
	"M":   1024 * 1024,
	"GiB": 1024 * 1024 * 1024,
	"G":   1024 * 1024 * 1024,
	"TiB": 1024 * 1024 * 1024 * 1024,
	"T":   1024 * 1024 * 1024 * 1024,
	"PiB": 1024 * 1024 * 1024 * 1024 * 1024,
	"P":   1024 * 1024 * 1024 * 1024 * 1024,
	"B":   1,
}

// Define a map for size suffixes to their multipliers
// A new one because everything in curtin is ^2
var curtinSizeMultiplier = map[string]float64{
	"k":  1024,
	"kB": 1024,
	"M":  1024 * 1024,
	"MB": 1024 * 1024,
	"G":  1024 * 1024 * 1024,
	"GB": 1024 * 1024 * 1024,
	"T":  1024 * 1024 * 1024 * 1024,
	"TB": 1024 * 1024 * 1024 * 1024,
	"P":  1024 * 1024 * 1024 * 1024 * 1024,
	"PB": 1024 * 1024 * 1024 * 1024 * 1024,
	"B":  1,
}

var raidPattern = regexp.MustCompile(`^raid(\d+)`)

// Function to print debug
func dmsg(level DebugLevel, f string, args ...interface{}) {
	if (debug & level) > 0 {
		fmt.Fprintf(os.Stderr, f, args...)
	}
}

// Comparator is an interface that allows for
// common routines to manipulate the objects without
// regard to their type.
type Comparator interface {
	Equal(Comparator) bool
	Merge(Comparator) error
}

// MergeList - This is actual merge over time.
// The merge function assumes that nd is the new object and
// its actual should replace the current d.
func MergeList(base, merge []Comparator) ([]Comparator, error) {
	if len(base) == 0 {
		base = merge
	} else {
		for _, nd := range merge {
			found := false
			for _, d := range base {
				if d.Equal(nd) {
					if err := d.Merge(nd); err != nil {
						return nil, err
					}
					found = true
					break
				}
			}
			if !found {
				base = append(base, nd)
			}
		}

		remove := []Comparator{}
		for _, d := range base {
			found := false
			for _, nd := range merge {
				if nd.Equal(d) {
					found = true
					break
				}
			}
			if !found {
				remove = append(remove, d)
			}
		}

		for _, rd := range remove {
			for i, d := range base {
				if d.Equal(rd) {
					s := base
					s[len(s)-1], s[i] = s[i], s[len(s)-1]
					base = s[:len(s)-1]
					break
				}
			}
		}
	}
	return base, nil
}

// SizeParseError is used to indicate a bad size
type SizeParseError error

// sizeParser parses a string size and returns a number.
// 1 KB = Kilobytes = 1024 Bytes
// 1 Kb = Kilobits = 1024 Bits (128 bytes)
// 1 KiB = Kilobytes = 1000 Bytes
// 1 Kib = Kilobits = 1000 Bits (125 bytes)
func sizeParser(v string, multiplier map[string]float64) (uint64, error) {
	sizeRE := regexp.MustCompile(`([0-9.]+) *([KMGTPkmgtp]?[i]?[Bb]?)`)
	parts := sizeRE.FindStringSubmatch(v)
	if len(parts) < 2 {
		return 0, SizeParseError(fmt.Errorf("%s cannot be parsed as a Size", v))
	}
	f, err := strconv.ParseFloat(parts[1], 10)
	if err != nil {
		return 0, SizeParseError(err)
	}

	if len(parts) == 3 {
		if multiplier, exists := multiplier[parts[2]]; exists {
			f = f * multiplier
		} else {
			return 0, SizeParseError(fmt.Errorf("%s is not a valid size suffix", parts[2]))
		}
	}
	return uint64(f), nil
}

// sizeStringer takes a number and returns a string size.
func sizeStringer(s uint64, unit string) string {
	var suffix string
	var i int
	for i, suffix = range []string{"B", "KB", "MB", "GB", "TB", "PB"} {
		if unit == suffix {
			break
		}
		mul := uint64(1) << ((uint64(i) + 1) * 10)
		if uint64(s) < mul {
			break
		}
	}
	if i != 0 {
		resVal := float64(s) / float64(uint64(1)<<(uint64(i)*10))
		return fmt.Sprintf("%s%s", strconv.FormatFloat(resVal, 'g', -1, 64), suffix)
	}
	return fmt.Sprintf("%dB", s)
}

func sizeToBytes(sin string, multiplier map[string]float64) (string, error) {
	d, err := sizeParser(sin, multiplier)
	return fmt.Sprintf("%dB", d), err
}

func runCommand(command string, args ...string) (string, error) {
	dmsg(DbgCmd, "Run Command: %s %v\n", command, args)
	out, err := exec.Command(command, args...).CombinedOutput()
	output := string(out)
	dmsg(DbgCmd, "Returned: %s\n%v\n", output, err)
	// Also check for exit error
	var exitError *exec.ExitError
	if errors.As(err, &exitError) {
		return output, fmt.Errorf("Command: %s failed with exit code %d\n%s", command, exitError.ExitCode(), output)
	}
	if err != nil {
		err = fmt.Errorf("Command: %s failed\n%v\n%s", command, err, string(out))
	}
	return output, err
}

func runCommandNoStdErr(command string, args ...string) (string, error) {
	dmsg(DbgCmd, "Run Command: %s %v\n", command, args)
	out, err := exec.Command(command, args...).Output()
	dmsg(DbgCmd, "Returned: %s\n%v\n", string(out), err)
	if err != nil {
		err = fmt.Errorf("Command: %s failed\n%v\n%s", command, err, string(out))
	}
	return string(out), err
}

// runParted runs parted with the provided options.
func runParted(options ...string) error {
	dmsg(DbgCmd, "Parted Command: parted -a opt -s %s\n", strings.Join(options, " "))
	lopts := []string{"-a", "opt", "-s"}
	lopts = append(lopts, options...)
	out, err := exec.Command("parted", lopts...).CombinedOutput()
	dmsg(DbgCmd, "Parted Returned: %s\n%v\n", string(out), err)
	if err != nil {
		err = fmt.Errorf("Parted Failed: %v\n%s", err, string(out))
	}
	return err
}

// udevUpdate causes the system to rescan devices.
func udevUpdate() error {
	if out, err := exec.Command("udevadm", "trigger").CombinedOutput(); err != nil {
		return fmt.Errorf("udevadm trigger Failed: %v\n%s", err, string(out))
	}
	if out, err := exec.Command("udevadm", "settle").CombinedOutput(); err != nil {
		return fmt.Errorf("udevadm settle Failed: %v\n%s", err, string(out))
	}
	return nil
}

func mountTmpFS(path string) (string, error) {
	dir, err := os.MkdirTemp("/tmp", "example")
	if err != nil {
		dmsg(DbgAction, "Failed to create tempdir: %v\n", err)
		return "", err
	}
	_, err = exec.Command("mount", path, dir).CombinedOutput()
	if err != nil {
		dmsg(DbgAction, "Failed to mount tempdir: %s on %s:  %v\n", path, dir, err)
		os.RemoveAll(dir)
		return "", err
	}
	return dir, nil
}

func unmountTmpFS(path string) error {
	_, err := exec.Command("umount", path).CombinedOutput()
	if err != nil {
		dmsg(DbgAction, "Failed to mount tempdir: %s:  %v\n", path, err)
	}
	os.RemoveAll(path)
	return err
}

type byLen []string

func (a byLen) Len() int {
	return len(a)
}

func (a byLen) Less(i, j int) bool {
	return len(a[i]) < len(a[j])
}

func (a byLen) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func isMountableType(t string) bool {
	switch t {
	case "linux_raid_member", "swap", "LVM2_member":
		return false
	default:
		return true
	}
}

func getAllFileSystems(swraids SwRaids, disks Disks, vgs VolumeGroups) ([]string, map[string]string, []string, error) {
	fsPathMap := map[string]string{}
	fsPathList := []string{}
	var fsSwapPathList []string

	dmsg(DbgMount, "getAllFileSystems:\n disks: %v\n swraids: %v\n vgs: %v\n\n", disks, swraids, vgs)

	for _, d := range disks {
		dmsg(DbgMount, "getAllFileSystems: processing disk: %s %s\n", d.FileSystem, d.Name)
		if d.FileSystem != nil && d.FileSystem.Mount != "" && isMountableType(d.FileSystem.Type) {
			fsPathMap[d.FileSystem.Mount] = d.Path
			fsPathList = append(fsPathList, d.FileSystem.Mount)
		}
		if d.FileSystem != nil && d.FileSystem.Type == "swap" {
			fsSwapPathList = append(fsSwapPathList, d.Path)
		}

		for _, p := range d.Partitions {
			dmsg(DbgMount, "getAllFileSystems: processing disk part: %s %s\n", p.FileSystem, p.Name)
			if isMountableType(p.PType) && p.FileSystem != nil && p.FileSystem.Mount != "" && isMountableType(p.FileSystem.Type) {
				fsPathMap[p.FileSystem.Mount] = p.FileSystem.parentPath
				fsPathList = append(fsPathList, p.FileSystem.Mount)
			}
			if p.FileSystem != nil && p.FileSystem.Type == "swap" {
				fsSwapPathList = append(fsSwapPathList, GeneratePartitionPath(d.Path, p.ID))
			}
		}
	}

	for _, sw := range swraids {
		dmsg(DbgMount, "getAllFileSystems: processing swraid: %s %s\n", sw.FileSystem, sw.Name)
		if sw.FileSystem != nil && sw.FileSystem.Mount != "" && isMountableType(sw.FileSystem.Type) {
			fsPathMap[sw.FileSystem.Mount] = sw.Path
			fsPathList = append(fsPathList, sw.FileSystem.Mount)
		}
		if sw.FileSystem != nil && sw.FileSystem.Type == "swap" {
			fsSwapPathList = append(fsSwapPathList, sw.Path)
		}

		for _, p := range sw.Partitions {
			dmsg(DbgMount, "getAllFileSystems: processing raid part: %s %s\n", p.FileSystem, p.Name)
			if isMountableType(p.PType) && p.FileSystem != nil && p.FileSystem.Mount != "" && isMountableType(p.FileSystem.Type) {
				fsPathMap[p.FileSystem.Mount] = p.FileSystem.parentPath
				fsPathList = append(fsPathList, p.FileSystem.Mount)
			}
			if p.FileSystem != nil && p.FileSystem.Type == "swap" {
				fsSwapPathList = append(fsSwapPathList, GeneratePartitionPath(sw.Path, p.ID))
			}
		}
	}

	for _, vg := range vgs {
		dmsg(DbgMount, "getAllFileSystems: processing vg: %v\n", vg.Name)
		for _, lv := range vg.LogicalVolumes {
			dmsg(DbgMount, "getAllFileSystems: processing lv part: %s %s\n", lv.FileSystem, lv.Name)
			if lv.FileSystem != nil && lv.FileSystem.Mount != "" && isMountableType(lv.FileSystem.Type) {
				fsPathMap[lv.FileSystem.Mount] = lv.Path
				fsPathList = append(fsPathList, lv.FileSystem.Mount)
			}
			if lv.FileSystem != nil && lv.FileSystem.Type == "swap" {
				fsSwapPathList = append(fsSwapPathList, lv.Path)
			}
		}
	}

	sort.Sort(byLen(fsPathList))

	return fsPathList, fsPathMap, fsSwapPathList, nil
}

func mountAll(basePath string, c *Config) (string, error) {
	dmsg(DbgMount, "mountAll\n")
	fsPathList, fsPathMap, fsSwapPathList, err := getAllFileSystems(c.SwRaids, c.Disks, c.VolumeGroups)
	if err != nil {
		return "", err
	}

	dmsg(DbgMount, "mounting filesystems: %w \n fsPathMap: %w \n fsSwapPaths: %w\n", fsPathList, fsPathMap, fsSwapPathList)

	dir := basePath
	if dir == "" {
		dir, err = os.MkdirTemp("/tmp", "eikon")
		if err != nil {
			dmsg(DbgAction, "Failed to create tempdir: %v\n", err)
			return "", err
		}
	}

	for _, fs := range fsPathList {
		path := filepath.Join(dir, fs)
		path = strings.TrimSuffix(path, "/")
		if err := os.MkdirAll(path, 0755); err != nil {
			unmountAll(dir, c)
			return "", err
		}
		if _, err := runCommand("mount", fsPathMap[fs], path); err != nil {
			unmountAll(dir, c)
			return "", err
		}
	}

	// Mount all swaps
	for _, swapPath := range fsSwapPathList {
		if _, err := runCommand("swapon", swapPath); err != nil {
			dmsg(DbgMount, "there was an error running swapon on %s, error is: %v\n", swapPath, err)
		}
	}

	// If a swap file is present, this is where you would create and mount it
	// Handle swap with same pattern as other components
	dmsg(DbgMount, "Creating swap file\n")
	if c.SwapFile != nil && !strings.HasPrefix(basePath, "/tmp") {
		dmsg(DbgConfig, "Swap file being created at path: %s Details: %w\n", basePath, c.SwapFile)
		c.SwapFile.target = basePath
		if err := c.SwapFile.Setup(); err != nil {
			dmsg(DbgMount, "there was an error creating swap file %s at %s, error is: %v\n", c.SwapFile.Filename, basePath, err)
			return "", err
		}
	}

	return dir, nil
}

func unmountAll(dir string, c *Config) error {
	dmsg(DbgUnmount, "unmountAll\n")
	fsPathList, _, fsSwapPathList, err := getAllFileSystems(c.SwRaids, c.Disks, c.VolumeGroups)
	if err != nil {
		return err
	}
	dmsg(DbgUnmount, "unmountAll: fsPathList: %v  fsSwapPathList: %v\n", fsPathList, fsSwapPathList)
	for i := len(fsPathList) - 1; i >= 0; i-- {
		fs := fsPathList[i]
		path := fmt.Sprintf("%s%s", dir, fs)
		if _, err := runCommand("umount", path); err != nil {
			return err
		}
	}

	// Unmount swap
	for _, swapPath := range fsSwapPathList {
		if _, err := runCommand("swapoff", swapPath); err != nil {
			dmsg(DbgMount, "there was an error running swapoff on %s, error is: %v\n", swapPath, err)
		}
	}

	return os.RemoveAll(dir)
}

func derefString(s *string) string {
	if s != nil {
		return *s
	}
	return ""
}

// getPartitionID extracts the partition ID from a device path
func getPartitionID(path string) (int, error) {
	// Regular expression to match the partition ID at the end of the device path
	re := regexp.MustCompile(`\d+$`)
	match := re.FindString(path)
	if match == "" {
		return 0, fmt.Errorf("no partition ID found in device path: %s", path)
	}
	// Convert the matched string to an integer
	partID, err := strconv.Atoi(match)
	if err != nil {
		return 0, fmt.Errorf("error converting partition ID to integer: %v", err)
	}
	return partID, nil
}

// getDiskNameFromPath get's the name of the disk from
// /dev/diskName path
func getDiskNameFromPath(diskPath string) (string, error) {
	cleanPath := filepath.Clean(diskPath)
	// Check if the device path exists
	if strings.HasPrefix(cleanPath, "/dev/") && !pathExists(cleanPath) {
		return "", fmt.Errorf("devname %s does not exist", cleanPath)
	}
	baseName := filepath.Base(cleanPath)

	if strings.HasPrefix(cleanPath, "/dev/") {
		return baseName, nil
	}
	return "", nil
}

// pathExists checks if a given path exists
func pathExists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func fileExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func getKernelVersion() (string, error) {
	out, err := runCommand("uname", "-r")
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(out), nil
}

func getFilesystemType(path string) (string, error) {
	out, err := runCommand("df", "-T", path)
	if err != nil {
		return "", err
	}
	lines := strings.Split(out, "\n")
	if len(lines) < 2 {
		return "", fmt.Errorf("unexpected df output")
	}
	fields := strings.Fields(lines[1])
	if len(fields) < 2 {
		return "", fmt.Errorf("unexpected df output format")
	}
	return fields[1], nil
}
