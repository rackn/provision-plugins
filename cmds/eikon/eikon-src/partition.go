package main

import (
	"errors"
	"fmt"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

// Partition defines a partition to put on the system
type Partition struct {
	// ID defines the partition index
	ID int `json:"id"`
	// Start defines the start of the partition
	Start string `json:"start,omitempty"`
	// End defines the end of the partition
	End string `json:"end,omitempty"`
	// Size defines the size of the partition
	Size string `json:"size,omitempty"`
	// PType is the type of the partition
	PType string `json:"ptype,omitempty"`
	// Name is the name of the partition
	Name string `json:"name,omitempty"`
	// Image is the image to put on this partition
	Image *Image `json:"image,omitempty"`
	// FileSystem is the filesystem to apply to this partition.
	FileSystem *FileSystem `json:"fs,omitempty"`
	// Flag defines the partition flag (boot, raid, etc)
	Flag string `json:"flag,omitempty"`
	// Filled in by ReadConfig.
	physicalVolume *PhysicalVolume
	parent         StorageDevice
	parentId       string
	imageApplied   bool
}

// Partitions defines list of Partition
type Partitions []*Partition

// Helper function to check partition ID uniqueness
func validatePartitionIDs(partitions Partitions, deviceName string, deviceType string) (Partitions, error) {
	if len(partitions) == 0 {
		return partitions, nil
	}

	ids := map[int]struct{}{}

	// Count how many partitions have IDs
	idsCount := 0
	for _, p := range partitions {
		if p.ID != 0 {
			idsCount++
		}
	}
	// Either all should have IDs or none should
	if idsCount > 0 && idsCount != len(partitions) {
		return nil, fmt.Errorf("%s %s: either all partitions must have IDs or none should", deviceType, deviceName)
	}

	// If no IDs provided, assign sequential IDs
	if idsCount == 0 {
		for i, p := range partitions {
			p.ID = i + 1
		}
		return partitions, nil
	}

	for _, p := range partitions {
		if p.ID <= 0 {
			return nil, fmt.Errorf("partition %s on %s %s has invalid ID: %d", p.Name, deviceType, deviceName, p.ID)
		}
		if _, ok := ids[p.ID]; ok {
			return nil, fmt.Errorf("duplicate partition ID %d found on %s %s (partition %s)", p.ID, deviceType, deviceName, p.Name)
		}
		ids[p.ID] = struct{}{}
	}
	return partitions, nil
}

// FindPartitionByPath finds a partition in the list by its Path
func (ps Partitions) FindPartitionByPath(path string) *Partition {
	for _, p := range ps {
		if GeneratePartitionPath(p.parent.GetPath(), p.ID) == path {
			return p
		}
	}
	return nil
}

// FindByName finds a piece by name
func (ps Partitions) FindByName(name string) interface{} {
	for _, p := range ps {
		if p.Name == name {
			return p
		}
	}
	return nil
}

// toCompList converts a list of Partition into a list of Comparator
func (ps Partitions) toCompList() []Comparator {
	ns := []Comparator{}
	for _, p := range ps {
		if p != nil {
			ns = append(ns, p)
		}
	}
	return ns
}

// ToPartitionList coverts a list of Comparator to a list of Partition
func ToPartitionList(src []Comparator, err error) (Partitions, error) {
	if err != nil {
		return nil, err
	}
	ps := Partitions{}
	for _, s := range src {
		if s != nil {
			ps = append(ps, s.(*Partition))
		}
	}
	return ps, nil
}

// GeneratePartitionPath generates the partition name based on the disk name and partition ID
func GeneratePartitionPath(diskName string, partitionID int) string {
	re := regexp.MustCompile(`^/dev/(nvme|md|dm-)`)

	if re.MatchString(diskName) {
		return fmt.Sprintf("%sp%d", diskName, partitionID)
	}
	return fmt.Sprintf("%s%d", diskName, partitionID)
}

// updateBlkInfo updates the partition with key/values from lsblk
func (p *Partition) updateBlkInfo(keys map[string]string) error {
	if fst, ok := keys["FSTYPE"]; ok && fst != "" {
		if fst != "LVM2_member" {
			if p.FileSystem == nil {
				p.FileSystem = &FileSystem{parentPath: GeneratePartitionPath(p.parent.GetPath(), p.ID)}
			}
			fs := p.FileSystem
			fs.updateBlkInfo(keys)
		}
	}
	return nil
}

// Equal tests partition identity.
func (p *Partition) Equal(c Comparator) bool {
	np := c.(*Partition)
	return p.ID == np.ID
}

// Merge merges the actual comparator with the actual partition
func (p *Partition) Merge(c Comparator) error {
	np := c.(*Partition)
	if p.FileSystem != nil {
		dmsg(DbgPart, "filesystem being passed in: %v\n", np.FileSystem)
		if err := p.FileSystem.Merge(np.FileSystem); err != nil {
			return err
		}
	} else {
		p.FileSystem = np.FileSystem
	}
	return nil
}

// Validate a Partition object.
func (p *Partition) Validate() error {
	var allErrors error

	if strings.ContainsAny(p.Name, " \t") {
		allErrors = errors.Join(allErrors, fmt.Errorf("name should not space or tab"))
	}
	if p.End == "" && p.Size == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("one of Size or End must be specified"))
	}
	if p.End != "" && p.Start == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("end is specified and Start is not"))
	}
	if p.Start != "" && p.End == "" {
		allErrors = errors.Join(allErrors, fmt.Errorf("start is specified and End is not"))
	}

	var err error
	if p.Start != "" {
		p.Start, err = sizeToBytes(p.Start, sizeMultipliers)
		if err != nil {
			return fmt.Errorf("Partition %s has invalid start: %v", p.Name, err)
		}
	}
	if p.End != "" && p.End != "100%" && p.End != "REST" {
		p.End, err = sizeToBytes(p.End, sizeMultipliers)
		if err != nil {
			return fmt.Errorf("Partition %s has invalid end: %v", p.Name, err)
		}
	}
	if p.Size != "" && p.Size != "100%" && p.Size != "REST" && p.Size != "-1" {
		p.Size, err = sizeToBytes(p.Size, sizeMultipliers)
		if err != nil {
			return fmt.Errorf("Partition %s has invalid size: %v", p.Name, err)
		}
	}

	if p.Image != nil {
		p.Image.rawOnly = true
		if e := p.Image.Validate(); e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("partition %s: Image is invalid: %v", p.Name, e))
		}
	}

	if p.FileSystem != nil {
		e := p.FileSystem.Validate()
		if e != nil {
			allErrors = errors.Join(allErrors, fmt.Errorf("partition %s filesystem failed validation: %v", p.Name, e))
		}
	}

	return allErrors
}

func (p *Partition) Remove() error {
	dmsg(DbgPart|DbgRemove, "Partition Remove %v\n", p)

	if p.physicalVolume != nil {
		if err := p.physicalVolume.Remove(); err != nil {
			return err
		}
	}

	if _, err := runCommand("wipefs", "--all", GeneratePartitionPath(p.parent.GetPath(), p.ID)); err != nil {
		return err
	}

	args := []string{
		p.parent.GetPath(), "rm",
		fmt.Sprintf("%d", p.ID),
	}
	if err := runParted(args...); err != nil {
		return err
	}

	return nil
}

// Helper method to determine the filesystem type
func (p *Partition) determineFSType() string {
	if p.FileSystem != nil {
		switch p.FileSystem.Type {
		case "swap":
			return "linux-swap"
		case "vfat":
			return "fat32"
		default:
			return p.FileSystem.Type
		}
	}

	if p.PType != "" {
		switch p.PType {
		case "swap":
			return "linux-swap"
		case "vfat":
			return "fat32"
		default:
			return p.PType
		}
	}
	return ""
}

func isValidGPTFlag(flag string) bool {
	validFlags := map[string]bool{
		"bios_grub": true,
		"lvm":       true,
		"raid":      true,
		"home":      true,
		"msftres":   true,
		"prep":      true,
	}
	return validFlags[flag]
}

// getSectorSize returns the logical block size (sector size) of a device
func getSectorSize(device string) int {
	cmd := exec.Command("cat", fmt.Sprintf("/sys/block/%s/queue/logical_block_size", strings.TrimPrefix(device, "/dev/")))
	output, err := cmd.Output()
	if err != nil {
		dmsg(DbgPart|DbgAction, "failed to get sector size, defaulting to 512 bytes: %v", err)
		return 512
	}

	sectorSize, err := strconv.Atoi(strings.TrimSpace(string(output)))
	if err != nil {
		dmsg(DbgPart|DbgAction, "failed to get sector size, defaulting to 512 bytes: %v", err)
		return 512
	}

	return sectorSize
}

// createMBRPartitionUsing uses fdisk to create msdos partitions
func createMBRPartition(device string, partitionNumber int, startSector uint64, endSector uint64, flag string) error {
	var fdiskInput string
	if flag == "prep" {
		return fmt.Errorf("PReP partitions require a GPT partition table")
	}
	if flag == "extended" {
		fdiskInput += fmt.Sprintf("n\ne\n%d\n%d\n%d\n", partitionNumber, startSector, endSector)
	} else if flag == "logical" {
		fdiskInput += fmt.Sprintf("n\nl\n%d\n%d\n", startSector, endSector)
	} else {
		// Default to primary
		fdiskInput += fmt.Sprintf("n\np\n%d\n%d\n%d\n", partitionNumber, startSector, endSector)
	}

	if flag == "boot" {
		fdiskInput += fmt.Sprintf("a\n%d\n", partitionNumber)
	}

	// Set partition type for swap (Linux swap partition type is 82)
	if flag == "swap" {
		fdiskInput += fmt.Sprintf("t\n%d\n82\n", partitionNumber)
	}

	fdiskInput += "w\n"
	cmd := exec.Command("bash", "-c", fmt.Sprintf("echo -e %q | sudo fdisk %s", fdiskInput, device))
	output, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("fdisk failed: %v\nOutput: %s", err, string(output))
	}

	dmsg(DbgPart|DbgAction, "Partition created successfully with fdisk.")
	return nil
}

// createGPTPartition uses sgdisk to create gpt partitions
func createGPTPartition(device string, partitionNumber int, startSector uint64, endSector uint64, flag string) error {
	typeCode := getGPTTypeCode(flag)
	// Convert uint64 sectors to strings
	startStr := strconv.FormatUint(startSector, 10)
	endStr := strconv.FormatUint(endSector, 10)
	partNumStr := strconv.Itoa(partitionNumber)

	cmd := exec.Command("sudo", "sgdisk", "--new="+partNumStr+":"+startStr+":"+endStr, "--typecode="+strconv.Itoa(partitionNumber)+":"+typeCode, device)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("failed to create GPT partition: %v\nOutput: %s", err, string(output))
	}

	dmsg(DbgPart|DbgAction, "GPT Partition created successfully.")
	return nil
}

// getGPTTypeCode maps flags to sgdisk type codes (matching Curtin's SGDISK_FLAGS)
func getGPTTypeCode(flag string) string {
	sgdiskFlags := map[string]string{
		"bios_grub": "ef02",
		"boot":      "ef00",
		"home":      "8302",
		"linux":     "8300",
		"lvm":       "8e00",
		"msftres":   "0c01",
		"prep":      "4100",
		"raid":      "fd00",
		"swap":      "8200",
	}

	if code, exists := sgdiskFlags[flag]; exists {
		return code
	}
	return "8300" // Default Linux filesystem
}

func sizeWithinTolerance(size1, size2 uint64, toleranceBytes uint64) bool {
	if size1 > size2 {
		return size1-size2 <= toleranceBytes
	}
	return size2-size1 <= toleranceBytes
}

// Action applies the configuration object to the actual object.
func (p *Partition) Action(np *Partition) (Result, error) {
	dmsg(DbgPart|DbgAction, "Partition Action %v %v\n", p, np)

	// This is a NOT new create.
	if p.ID != 0 {
		dmsg(DbgPart|DbgAction, "Not a new partition create %v %v\n", p, np)
		out := ""

		// We need to be a specific location
		if p.ID != np.ID && np.ID != 0 {
			out += fmt.Sprintf("Partition IDs don't match.  A: %d C: %d\n", p.ID, np.ID)
		}

		if np.Start != "" && np.Start != p.Start {
			o, _ := sizeParser(p.Start, sizeMultipliers)
			n, _ := sizeParser(np.Start, sizeMultipliers)
			if !sizeWithinTolerance(o, n, 1024*1024) { // 1MB tolerance
				out += fmt.Sprintf("%s: Start does not match. A: %s C: %s\n", np.Name, p.Start, np.Start)
			}
		}
		if np.End != "" && np.End != "100%" && np.End != "REST" && np.End != p.End {
			o, _ := sizeParser(p.End, sizeMultipliers)
			n, _ := sizeParser(np.End, sizeMultipliers)
			if !sizeWithinTolerance(o, n, 1024*1024) { // 1MB tolerance
				out += fmt.Sprintf("%s: End does not match. A: %s C: %s\n", np.Name, p.End, np.End)
			}
		}
		if np.Size != "REST" && np.Size != "-1" && np.Size != "100%" && np.Size != "" && np.Size != p.Size {
			o, _ := sizeParser(p.Size, sizeMultipliers)
			n, _ := sizeParser(np.Size, sizeMultipliers)
			// Allow 1MB tolerance
			if !sizeWithinTolerance(o, n, 1024*1024) {
				out += fmt.Sprintf("%s: Size does not match. A: %s C: %s\n", np.Name, p.Size, np.Size)
			}
		}
		// XXX: Not all part types show up here. can we validate the partition type

		// Fail if there are errors.
		if out != "" {
			return ResultFailed, fmt.Errorf(out)
		}

		// Image the partition.
		if np.Image != nil && !p.imageApplied {
			p.imageApplied = true

			// Put image in place
			np.Image.Path = GeneratePartitionPath(p.parent.GetPath(), p.ID)
			if err := np.Image.Action(); err != nil {
				return ResultFailed, err
			}
			return ResultRescan, nil
		}

		dmsg(DbgPart, "Filesystem action is being called: %v\n", np.FileSystem)

		// Make filesystem if one is here.
		if np.FileSystem != nil {
			// Create it if missing.
			rescan := false
			if p.FileSystem == nil {
				p.FileSystem = &FileSystem{parentPath: GeneratePartitionPath(p.parent.GetPath(), p.ID)}
				rescan = true
			}
			dmsg(DbgPart, "Calling filesystem action\n")

			if rs, err := p.FileSystem.Action(np.FileSystem); err != nil {
				return ResultFailed, err
			} else if rescan || rs == ResultRescan {
				return ResultRescan, nil
			}
			dmsg(DbgPart, "Filesystem action complete\n")
		}

		// Create physical volume on LVM2_member typed partition
		if np.physicalVolume != nil {
			rescan := false
			if p.physicalVolume == nil {
				p.physicalVolume = &PhysicalVolume{parentPath: GeneratePartitionPath(p.parent.GetPath(), p.ID)}
				rescan = true
			}

			if rs, err := p.physicalVolume.Action(np.physicalVolume); err != nil {
				return ResultFailed, err
			} else if rescan || rs == ResultRescan {
				return ResultRescan, nil
			}
		}

		dmsg(DbgPart, "Partition %d %s created successfully \n", p.ID, p.Name)
		return ResultSuccess, nil
	}

	// New partition creation starts here
	dmsg(DbgPart|DbgAction, "Starting to partition: %s\n", np.Name)
	dmsg(DbgPart|DbgAction, "  Parent PTable: %s\n", p.parent.GetPTable())
	dmsg(DbgPart|DbgAction, "  Parent NextStart: %s\n", p.parent.NextStart())

	// Calculate start and end positions
	start := p.parent.NextStart()
	if start == "" {
		start = "1048576B"
	}
	if np.Start != "" {
		start = np.Start
	}
	dmsg(DbgPart|DbgAction, "  Start %s\n", start)

	end := "100%"
	if np.Size != "" {
		dmsg(DbgPart|DbgAction, "  Config Size specified %s\n", np.Size)
		if np.Size == "REST" || np.Size == "-1" {
			np.End = "100%"
		} else {
			size, e := sizeParser(np.Size, sizeMultipliers)
			if e != nil {
				return ResultFailed, e
			}
			sb, e := sizeParser(start, sizeMultipliers)
			if e != nil {
				return ResultFailed, e
			}
			np.End = sizeStringer(sb+size-1, "B")
		}
	}
	if np.End != "" {
		if np.End == "REST" {
			end = "100%"
		}
		end = np.End
	}
	dmsg(DbgPart|DbgAction, "  End %s\n", end)

	// Set the next start
	ns, _ := sizeParser(end, sizeMultipliers)
	p.parent.SetNextStart(fmt.Sprintf("%dB", ns+1))

	pTable := np.parent.GetPTable()
	var err error
	sectorSize := getSectorSize(p.parent.GetPath())
	startBytes, _ := sizeParser(start, sizeMultipliers)
	endBytes, _ := sizeParser(end, sizeMultipliers)

	startSector := startBytes / uint64(sectorSize)
	endSector := endBytes / uint64(sectorSize)

	// Use createMBRPartition and createGPTPartition based on partition table
	switch pTable {
	case "msdos", "dos":
		err = createMBRPartition(p.parent.GetPath(), np.ID, startSector, endSector, np.Flag)
		if err != nil {
			return ResultFailed, err
		}

	case "gpt":
		err = createGPTPartition(p.parent.GetPath(), np.ID, startSector, endSector, np.Flag)
		if err != nil {
			return ResultFailed, err
		}

	default:
		return ResultFailed, fmt.Errorf("unsupported partition table type: %s", pTable)
	}

	// If LVM Raid member, set the raid type _after_ partition is created
	fsType := np.determineFSType()
	if isRaidPartition(fsType) && np.ID != 0 {
		dmsg(DbgPart|DbgAction, "  FSType %s. Setting raid on for ID: %d\n", fsType, p.ID)
		if _, err := runCommand("parted", p.parent.GetPath(), "set", strconv.Itoa(p.ID), "raid", "on"); err != nil {
			return ResultFailed, err
		}
	}
	return ResultRescan, err
}

func isRaidPartition(fsType string) bool {
	if fsType == "linux_raid_member" || fsType == "linux_raid" {
		return true
	}
	return false
}

func (p *Partition) String() string {
	if p == nil {
		return "unset"
	}
	dname := "unset"
	if p.parent != nil {
		dname = p.parent.GetName()
	}
	return fmt.Sprintf("<id:%d start:%s end:%s size:%s ptype:%s name:%s image:%v fs:%v pv:%v parent:%s imageApplied:%v>",
		p.ID, p.Start, p.End, p.Size, p.PType, p.Name, p.Image, p.FileSystem, p.physicalVolume, dname, p.imageApplied)
}
