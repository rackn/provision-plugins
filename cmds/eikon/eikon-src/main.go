package main

import (
	"flag"
	"fmt"
	"gopkg.in/yaml.v2"
	"os"
)

var debug DebugLevel

func main() {
	filePtr := flag.String("file", "config.yaml", "The system configuration file")
	dumpPtr := flag.Bool("dump", false, "Dump config objects")
	debugPtr := flag.Int("debug", 0, "Debug level (number bitfield)")
	formatPtr := flag.String("format", "yaml", "The format in which inventory needs to be displayed. Valid formats: yaml, json")
	inventoryPtr := flag.Bool("inventory", false, "Dump Inventory")
	validatePtr := flag.Bool("validate", false, "Validateconfig objects")
	mountPtr := flag.String("mount", "", "Mount the config file at the directory")
	unmountPtr := flag.String("unmount", "", "Unmount the config file at the directory")
	convertPrt := flag.Bool("convert", false, "Converts the given curtin config into an eikon plan. Assumes that the file passed in is a curtin config file.")

	flag.Parse()

	debug = DebugLevel(*debugPtr)
	format = *formatPtr

	// If convert pointer is set, just convert the file to eikon/plan and return
	if *convertPrt {
		ec := &Config{}
		byteValue, err := os.ReadFile(*filePtr)
		if err != nil {
			fmt.Printf("Error: error reading file: %s. %v", *filePtr, err)
			os.Exit(1)
		}

		cc := &CurtinConfig{}
		if err := yaml.Unmarshal(byteValue, &cc); err != nil {
			fmt.Printf("Error: error unmarshalling YAML from %s: %v", *filePtr, err)
		}

		// This is a valid curtin config, so we are going to try to convert it
		err = ec.convertToEikonPlan(cc)
		if err != nil {
			fmt.Printf("Error: error converting curtin config to eikon plan: %s. %v", *filePtr, err)
			os.Exit(1)
		}
		ec.Dump()
		os.Exit(0)
	}

	c := &Config{}
	if e := c.ScanSystem(); e != nil {
		fmt.Printf("Error: scanning %v\n", e)
		os.Exit(1)
	}
	if *dumpPtr || *inventoryPtr {
		c.Dump()
	}
	if *inventoryPtr {
		os.Exit(0)
	}

	ic := &Config{}
	if e := ic.ReadConfig(*filePtr); e != nil {
		fmt.Printf("Error: parsing config file: %s :%v\n", *filePtr, e)
		os.Exit(1)
	}
	if *validatePtr {
		ic.Dump()
		os.Exit(0)
	}
	if *dumpPtr {
		ic.Dump()
	}

	if *unmountPtr != "" {
		if e := ic.ScanSystem(); e != nil {
			fmt.Printf("Error: scanning %v\n", e)
			os.Exit(1)
		}
		_, err := ic.UnmountAll(*unmountPtr)
		if err != nil {
			fmt.Printf("Error: Unmounting %s: %v\n", *unmountPtr, err)
			os.Exit(1)
		}
		os.Exit(0)
	}
	if *mountPtr != "" {
		if e := ic.ScanSystem(); e != nil {
			fmt.Printf("Error: scanning %v\n", e)
			os.Exit(1)
		}
		// We just did a scan system - lets dump it for debugging
		ic.Dump()
		_, _, err := ic.MountAll(*mountPtr)
		if err != nil {
			fmt.Printf("Error: Mounting %s: %v\n", *mountPtr, err)
			os.Exit(1)
		}
		os.Exit(0)
	}

	rs, err := c.Apply(ic)
	if err != nil {
		fmt.Printf("Error Applying: %v\n", err)
		os.Exit(1)
	}
	for rs == ResultRescan {
		dmsg(DbgRescan, "Rescanning the system\n")
		if e := c.ScanSystem(); e != nil {
			fmt.Printf("Error: scanning %v\n", e)
			os.Exit(1)
		}
		if *dumpPtr {
			c.Dump()
		}
		rs, err = c.Apply(ic)
		if err != nil {
			fmt.Printf("Error Applying: %v\n", err)
			os.Exit(1)
		}
	}

	os.Exit(0)
}
