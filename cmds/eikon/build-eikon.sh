#!/bin/bash
mkdir -p embedded

cd eikon-src && GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -o ../embedded/eikon.amd64.linux
