package main

/*
  Name: kaholo-trigger-lacework-alert-webhook
  Description: Lacework Alert
  Documentation: Lacework Alert
  Path: kaholo-trigger-lacework/src/config.json

const translateTextMap = {"Any": "any", "Info": 5, "Low": 4,
                      "Medium": 3, "High": 2, "Critical": 1,
                      "5": 5, "4": 4, "3": 3, "2": 2, "1": 1};
const translateNumMap = {0: "", 1: "- Critical", 2: "- High Severity", 3: "- Medium Severity", 4: "- Low Severity", 5: "- Info"};

async function alertWebhook(req, res, settings, triggerControllers) {
  const body = req.body;
  try {
    const reqEType = body.event_type; // Get event type
    const reqEDescription = body.event_description; // Get event ID
    const reqESeverity = body.event_severity; // Get event severity
    const reqETitle = body.event_title; // Get event severity
    const recId = body.rec_id; // Get event severity
    if (!reqEType || !reqEDescription || !reqESeverity){
      res.status(400).send("bad lacework alert format");
      console.error("bad lacework alert format");
    }
    triggerControllers.forEach(trigger => {
      let {eventType, id, eventSeverity, includeHigherSev} = trigger.params;
      if (typeof(eventSeverity) === "string"){
        eventSeverity = translateTextMap[eventSeverity] || "any";
        if (eventSeverity === "any") {
          includeHigherSev = true, eventSeverity = 5;
        }
      }
      eventType = eventType || "Any";

      if (eventType !== "Any" && reqEType !== eventType) return;
      if (reqESeverity > eventSeverity) return;
      if (!includeHigherSev && reqESeverity !== eventSeverity) return;
      if (id && recId !== id) return;

      const msg = `${reqETitle} ${translateNumMap[reqESeverity]}`;
      trigger.execute(msg, body);
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }
}

module.exports = { alertWebhook }


*/

import (
	"encoding/json"
	"fmt"
	"reflect"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

var translateTextMap = map[string]interface{}{
	"Any":      "any",
	"Info":     5,
	"Low":      4,
	"Medium":   3,
	"High":     2,
	"Critical": 1,
	"5":        5,
	"4":        4,
	"3":        3,
	"2":        2,
	"1":        1,
}

type laceworkRequest struct {
	EventType        string `json:"event_type"`
	EventDescription string `json:"event_description"`
	EventSeverity    int    `json:"event_severity"`
	EventTitle       string `json:"event_title"`
	RecId            string `json:"rec_id"`
}

func kaholo_trigger_lacework_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {

	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &laceworkRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.EventType == "" || body.EventDescription == "" || body.EventSeverity == 0 {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("bad lacework alert format"))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["kaholo-trigger-lacework/alert-webhook/id"]; ok {
			if val.(string) != body.RecId {
				continue
			}
		}

		if val, ok := params["kaholo-trigger-lacework/alert-webhook/event-type"]; ok {
			if val.(string) != "Any" && val.(string) != body.EventType {
				continue
			}
		}

		var eventSeverity interface{}
		var includeHigherSev bool
		if val, ok := params["kaholo-trigger-lacework/alert-webhook/event-severity"]; ok {
			if reflect.TypeOf(val).Kind() == reflect.String {
				eventSeverity = translateTextMap[val.(string)]
				if eventSeverity == "any" {
					eventSeverity = 5
					includeHigherSev = true
				}
			}
		}

		if body.EventSeverity > eventSeverity.(int) {
			continue
		}

		if !includeHigherSev && (body.EventSeverity != eventSeverity.(int)) {
			continue
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	l.Infof("Completed processing lacework trigger for %v", body)
	return triggerResult, nil
}
