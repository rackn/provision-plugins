package main

/*
  Name: monday-trigger-status_update_webhook
  Description: Board Change
  Documentation: Board Change
  Path: kaholo-trigger-monday/src/config.json

function statusUpdateWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        if (body.hasOwnProperty("challenge")){
            return res.send(body); // send challenge back to monday
        }
        const reqBoardId = body.event.boardId; // Get board ID
        const eventType = body.event.type; // Get event type
        if (!eventType || !reqBoardId){
            return res.status(400).send("bad monday update format");
        }
        triggerControllers.forEach(trigger => {
            const boardId = trigger.params.BOARD_ID || settings.boardId;
            if (boardId && reqBoardId.toString() !== boardId) return;
            trigger.execute(`Monday ${eventType} Event - ${reqBoardId}`, body);
        });
        res.status(200).send("OK");
      }
    catch (err){
        res.status(422).send(err.message);
    }
}

module.exports = {
    STATUS_UPDATE_WEBHOOK: statusUpdateWebhook
}


*/

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type mondayRequest struct {
	Challenge string      `json:"challenge"`
	Event     mondayEvent `json:"event"`
}

type mondayEvent struct {
	BoardId string `json:"boardId"`
	Type    string `json:"type"`
}

func monday_trigger_status_update_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &mondayRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.Challenge != "" {
		// send challenge back to monday
		triggerResult.Data = triggerInput.Data
		return triggerResult, nil
	}

	if body.Event.BoardId == "" || body.Event.Type == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Event missing boardId and type details"))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["monday-trigger/status_update_webhook/board_id"]; ok {
			s := val.(string)
			if s == "" {
				if anp, ok := params["monday-trigger/board-id"]; ok {
					s = anp.(string)
				}
			}
			if s != "" && s != body.Event.BoardId {
				continue
			}
		}

		boardId := params["monday-trigger/status_update_webhook/board_id"].(string)
		if boardId == "" {
			boardId = params["monday-trigger/board-id"].(string)
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
