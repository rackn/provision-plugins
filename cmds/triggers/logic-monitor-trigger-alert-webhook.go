package main

/*
  Name: logic-monitor-trigger-alert-webhook
  Description: LogicMonitor Alert
  Documentation: LogicMonitor Alert
  Path: kaholo-trigger-logicmonitor/src/config.json

const { isMatch } = require("micromatch");

async function alertWebhook(req, res, settings, triggerControllers) {
  try {
    const body = req.body;
    const { alertType, alertId, alertStatus, admin, host, level, group } = body;
    triggerControllers.forEach((trigger) => {
      const { status: trigAlertStatus, alertType: trigAlertType, admin: trigAdmin, idPat,
        hostPat, groupPat, levelPat } = trigger.params;
      if (trigAlertStatus && trigAlertStatus !== "any" && trigAlertStatus !== alertStatus) return;
      if (trigAlertType && trigAlertType !== "any" && trigAlertType !== alertType) return;
      if (trigAdmin && trigAdmin !== admin) return;
      if (idPat && !isMatch(alertId, idPat)) return;
      if (hostPat && !isMatch(host, hostPat)) return;
      if (groupPat && !isMatch(group, groupPat)) return;
      if (levelPat && !isMatch(level, levelPat)) return;
      trigger.execute(`LogicMonitor ${alertType} ${alertId} ${level} ${alertStatus} - ${host}`, body);
     });
     res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message || JSON.stringify(err));
  }
}

module.exports = {
  alertWebhook
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type logicMonitorRequest struct {
	AlertType   string `json:"alertType"`
	AlertId     string `json:"alertId"`
	AlertStatus string `json:"alertStatus"`
	Admin       string `json:"admin"`
	Host        string `json:"host"`
	Level       string `json:"level"`
	Group       string `json:"group"`
}

func logic_monitor_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &logicMonitorRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["logic-monitor-trigger/alert-webhook/status"]; ok {
			s := val.(string)
			if s != "" && s != "any" && s != body.AlertStatus {
				continue
			}
		}

		if val, ok := params["logic-monitor-trigger/alert-webhook/alert-type"]; ok {
			s := val.(string)
			if s != "" && s != "any" && s != body.AlertType {
				continue
			}
		}

		if val, ok := params["logic-monitor-trigger/alert-webhook/admin"]; ok {
			s := val.(string)
			if s != "" && s != body.Admin {
				continue
			}
		}

		if val, ok := params["logic-monitor-trigger/alert-webhook/id-pat"]; ok {
			s := val.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(body.AlertId) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad alertId pattern: %v", rerr)
			}
		}

		if val, ok := params["logic-monitor-trigger/alert-webhook/host-pat"]; ok {
			s := val.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(body.Host) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad host pattern: %v", rerr)
			}
		}

		if val, ok := params["logic-monitor-trigger/alert-webhook/group-pat"]; ok {
			s := val.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(body.Group) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad group pattern: %v", rerr)
			}
		}

		if val, ok := params["logic-monitor-trigger/alert-webhook/level-pat"]; ok {
			s := val.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(body.Level) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad group pattern: %v", rerr)
			}
		}
		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	l.Infof("Completed processing logic monitor trigger for %v", body)
	return triggerResult, nil
}
