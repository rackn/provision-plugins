package main

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

/*
 * json_trigger_webhook - matches the secret and key, removes the secret, and passes on the rest as data.
 */
func json_trigger_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := map[string]interface{}{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	key, kok := body["key"]
	secret, sok := body["secret"]
	if !kok || !sok {
		triggerResult.Code = 400
		triggerResult.Data = []byte("JSON Trigger data must have a key and a secret")
		return triggerResult, nil
	}
	delete(body, "secret")

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["json-trigger/secret"]; ok {
			s := anp.(string)
			if s != secret {
				continue
			}
		}
		if anp, ok := params["json-trigger/key"]; ok {
			s := anp.(string)
			if s != key {
				continue
			}
		}
		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
