package main

/*
  Name: prometheus-trigger-alert-webhook
  Description: Alert
  Documentation: Alert
  Path: kaholo-trigger-prometheus/src/config.json

const { parseLabels } = require(`./helpers`);
const minimatch = require("minimatch");

async function alertWebhook(req, res, settings, triggerControllers) {
  const body = req.body;
  const groupKey = body.groupKey;
  if (!groupKey){
    return res.status(400).send(`Bad prometheus webhook format`);
  }
  try {
    // filter out controllers without matching group key
    triggerControllers = triggerControllers.filter(trigger => !trigger.params.groupKeyPat ||
                                                              minimatch(groupKey, trigger.params.groupKeyPat));
    for (let i = 0; i < body.alerts.length; i++){
      const alert = body.alerts[i];
      const {status: alertStatus, labels: alertLabels} = alert;
      const alertName = alertLabels.alertname || "";
      triggerControllers.forEach((trigger) => {
        const {alertNamePat, status} = trigger.params;
        const labels = parseLabels(trigger.params.labels);
        if (status && alertStatus !== status) return;
        if (alertNamePat && !minimatch(alertName, alertNamePat)) return;
        if (labels && !Object.entries(labels).every(([key, val]) =>   alertLabels.hasOwnProperty(key) &&
                                                                      alertLabels[key] == val)) return;
        trigger.execute(alertName, alert);
      });
    }
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }
}

module.exports = {
  alertWebhook
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type prometheusAlert struct {
	Status string      `json:"status"`
	Labels interface{} `json:"labels"`
}
type prometheusRequest struct {
	GroupKey string            `json:"groupKey"`
	Alerts   []prometheusAlert `json:"alerts"`
}

func prometheus_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &prometheusRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.GroupKey == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Bad Webhook Format"))
		return triggerResult, nil
	}

	count := 0
	for _, alert := range body.Alerts {
		for _, t := range triggerInput.Triggers {
			params, err := p.GetTriggerParams(t.Name)
			if err != nil {
				l.Errorf("Error getting %s trigger params: %v", err)
				continue
			}

			if val, ok := params["prometheus-trigger/alert-webhook/group-key-pat"]; ok {
				s := val.(string)
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(body.GroupKey) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad group key pattern: %v", rerr)
				}
			}

			if val, ok := params["prometheus-trigger/alert-webhook/status"]; ok {
				s := val.(string)
				if s != "" && s != alert.Status {
					continue
				}
			}

			// TODO: Do the rest!!!

			count++
			plugin.Publish("trigger", "fired", t.Name, body)
		}
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
