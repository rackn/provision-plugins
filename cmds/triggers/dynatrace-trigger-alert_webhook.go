package main

/*
  Name: dynatrace-trigger-alert_webhook
  Description: Dynatrace alert webhook
  Documentation: Dynatrace alert webhook
  Path: kaholo-trigger-dynatrace/src/config.json

function alertWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body
        const reqState = body.State
        triggerControllers.forEach(trigger => {
            const state = trigger.params.STATE || "Both";
            if (state !== "Both" && state !== reqState) return;
            trigger.execute(`Dynatrace Alert - ${trigger.name}`, body);
        });
        res.status(200).send("OK");
      }
    catch (err){
        res.status(422).send(err.message);
    }
}

module.exports = {
    ALERT_WEBHOOK: alertWebhook
}


*/

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func dynatrace_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := map[string]interface{}{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}
	state := ""
	if sobj, ok := body["State"]; ok {
		state = sobj.(string)
	}
	if state == "" {
		triggerResult.Code = 422
		triggerResult.Data = []byte("Missing state")
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["dynatrace-trigger/alert-webhook/state"]; ok {
			s := anp.(string)
			if s != "Both" && s != state {
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
