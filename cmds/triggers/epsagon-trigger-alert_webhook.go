package main

/*
  Name: epsagon-trigger-alert_webhook
  Description: Alert from Epsagon
  Documentation: Alert from Epsagon
  Path: kaholo-trigger-epsagon/src/config.json

function alertWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        triggerControllers.forEach(trigger => {
            trigger.execute(`Epsagon Alert - ${trigger.name}`, body);
        });
        res.status(200).send("OK");
      }
    catch (err){
        res.status(422).send(err.message);
    }
}

module.exports = {
    ALERT_WEBHOOK: alertWebhook
}


*/

import (
	"encoding/json"
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func epsagon_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}
	data := map[string]interface{}{}
	json.Unmarshal(triggerInput.Data, &data)
	for _, t := range triggerInput.Triggers {
		plugin.Publish("trigger", "fired", t.Name, data)
	}
	return triggerResult, nil
}
