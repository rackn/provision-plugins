package main

/*
  Name: azure-monitor-trigger-alert-webhook
  Description: Alert
  Documentation: Alert
  Path: kaholo-trigger-azure-monitoring/src/config.json

const micromatch = require("micromatch");
const parsers = require("./parsers");

async function alertWebhook(req, res, settings, triggerControllers) {
  try {
    const body = req.body;
    const sevirity = parsers.severity(body.data.essentials.severity);
    const {alertRule} = body.data.essentials;
    triggerControllers.forEach((trigger) => {
      let {alertNamePat, alertSeverity, includeHigherSeverity} = trigger.params;
      alertSeverity = parsers.severity(alertSeverity);

      if (alertNamePat && !micromatch.isMatch(alertRule, alertNamePat)) return;
      if (alertSeverity !== "Any"){
        if (alertSeverity < sevirity) return;
        if (!includeHigherSeverity && alertSeverity != sevirity) return;
      }

      trigger.execute(alertRule, body);
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message || JSON.stringify(err));
  }
}

module.exports = {
  alertWebhook
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type azureAlert struct {
	Data azureEssentials `json:"data"`
}
type azureEssentials struct {
	Severity  string `json:"severity"`
	AlertRule string `json:"alertRule"`
}

func azure_monitor_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	reqData := &azureAlert{}
	if jerr := json.Unmarshal(triggerInput.Data, &reqData); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}
	sevNum := azureSeverityToNumber(reqData.Data.Severity)

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["azure-monitor-trigger/alert-webhook/alert-name-pat"]; ok {
			pat := anp.(string)
			if r, rerr := regexp.Compile(pat); rerr == nil {
				if !r.MatchString(reqData.Data.AlertRule) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad alert pattern: %v", rerr)
			}
		}
		higher := false
		if bobj, ok := params["azure-monitor-trigger/alert-webhook/include-higher-severity"]; ok {
			higher = bobj.(bool)
		}

		if sobj, ok := params["azure-monitor-trigger/alert-webhook/alert-severity"]; ok {
			sev := sobj.(string)
			tSevNum := azureSeverityToNumber(sev)
			if sev != "Any" {
				if !higher && tSevNum != sevNum {
					continue
				}
				if higher && tSevNum > sevNum {
					continue
				}
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, reqData)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}

func azureSeverityToNumber(sev string) int {
	switch sev {
	case "Critical":
		return 0
	case "Error":
		return 1
	case "Warning":
		return 2
	case "Informational":
		return 3
	case "Verbose":
		return 4
	case "Sev0":
		return 0
	case "Sev1":
		return 1
	case "Sev2":
		return 2
	case "Sev3":
		return 3
	case "Sev4":
		return 4
	}
	return 5
}
