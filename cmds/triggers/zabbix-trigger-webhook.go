package main

/*
  Name: zabbix-trigger-webhook
  Description: Zabbix Webhook
  Documentation: Zabbix Webhook
  Path: kaholo-trigger-zabbix/src/config.json

async function webhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        const reqSecret = body.secret, reqSeverity = body.severity;
        triggerControllers.forEach(trigger => {
            const secret = trigger.params.SECRET, severity = trigger.params.SEVERITY;
            if (secret && reqSecret !== secret) return;
            if (severity && reqSeverity != severity) return;
            const msg = `${trigger.name} Alert ${reqSeverity ? `Severity ${reqSeverity}` : ""}`;
            trigger.execute(msg, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
        res.status(422).send(err.message);
    }
}

module.exports = {
    webhook
}

*/

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type zabbixRequest struct {
	Secret   string `json:"secret"`
	Severity string `json:"severity"`
}

func zabbix_trigger_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &zabbixRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["zabbix-trigger/webhook/secret"]; ok {
			s := val.(string)
			if s != "" && s != "any" && s != body.Secret {
				continue
			}
		}

		if val, ok := params["zabbix-trigger/webhook/severity"]; ok {
			s := val.(string)
			if s != "" && s != "any" && s != body.Severity {
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
