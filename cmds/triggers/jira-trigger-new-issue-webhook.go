package main

/*
  Name: jira-trigger-new-issue-webhook
  Description: Jira new issue webhook
  Documentation: Jira new issue webhook
  Path: kaholo-trigger-jira/src/config.json

function issueUpdateWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        const reqStatusName = body.issue.fields.status.name;
        const reqProjectKey = body.issue.fields.project.key;
        triggerControllers.forEach(trigger => {
            const {statusName, projectKey} = trigger.params;

            if (statusName && reqStatusName !== statusName) return;
            if (projectKey && reqProjectKey !== projectKey) return;

            trigger.execute(`Jira Update Issue - ${trigger.name}`, body);
        });
        res.status(200).send("OK");
      }
    catch (err){
        res.status(422).send(err.message);
    }
}

function newIssueWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        const reqProjectKey = body.issue.fields.project.key;
        triggerControllers.forEach(trigger => {
            const {projectKey} = trigger.params;
            if (projectKey && reqProjectKey !== projectKey) return;
            trigger.execute(`Jira New Issue - ${trigger.name}`, body);
        });
        res.status(200).send("OK");
      }
    catch (err){
        res.status(422).send(err.message);
    }
}

module.exports = {
    issueUpdateWebhook,
    newIssueWebhook
}


*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func jira_trigger_new_issue_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &jiraRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	reqProjectKey := body.Issue.Fields.Project.Key

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["jira-trigger/new-issue-webhook/project-key"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqProjectKey) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad project-key pattern: %v", rerr)
			}
		}
		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
