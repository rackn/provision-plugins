package main

/*
  Name: github-trigger-webhook-pr
  Description: Github pull request
  Documentation: Github pull request
  Path: kaholo-trigger-github/src/config.json

const { verifyRepoName, verifySignature, isMatch } = require("./helpers");

async function webhookPR(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        if (!body.pull_request && body.hook){
            // first request
            return res.status(200).send("OK");
        }
        const reqRepoName = body.repository.name; //Github repository name
        const reqSecret = req.headers["x-hub-signature-256"]
        const reqTargetBranch = body.pull_request.base.ref; //Get target branch name
        const reqSourceBranch = body.pull_request.head.ref; //Get source branch name
        const merged = body.pull_request.merged;
        const reqActionType =  body.action === "closed" ? (merged ? "merged" : "declined") : body.action;

        triggerControllers.forEach((trigger) => {
            if (!verifyRepoName(trigger, reqRepoName) || !verifySignature(trigger, reqSecret, body)) return;
            const {toBranch, fromBranch, actionType} = trigger.params;
            if (!isMatch(reqTargetBranch, toBranch)) return;
            // Check that From branch provided is same as request. If not provided consider as any.
            if (!isMatch(reqSourceBranch, fromBranch)) return;
            // Check that action type provided is same as request. If not provided consider as any.
            if (actionType && actionType !== "any" && actionType !== reqActionType) return;
            const msg = `Github ${reqRepoName} ${reqSourceBranch}->${reqTargetBranch} PR ${reqActionType}`
            trigger.execute(msg, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
        res.status(422).send(err.message);
    }
}
*/

import (
	"crypto"
	"crypto/hmac"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type githubRequest struct {
	Repository  githubRepository   `json:"repository"`
	PullRequest *githubPullRequest `json:"pull_request"`
	Hook        string             `json:"hook"`
	Action      string             `json:"action"`
	Ref         string             `json:"ref"`
}

type githubRepository struct {
	Name string `json:"name"`
}

type githubPullRequest struct {
	Base   githubBranch `json:"base"`
	Head   githubBranch `json:"head"`
	Merged bool         `json:"merged"`
}

type githubBranch struct {
	Ref string `json:"ref"`
}

func github_trigger_webhook_pr(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &githubRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.PullRequest == nil && body.Hook != "" {
		return triggerResult, nil
	}

	reqRepoName := body.Repository.Name //Github repository name
	reqSecret := ""
	reqSecrets := triggerInput.Header["x-hub-signature-256"]
	if len(reqSecrets) > 0 {
		reqSecret = reqSecrets[0]
	}
	reqTargetBranch := body.PullRequest.Base.Ref //Get target branch name
	reqSourceBranch := body.PullRequest.Head.Ref //Get source branch name
	merged := body.PullRequest.Merged
	reqActionType := body.Action
	if body.Action == "closed" {
		if merged {
			reqActionType = "merged"
		} else {
			reqActionType = "declined"

		}
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if anp, ok := params["github-trigger/webhook-pr/secret"]; ok {
			s := anp.(string)
			if s != "" {
				mac := hmac.New(crypto.SHA256.New, []byte(s))
				mac.Write(triggerInput.Data)
				shaHex := hex.EncodeToString(mac.Sum(nil))
				if shaHex != reqSecret[7:] {
					continue
				}
			}
		}
		if anp, ok := params["github-trigger/webhook-pr/repo-name"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqRepoName) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
			}
		}
		if anp, ok := params["github-trigger/webhook-pr/to-branch"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqTargetBranch) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad to-branch pattern: %v", rerr)
			}
		}
		if anp, ok := params["github-trigger/webhook-pr/from-branch"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqSourceBranch) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad from-branch pattern: %v", rerr)
			}
		}
		if anp, ok := params["github-trigger/webhook-pr/action-type"]; ok {
			s := anp.(string)
			if s != "any" {
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(reqActionType) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad action-type pattern: %v", rerr)
				}
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
