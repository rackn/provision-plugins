package main

/*
  Name: elementor-trigger-form-submit
  Description: Form Action After Submit
  Documentation: Form Action After Submit
  Path: kaholo-trigger-elementor/src/config.json

const {formatBody} = require("./helpers");

async function formSubmit(req, res, settings, triggerControllers) {
  try {
    const body = formatBody(req);
    const {form_id: reqFormId, form_name: reqFormName} = body;
    triggerControllers.forEach(trigger => {
        const {formId} = trigger.params;
        if (formId && reqFormId !== formId && reqFormName !== formId) return;
        trigger.execute(`${reqFormName} Submitted`, body);
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  };
}

module.exports = {
  formSubmit
};


*/

import (
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
)

func elementor_trigger_form_submit(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	l.Errorf("XXX: elementor_trigger format: %s", string(triggerInput.Data))

	triggerResult := &models.TriggerResult{
		Data:   []byte("Unknown event format."),
		Code:   400,
		Header: map[string][]string{},
	}
	return triggerResult, nil
}
