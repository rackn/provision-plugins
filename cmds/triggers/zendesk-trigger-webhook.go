package main

/*
   Name: zendesk-trigger-webhook
   Description: zendesk webhook
*/

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type zendeskWebhook struct {
	Title       string `json:"ticket_title"`
	Description string `json:"ticket_description"`
	Id          string `json:"ticket_id"`
	Raw         string `json:"raw"`
}

func zendesk_trigger_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &zendeskWebhook{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}
	body.Raw = string(triggerInput.Data)

	count := 0
	for _, t := range triggerInput.Triggers {
		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
