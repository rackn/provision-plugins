package main

/*
  Name: pingdom-trigger-webhook-alert
  Description: Pingdom Alert
  Documentation: Pingdom Alert
  Path: kaholo-trigger-pingdom/src/config.json

const minimatch = require("minimatch");

function webhookAlert(req, res, settings, triggerControllers) {
  try {
    const body = req.body;
    const {check_name, check_type} = body;
    if (!check_name || !check_type){
      return res.status(400).send("Bad Check Format");
    }
    triggerControllers.forEach(trigger => {
        const {namePat, checkType} = trigger.params;
        if (namePat && !minimatch(check_name, namePat)) return;
        if (checkType && checkType !== "any" && checkType !== check_type) return;
        trigger.execute(`${check_name}-${checkType} Check`, body);
    });
    res.status(200).send("OK");
  }
  catch (err){
      res.status(422).send(err.message);
  }
}

module.exports = {
  webhookAlert
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type pingdomRequest struct {
	CheckType string `json:"check_type"`
	CheckName string `json:"check_name"`
}

func pingdom_trigger_webhook_alert(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &pingdomRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.CheckType == "" || body.CheckName == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Bad Check Format"))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["pingdom-trigger/webhook-alert/name-pat"]; ok {
			s := val.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(body.CheckName) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad check name pattern: %v", rerr)
			}
		}

		if val, ok := params["pingdom-trigger/webhook-alert/check-type"]; ok {
			s := val.(string)
			if s != "" && s != "any" && s != body.CheckType {
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	l.Infof("Completed processing pingdom trigger for %v", body)
	return triggerResult, nil
}
