package main

/*
  Name: vsts-trigger-push-webhook
  Description: VSTS Push Webhook
  Documentation: VSTS Push Webhook
  Path: kaholo-trigger-vsts/SRC/config.json

const minimatch = require("minimatch")

function prWebhook (req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        let {repository, targetRefName, sourceRefName} = body.resource;
        if(!repository){
            return res.status(400).send("Repo not found");
        }
        targetRefName = targetRefName.slice(11); sourceRefName = sourceRefName.slice(11);
        triggerControllers.forEach(trigger => {
            const {REPO_URL: repoUrl, TO_BRANCH: toBranch, FROM_BRANCH: fromBranch} = trigger.params;
            if (toBranch && !minimatch(targetRefName, toBranch)) return;
            if (fromBranch && !minimatch(sourceRefName, fromBranch)) return;
            if (repoUrl && repository.remoteUrl !== repoUrl) return;
            const msg = `${sourceRefName}->${targetRefName} Pull Request`;
            trigger.execute(msg, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
        res.status(422).send(err.message);
    }
}

function pushWebhook (req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        const repository = body.resource.repository;
        const reqBranch = body.resource.refUpdates[body.resource.refUpdates.length - 1].name.slice(11); // Get target branch name
        if(!repository){
            return res.status(400).send("Repo not found");
        }
        triggerControllers.forEach(trigger => {
            const {REPO_URL: repoUrl, PUSH_BRANCH: branch} = trigger.params;
            if (branch && !minimatch(reqBranch, branch)) return;
            if (repoUrl && repository.remoteUrl !== repoUrl) return;
            const msg = `${reqBranch} Branch Push`;
            trigger.execute(msg, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
        res.status(422).send(err.message);
    }
}

module.exports = {
    prWebhook,
    pushWebhook
}

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func vsts_trigger_push_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &vtsRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.Resource.Repository.RemoteUrl == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Repo not found"))
		return triggerResult, nil
	}

	if len(body.Resource.SourceRefName) < 11 || len(body.Resource.TargetRefName) < 11 {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("Invalid SourceRefName or TargetRefName provided"))
		return triggerResult, nil
	}

	requestBranch := ""
	if len(body.Resource.RefUpdates) > 0 {
		requestBranch = body.Resource.RefUpdates[len(body.Resource.RefUpdates)-1].Name[11:]
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["vsts-trigger/push-webhook/push_branch"]; ok {
			s := val.(string)
			if s != "" {
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(requestBranch) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad request branch pattern: %v", s, rerr)
				}
			}
		}

		if val, ok := params["vsts-trigger/push-webhook/repo_url"]; ok {
			s := val.(string)
			if s != "" && s != body.Resource.Repository.RemoteUrl {
				continue
			}
		}
		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
