package main

/*
  Name: zapier-trigger-webhook-post
  Description: Post Request
  Documentation: Post Request
  Path: kaholo-trigger-zapier/src/config.json

const minimatch = require("minimatch");

async function webhookPost(req, res, settings, triggerControllers) {
  try {
    const key = req.headers["x-kaholo-key"];
    if (!key){
      return res.status(400).send("Bad Kaholo Webhook Configuration");
    }
    triggerControllers.forEach(trigger => {
        const {keyPat} = trigger.params;
        if (keyPat && !minimatch(key, keyPat)) return;
        trigger.execute(`Zapier ${key}`, req.body);
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }

}

module.exports = {
  webhookPost
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func zapier_trigger_webhook_post(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := map[string]interface{}{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	key := ""
	keys := triggerInput.Header["x-kaholo-key"] // TODO: Update this to a RackN key?
	if len(keys) > 0 {
		key = keys[0]
	}
	if key == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Bad Kaholo Webhook Configuration. Missing koholo key header."))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["zapier-trigger/webhook-post/key-pat"]; ok {
			s := val.(string)
			if s != "" {
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(key) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad alert pattern: %v", s, rerr)
				}
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
