package main

/*
  Name: slack-button-trigger-webhook-button
  Description: Button Action Trigger
  Documentation: Button Action Trigger
  Path: kaholo-trigger-slack-button/src/config.json

const { verifySignature } = require(`./helpers`);
const minimatch = require("minimatch");

async function webhookButton(req, res, settings, triggerControllers) {
  if (req.body.challenge){
    return res.status(200).send(req.body.challenge);
  }
  try {
    const body = JSON.parse(req.body.payload);
    if (body.type !== "interactive_message") return res.status(400).send("not an interactive message");
    const buttons = body.actions.filter(act => act.type === "button");

    triggerControllers.forEach(trigger => {
      if (!verifySignature(req, trigger)) return;
      const {valuePat} = trigger.params;
      buttons.forEach(button => {
        if (valuePat && !minimatch(button.value, valuePat)) return;
        trigger.execute(`Slack Button "${button.value}"`, button);
      })
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }
}

module.exports = {
  webhookButton
};

https://api.slack.com/reference/interaction-payloads/block-actions#examples

*/

import (
	"crypto"
	"crypto/hmac"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/url"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type slackRequest struct {
	Challenge string              `json:"challenge"`
	Payload   slackRequestPayload `json:"payload"`
}

type slackRequestPayload struct {
	Type    string               `json:"type"`
	Actions []slackPayloadAction `json:"actions"`
}

type slackPayloadAction struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

func slack_button_trigger_webhook_button(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	requestSignature := ""
	requestSignatures := triggerInput.Header["x-slack-signature"]
	if len(requestSignatures) > 0 {
		requestSignature = requestSignatures[0]
	}

	requestTimestamp := ""
	requestTimestamps := triggerInput.Header["x-slack-request-timestamp"]
	if len(requestTimestamps) > 0 {
		requestTimestamp = requestTimestamps[0]
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &slackRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.Challenge != "" {
		triggerResult.Data = []byte(body.Challenge)
		return triggerResult, nil
	}

	if body.Payload.Type != "interactive_message" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("not an interactive messages"))
		return triggerResult, nil
	}

	var buttonActions []slackPayloadAction
	for _, action := range body.Payload.Actions {
		if action.Type == "button" {
			buttonActions = append(buttonActions, action)
		}
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["slack-button-trigger/webhook-button/signing-secert"]; ok {
			s := val.(string)
			marshaledPayload, _ := json.Marshal(body.Payload)
			payloadString := string(marshaledPayload)
			encodedPayload := fmt.Sprintf("payload=%s", url.QueryEscape(payloadString))

			if s != "" {
				toSign := fmt.Sprintf("v0:%s:%s", requestTimestamp, encodedPayload)
				mac := hmac.New(crypto.SHA256.New, []byte(s))
				mac.Write([]byte(toSign))
				shaHex := hex.EncodeToString(mac.Sum(nil))

				if shaHex != requestSignature {
					continue
				}
			}
		}

		if val, ok := params["logic-monitor-trigger/alert-webhook/id-pat"]; ok {
			s := val.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				for _, action := range buttonActions {
					if !r.MatchString(action.Value) {
						continue
					}
					count++
					plugin.Publish("trigger", "fired", t.Name, body)
				}
			} else {
				l.Errorf("Trigger %s has bad button action pattern: %v", rerr)
			}
		}
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
