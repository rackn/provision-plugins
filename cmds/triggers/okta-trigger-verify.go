package main

/*
  Name: okta-trigger-verify
  Description: Verify
  Documentation: Verify
  Path: kaholo-trigger-okta/src/config.json

const micromatch = require("micromatch");
const parsers = require("./parsers");

async function alert(req, res, settings, triggerControllers) {
  try {
    const events = req.body.data.events;
    events.forEach(event => {
      const eventType = event.eventType;
      const severity = parsers.severity(event.severity);
      triggerControllers.forEach((trigger) => {
        let {alertEventTypes, alertSeverity, includeHigherSeverity} = trigger.params;
        alertEventTypes = alertEventTypes ? alertEventTypes.split("\n").map(eType => eType.trim()) : undefined;
        alertSeverity = parsers.severity(alertSeverity);

        if (alertEventTypes && !alertEventTypes.some(eType => micromatch.isMatch(eventType, eType))) return;
        if (alertSeverity !== "Any"){
          if (alertSeverity > severity) return;
          if (!includeHigherSeverity && alertSeverity != severity) return;
        }
        trigger.execute(`Okta Event: ${eventType} ${event.displayMessage}`, event);
      });
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message || JSON.stringify(err));
  }
}

async function verify(req, res, settings, triggerControllers) {
  try {
    const reqChanllenge = req.headers['x-okta-verification-challenge'];
    if (reqChanllenge){
      res.status(200).send({"verification" : reqChanllenge});
      triggerControllers.forEach((trigger) => {
        trigger.execute(`Okta Verified Event Hook`, req.body || {});
      });
      return "Okta Event Hook Verified";
    }
    res.status(422).send("Unknown Request Format");
  }
  catch (err){
    res.status(422).send(err.message || JSON.stringify(err));
  }
}

module.exports = {
  alert,
  verify
};

*/

import (
	"encoding/json"
	"fmt"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

func okta_trigger_verify(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &oktaRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	reqChanllenge := ""
	reqChanllenges := triggerInput.Header["x-okta-verification-challenge"]
	if len(reqChanllenges) > 0 {
		reqChanllenge = reqChanllenges[0]
	}

	if reqChanllenge != "" {
		input := map[string]string{"verification": reqChanllenge}
		triggerResult.Data = []byte(fmt.Sprintf("%v", input))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
