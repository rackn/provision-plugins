package main

/*
  Name: unbounce-trigger-webhook-submit
  Description: Form Submit
  Documentation: Form Submit
  Path: kaholo-trigger-unbounce/src/config.json

const minimatch = require("minimatch");

async function webhookSubmit(req, res, settings, triggerControllers) {
  try {
    const body = req.body;

    const {page_name: reqPName, page_url: reqPUrl, variant: reqVariant} = body;
    if (!reqPName || !reqPUrl || !reqVariant){
      return res.status(400).send("Bad Check Format");
    }
    triggerControllers.forEach(trigger => {
        const {pageName, pageUrl, variant} = trigger.params;
        if (pageName && !minimatch(reqPName, pageName)) return
        if (pageUrl && !minimatch(reqPUrl, pageUrl)) return;
        if (variant && reqVariant !== variant) return;
        trigger.execute(`${reqPName} Submitted`, JSON.parse(body["data.json"]));
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }
}

module.exports = {
  webhookSubmit
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type unbounceRequest struct {
	PageName string `json:"page_name"`
	PageUrl  string `json:"page_url"`
	Variant  string `json:"variant"`
}

func unbounce_trigger_webhook_submit(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &unbounceRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.PageName == "" || body.PageUrl == "" || body.Variant == "" {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Bad Check Format"))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["unbounce-trigger/webhook-submit/page-name"]; ok {
			s := val.(string)
			if s != "" {
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(body.PageName) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad page name pattern: %v", s, rerr)
				}
			}
		}

		if val, ok := params["unbounce-trigger/webhook-submit/page-url"]; ok {
			s := val.(string)
			if s != "" {
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(body.PageUrl) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad page url pattern: %v", s, rerr)
				}
			}
		}

		if val, ok := params["unbounce-trigger/webhook-submit/variant"]; ok {
			s := val.(string)
			if s != "" && s != body.Variant {
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
