package main

/*
  Name: git-lab-trigger-mr-webhook
  Description: GitLab Webhook merge request
  Documentation: GitLab Webhook merge request
  Path: kaholo-trigger-gitlab/src/config.json

const minimatch = require("minimatch")

function mrWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        if(!body.repository) {
            return res.status(400).send("Repository not found");
        }
        const reqUrl = body.project.http_url; //Gitlab Http URL
        const {target_branch: reqTarget, source_branch: reqSrc} = body.object_attributes; //Get source and target branch names
        const reqSecret = req.get('X-Gitlab-Token');

        triggerControllers.forEach((trigger) => {
            const { SECRET: trigSecret, TO_BRANCH: target,
                    FROM_BRANCH: src, REPO_URL: repoUrl} = trigger.params;
            const secret = trigSecret || settings.SECRET;

            if (secret && reqSecret !== secret) return;
            if (target && !minimatch(reqTarget, target)) return;
            if (src && !minimatch(reqSrc, src)) return;
            if (repoUrl && !minimatch(reqUrl, repoUrl)) return;

            const msg = `${reqSrc}->${reqTarget} Merge Request`;
            trigger.execute(msg, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
        res.status(422).send(err.toString());
    }
}
*/

import (
	"encoding/json"
	"fmt"
	"regexp"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type gitlabRequest struct {
	Ref              string            `json:"ref"`
	Repository       gitlabRepository  `json:"repository"`
	Project          gitlabProject     `json:"project"`
	ObjectAttributes map[string]string `json:"object_attributes"`
}

type gitlabRepository struct {
	GitHttpUrl string `json:"git_http_url"`
}

type gitlabProject struct {
	HttpUrl string `json:"http_url"`
}

func git_lab_trigger_mr_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &gitlabRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	reqURL := body.Project.HttpUrl
	targetBranch, _ := body.ObjectAttributes["target_branch"]
	sourceBranch, _ := body.ObjectAttributes["source_branch"]
	reqSecret := ""
	reqSecrets := triggerInput.Header["X-Gitlab-Token"]
	if len(reqSecrets) > 0 {
		reqSecret = reqSecrets[0]
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["git-lab-trigger/mr-webhook/secret"]; ok {
			s := anp.(string)
			if s == "" {
				if anp, ok := params["git-lab-trigger/secret"]; ok {
					s = anp.(string)
				}
			}
			if s != "" && s != reqSecret {
				continue
			}
		}
		if anp, ok := params["git-lab-trigger/mr-webhook/repo-url"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqURL) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-url pattern: %v", rerr)
			}
		}
		if anp, ok := params["git-lab-trigger/mr-webhook/to_branch"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(targetBranch) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad to_branch pattern: %v", rerr)
			}
		}
		if anp, ok := params["git-lab-trigger/mr-webhook/from_branch"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(sourceBranch) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad from_branch pattern: %v", rerr)
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count
	return triggerResult, nil
}
