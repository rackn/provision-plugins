package main

/*
  Name: prtg-trigger-post-notification
  Description: PRTG HTTP Notification
  Documentation: PRTG HTTP Notification
  Path: kaholo-trigger-prtg/src/config.json

const micromatch = require("micromatch");

function postNotification(req, res, settings, triggerControllers) {
  try {
    const body = req.body;
    const {sensor, device, deviceid, sensorid, priority, message} = body;
    if (!sensor || !device || !deviceid || !sensorid || !priority || !message){
      return res.status(400).send("Bad Kahoko Notification Format");
    }
    triggerControllers.forEach(trigger => {
        const {sensorPat, devicePat, priority: trigPrior, includeHigher} = trigger.params;
        if (sensorPat && sensorid !== sensorPat && !micromatch.isMatch(sensor, sensorPat)) return;
        if (devicePat && deviceid !== devicePat && !micromatch.isMatch(device, devicePat)) return;
        if (trigPrior && trigPrior !== "Any"){
          if (!includeHigher && trigPrior != priority) return;
          if (parseInt(trigPrior) > priority) return;
        }
        trigger.execute(`${sensor} Notification - ${message}`, body);
    });
    res.status(200).send("OK");
  }
  catch (err){
      res.status(422).send(err.message);
  }
}

module.exports = {
  postNotification
};

*/

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type prtgRequest struct {
	Sensor   string `json:"sensor"`
	SensorId string `json:"sensorid"`
	Device   string `json:"device"`
	DeviceId string `json:"deviceid"`
	Priority int    `json:"priority"`
	Message  string `json:"message"`
}

func prtg_trigger_post_notification(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &prtgRequest{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	if body.Sensor == "" || body.SensorId == "" || body.Device == "" || body.DeviceId == "" || body.Priority == 0 {
		triggerResult.Code = 400
		triggerResult.Data = []byte(fmt.Sprintf("Bad Webhook Format"))
		return triggerResult, nil
	}

	count := 0
	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}

		if val, ok := params["prtg-trigger/post-notification/sensor-pat"]; ok {
			s := val.(string)
			if s != "" && s != body.SensorId {
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(body.Sensor) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad alertId pattern: %v", rerr)
				}
			}
		}

		if val, ok := params["prtg-trigger/post-notification/device-pat"]; ok {
			s := val.(string)
			if s != "" && s != body.DeviceId {
				if r, rerr := regexp.Compile(s); rerr == nil {
					if !r.MatchString(body.Device) {
						continue
					}
				} else {
					l.Errorf("Trigger %s has bad alertId pattern: %v", rerr)
				}
			}
		}

		if val, ok := params["prtg-trigger/post-notification/priority"]; ok {
			s := val.(string)
			if s != "" && s != "Any" {
				if priority, err := strconv.Atoi(s); err != nil {
					if priority > body.Priority {
						continue
					}
					if val2, ok := params["prtg-trigger/post-notification/include-higher"]; ok {
						b := val2.(bool)
						if !b && priority != body.Priority {
							continue
						}
					}
				}
				continue
			}
		}

		count++
		plugin.Publish("trigger", "fired", t.Name, body)
	}
	triggerResult.HasCount = true
	triggerResult.Count = count

	return triggerResult, nil
}
