Author: RackN
CodeSource: https://gitlab.com/rackn/provision-plugins
Color: black
Copyright: RackN
Description: Azure KeyVault secrets store
DisplayName: Azure KeyVault
DocUrl: https://docs.rackn.io/stable/redirect/?ref=rs_content_azkeyvault
Documentation: |-
  # DRP's Integration with Microsoft Azure KeyVault

  DRP integrates with Microsoft's Azure KeyVault to offer a diversified secret management solution. With the this plugin, users have the option to retrieve secrets from Azure KeyVault, expanding the range of DRP's robust and secure secret management capabilities.

  ## Plugin Configuration

  The plugin assumes that the azure security credentials are in the home directory of the user running DRP.

  Optionally, set a cache timeout to store secrets from Azure KeyVault temporarily in memory, reducing excessive requests to the KeyVault server. This duration is in seconds, defaulting to 300 seconds.

    * `azkeyvault/cache-timeout`: Duration (in seconds) to cache secrets in-memory.

  ## Operational Configuration

  In addition to the plugin configuration, define a `lookupUri` for the secret's location:

    * `decrypt/lookup-uri`:
        * Denotes the secret's storage location.
        * Format: `azkeyvault://<vault hostname>?secret=<secret or cert name>&format=json&type=[secret|cert]`
        * Example: `azkeyvault://key-vault-test?secret=name-of-json-encoded-secret-in-value&format=json`
        * Option: vault hostname is the name of the key vault that will get prepended to .vault.azure.net
        * Option: secret is the path to the secret or certificate
        * Option: format is optional and the value is returned directly as a string or if set to json marshaled into an object.
        * Option: type is secret for a secret or cert for a certificate.  If cert, then format is ignored and a string is returned.

  !!! note
      This assume https based communications.

  The URI can also have parameter expansions.

  ```
  azkeyvault://key-vault-test?secret=test-{{.Machine.Name}}-json&format=json
  ```

  ## Usage

  After completing both configurations, use the plugin:

    ```sh
    # Set a machine's IPMI password to a Vault secret:
    drpcli machines set Name:foo param ipmi/password to '{ "LookupUri": "azkeyvault://key-vault-test?secret=test-json&format=json" }'

    # Retrieve the machine's IPMI password:
    drpcli machines get Name:foo param ipmi/password --decode
    ```

    This assumes that there is a key vault accessible by the credentials of `key-vault-test` with a secret `test-json` that is stored json encoded.

Icon: bullseye
License: RackN
Name: azkeyvault
Order: "800"
Prerequisites: "drp-community-content: >=4.8.0"
Source: RackN
Tags: advanced,rackn,provision,secrets,microsoft,azure
