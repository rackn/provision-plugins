package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml Version=$version"
//go:generate sh -c "drpcli contents document content.yaml > azkeyvault.rst"

import (
	_ "embed"
	"fmt"
	"os"
	"time"

	utils2 "github.com/VictorLowther/jsonpatch2/utils"
	"gitlab.com/rackn/logger"
	v4 "gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

const (
	DefaultCacheTimeout = 5 * time.Minute
)

var (
	//go:embed content.yaml
	content string
	version = v4.RSVersion
	def     = models.PluginProvider{
		Name:          "azkeyvault",
		Version:       version,
		AutoStart:     true,
		PluginVersion: 4,
		AvailableActions: []models.AvailableAction{
			{Command: "decrypt",
				Model: "machines",
				RequiredParams: []string{
					"decrypt/lookup-uri",
				},
				OptionalParams: []string{
					"azkeyvault/cache-timeout",
				},
			},
			{Command: "clearMachineCache",
				Model: "machines",
			},
			{Command: "clearCache",
				Model: "system",
			},
		},
		Content: content,
	}
)

type Plugin struct {
	// cache represents the temporary azkeyvault cache
	cache *utils.Cache

	// cacheTimeout represents the amount of time for which the value retrieved
	// from azkeyvault would be cached in memory
	cacheTimeout time.Duration
}

func (p *Plugin) Config(l logger.Logger, session *api.Client, config map[string]interface{}) *models.Error {
	var ok bool
	timeoutSeconds, ok := config["azkeyvault/cache-timeout"].(int)
	if !ok {
		p.cacheTimeout = DefaultCacheTimeout
	} else {
		p.cacheTimeout = time.Duration(timeoutSeconds) * time.Second
	}

	// Set up Cache
	p.cache = utils.CreateCache(p.cacheTimeout, p.fetchSecretFromVault)

	return nil
}

func (p *Plugin) Action(l logger.Logger, ma *models.Action) (answer interface{}, err *models.Error) {
	switch ma.Command {
	case "decrypt":
		answer, err = p.decryptAction(l, ma.Model, ma.Params)
	case "clearMachineCache":
		err = p.clearMachineCache(l, ma.Model)
	case "clearCache":
		err = p.clearCache(l)
	default:
		err = &models.Error{Code: 404,
			Model:    "Plugin",
			Key:      "azkeyvault",
			Type:     "rpc",
			Messages: []string{fmt.Sprintf("Unknown command: %s", ma.Command)}}
	}
	return
}

func (p *Plugin) decryptAction(l logger.Logger, model interface{}, params map[string]interface{}) (interface{}, *models.Error) {
	lookupUri := utils.GetParamOrString(params, "decrypt/lookup-uri", "")
	l.Infof("Decrypting azkeyvault secret")

	// Get the machine details
	machine := &models.Machine{}
	machine.Fill()
	if err := utils2.Remarshal(model, &machine); err != nil {
		return "", utils.ConvertError(400, err)
	}

	// Check the cache
	secret, err := p.cache.Get(machine.Uuid.String(), lookupUri)
	if err != nil {
		return nil, utils.ConvertError(500, err)
	}

	return secret, nil
}

func (p *Plugin) clearMachineCache(l logger.Logger, model interface{}) *models.Error {
	l.Infof("Clearing machine cache")
	// Grab the machine
	machine := &models.Machine{}
	machine.Fill()
	if err := utils2.Remarshal(model, &machine); err != nil {
		return utils.ConvertError(400, err)
	}

	// clear all machine data from cache
	p.cache.ClearMachineCache(machine.UUID())

	return nil
}

func (p *Plugin) clearCache(l logger.Logger) *models.Error {
	l.Infof("Clearing system cache")

	// clear all machine data from cache
	p.cache.Empty()

	return nil
}

func main() {
	plugin.InitApp("azkeyvault", "Retrieve secret from Azure KeyVault", version, &def, &Plugin{})
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
