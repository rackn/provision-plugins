package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/url"

	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/security/keyvault/azcertificates"
	"github.com/Azure/azure-sdk-for-go/sdk/security/keyvault/azsecrets"
	"gitlab.com/rackn/provision/v4/models"
)

// azkeyvault url format:  azkeyvault://<vault>?secret=<secret>&format=json&type=[secert|cert]
// vault is the name of the key vault that will get prepended to .vault.azure.net
// secret is the path to the secret or certificate
// format is optional and the value is returned directly as a string or if set to json marshaled into an object.
// type is secret for a secret or cert for a certificate.  If cert, then format is ignored and a string is returned.

func (p *Plugin) fetchSecretFromVault(lookupUri string, machineId string) (interface{}, *models.Error) {
	err := &models.Error{Type: "plugin", Model: "", Key: "azkeyvault"}

	// Parse the lookup URI and get the scheme and host
	// example: "plugin-name://secret-key?path=path-to-secret"
	uri, verr := url.ParseRequestURI(lookupUri)
	if verr != nil {
		err.Code = 400
		err.Errorf("fetch secret: invalid lookupUri provided")
		return "", err
	}

	vault := fmt.Sprintf("https://%s.vault.azure.net", uri.Host)
	path := uri.Query()["secret"][0]
	format := ""
	if uri.Query()["format"] != nil {
		format = uri.Query()["format"][0]
	}
	azType := "secret"
	if uri.Query()["type"] != nil {
		azType = uri.Query()["type"][0]
	}

	//Create a credential using the NewDefaultAzureCredential type.
	cred, cerr := azidentity.NewDefaultAzureCredential(nil)
	if cerr != nil {
		err.Code = 400
		err.AddError(cerr)
		return nil, err
	}

	var finalResult interface{}
	switch azType {
	case "secret":
		//Establish a connection to the Key Vault client
		client, cerr := azsecrets.NewClient(vault, cred, nil)
		if cerr != nil {
			err.Code = 400
			err.AddError(cerr)
			return nil, err
		}

		secret, verr := client.GetSecret(context.TODO(), path, "", nil)
		if verr != nil {
			err.Code = 400
			err.AddError(verr)
			return "", err
		}

		if secret.Value == nil {
			err.Code = 404
			err.Errorf("Secret %s not found in azkeyvault.", path)
			return "", err
		}

		// If a format is specified, then we will also need convert data
		switch format {
		case "json":
			verr = json.Unmarshal([]byte(*secret.Value), &finalResult)
			if verr != nil {
				// There was an error converting the value to json, most like it is not valid json so just return
				finalResult = *secret.Value
			}
		default:
			finalResult = *secret.Value
		}
	case "cert":
		//Establish a connection to the Key Vault client
		client, cerr := azcertificates.NewClient(vault, cred, nil)
		if cerr != nil {
			err.Code = 400
			err.AddError(cerr)
			return nil, err
		}

		cert, verr := client.GetCertificate(context.TODO(), path, "", nil)
		if verr != nil {
			err.Code = 400
			err.AddError(verr)
			return "", err
		}

		if cert.CER == nil {
			err.Code = 404
			err.Errorf("Certificate %s not found in azkeyvault", path)
			return "", err
		}

		finalResult = string(cert.CER)
	}

	return finalResult, nil
}
