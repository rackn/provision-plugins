package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml Version=$version"
//go:generate sh -c "drpcli contents document content.yaml > awssecman.rst"

import (
	"context"
	_ "embed"
	"fmt"
	"os"
	"time"

	utils2 "github.com/VictorLowther/jsonpatch2/utils"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"gitlab.com/rackn/logger"
	v4 "gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

const (
	DefaultCacheTimeout = 5 * time.Minute
)

var (
	//go:embed content.yaml
	content string
	version = v4.RSVersion
	def     = models.PluginProvider{
		Name:          "awssecman",
		Version:       version,
		AutoStart:     true,
		PluginVersion: 4,
		AvailableActions: []models.AvailableAction{
			{Command: "decrypt",
				Model: "machines",
				RequiredParams: []string{
					"decrypt/lookup-uri",
				},
				OptionalParams: []string{
					"awssecman/cache-timeout",
					"awssecman/aws-key-id",
					"awssecman/aws-secret",
					"awssecman/aws-region",
					"awssecman/aws-profile",
				},
			},
			{Command: "clearMachineCache",
				Model: "machines",
			},
			{Command: "clearCache",
				Model: "system",
			},
		},
		Content: content,
	}
)

type Plugin struct {
	// cache represents the temporary awssecman cache
	cache *utils.Cache

	// awscfg is the config for accessing things
	awscfg aws.Config

	// cacheTimeout represents the amount of time for which the value retrieved
	// from awssecman would be cached in memory
	cacheTimeout time.Duration
}

// VaultOptions is a set of options that can be passed in as
// query parameters into the lookup URI
type VaultOptions struct {
	// format is the format in which the secret is stored
	// for example the lookup uri could be
	// awssecman://aws?secret=secret&format=<format>
	// this would mean the secret is stored as json and the plugin
	// would read it and convert it before returning the secret
	// default which nothing is passed is just `string`
	format string

	// secret denotes the secret
	// example: awssecman://aws?secret=<secret name>
	secret string
}

func (p *Plugin) Config(l logger.Logger, session *api.Client, cfg map[string]interface{}) *models.Error {
	var ok bool
	timeoutSeconds, ok := cfg["awssecman/cache-timeout"].(int)
	if !ok {
		p.cacheTimeout = DefaultCacheTimeout
	} else {
		p.cacheTimeout = time.Duration(timeoutSeconds) * time.Second
	}

	// Setup the access creds
	awskid, _ := cfg["awssecman/aws-key-id"].(string)
	awssecret, _ := cfg["awssecman/aws-secert"].(string)
	awsregion, _ := cfg["awssecman/aws-region"].(string)
	awsprofile, _ := cfg["awssecman/aws-profile"].(string)

	var lf [](func(*config.LoadOptions) error)
	if awskid != "" {
		lf = [](func(*config.LoadOptions) error){
			config.WithCredentialsProvider(credentials.NewStaticCredentialsProvider(awskid, awssecret, "")),
		}
	} else {
		lf = [](func(*config.LoadOptions) error){
			config.WithSharedConfigProfile(awsprofile),
		}
	}
	if awsregion != "" {
		lf = append(lf, config.WithRegion(awsregion))
	}
	awscfg, cerr := config.LoadDefaultConfig(context.TODO(), lf...)
	if cerr != nil {
		err := &models.Error{Type: "plugin", Model: "plugins", Key: "awssecman"}
		err.AddError(cerr)
		return err
	}
	p.awscfg = awscfg

	// Set up Cache
	p.cache = utils.CreateCache(p.cacheTimeout, p.fetchSecretFromVault)

	return nil
}

func (p *Plugin) Action(l logger.Logger, ma *models.Action) (answer interface{}, err *models.Error) {
	switch ma.Command {
	case "decrypt":
		answer, err = p.decryptAction(l, ma.Model, ma.Params)
	case "clearMachineCache":
		err = p.clearMachineCache(l, ma.Model)
	case "clearCache":
		err = p.clearCache(l)
	default:
		err = &models.Error{Code: 404,
			Model:    "Plugin",
			Key:      "awssecman",
			Type:     "rpc",
			Messages: []string{fmt.Sprintf("Unknown command: %s", ma.Command)}}
	}
	return
}

func (p *Plugin) decryptAction(l logger.Logger, model interface{}, params map[string]interface{}) (interface{}, *models.Error) {
	lookupUri := utils.GetParamOrString(params, "decrypt/lookup-uri", "")
	l.Infof("Decrypting awssecman secret")

	// Get the machine details
	machine := &models.Machine{}
	machine.Fill()
	if err := utils2.Remarshal(model, &machine); err != nil {
		return "", utils.ConvertError(400, err)
	}

	// Check the cache
	secret, err := p.cache.Get(machine.Uuid.String(), lookupUri)
	if err != nil {
		return nil, utils.ConvertError(500, err)
	}

	return secret, nil
}

func (p *Plugin) clearMachineCache(l logger.Logger, model interface{}) *models.Error {
	l.Infof("Clearing machine cache")
	// Grab the machine
	machine := &models.Machine{}
	machine.Fill()
	if err := utils2.Remarshal(model, &machine); err != nil {
		return utils.ConvertError(400, err)
	}

	// clear all machine data from cache
	p.cache.ClearMachineCache(machine.UUID())

	return nil
}

func (p *Plugin) clearCache(l logger.Logger) *models.Error {
	l.Infof("Clearing system cache")

	// clear all machine data from cache
	p.cache.Empty()

	return nil
}

func main() {
	plugin.InitApp("awssecman", "Retrieve secret from Amazon Secrets Manager", version, &def, &Plugin{})
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
