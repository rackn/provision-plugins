package main

import (
	"context"
	"encoding/json"
	"net/url"

	"github.com/aws/aws-sdk-go-v2/service/secretsmanager"
	"gitlab.com/rackn/provision/v4/models"
)

func (p *Plugin) fetchSecretFromVault(lookupUri string, machineId string) (interface{}, *models.Error) {
	err := &models.Error{Type: "plugin", Model: "", Key: "awssecman"}

	// Parse the lookup URI and get the scheme and host
	// example: "awssecman://aws?secret=secret-name"
	uri, verr := url.ParseRequestURI(lookupUri)
	if verr != nil {
		err.Code = 400
		err.Errorf("fetch secret: invalid lookupUri provided")
		return "", err
	}

	var vault VaultOptions
	vault.secret = uri.Query()["secret"][0]
	if uri.Query()["format"] != nil {
		vault.format = uri.Query()["format"][0]
	}

	ssClient := secretsmanager.NewFromConfig(p.awscfg)

	secret, verr := ssClient.GetSecretValue(context.TODO(), &secretsmanager.GetSecretValueInput{SecretId: &vault.secret})
	if verr != nil {
		err.Code = 500
		err.Errorf(verr.Error())
		return "", err
	}

	if secret.SecretString == nil {
		err.Code = 404
		err.Errorf("Secret %s not found in awssecman.", vault.secret)
		return "", err
	}

	var finalResult interface{}
	// If a format is specified, then we will also need convert data
	switch vault.format {
	case "json":
		verr = json.Unmarshal([]byte(*secret.SecretString), &finalResult)
		if verr != nil {
			// There was an error converting the value to json, most like it is not valid json so just return
			return secret.SecretString, nil
		}
	default:
		finalResult = secret.SecretString
	}

	return finalResult, nil
}
