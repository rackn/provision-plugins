package main

import (
	"bytes"
	"encoding/json"
	utils2 "github.com/VictorLowther/jsonpatch2/utils"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/models"
	"log"
	"sync"
	"time"
)

const ipmiScanResult = "ipmi_scan_results"

type State string

const (
	StateCreated  State = "created"
	StateRunning  State = "running"
	StateFinished State = "finished"
	StateCanceled State = "canceled"
)

// IpmiScanResult is used to hold the results of an IPMI range scan
// plugin action: scan-range
type IpmiScanResult struct {
	models.Bundled
	models.Validation
	models.Access
	models.Meta

	// Fields for RawModel
	Id     string
	Type   string
	Params map[string]interface{}

	// Scan start address
	ScanStartAddress string
	// Scan end address
	ScanEndAddress string
	// The latest scanned address, this will be used in case the scan stops in the middle for some reason and needs to
	// be restarted
	LowestScannedIndex int
	// The lowest scanned address which is present at the lowest scanned index
	LowestScannedAddress string
	// The total number of IPs being scanned
	// The highest possible value for the LowestScannedIndex will be ScanSize - 1
	ScanSize int
	// A list of UUIDs created as a part of this scan
	CreatedUuids []string
	// Any errors encountered during the range-scan
	ScanErrors []string
	// The state of the current scan, can be one of `running`, `finished` or `canceled`
	State State
	// When an action is created, a job ID is automatically associated with it. This is that job ID
	ScanActionJobId string
	// The time the range-scan started running.
	ScanStartTime time.Time
	// The time the range-scan failed or finished.
	ScanEndTime time.Time
	// The time the range-scan db object got updated.
	ScanUpdatedTime time.Time
	// If set to true, this scan is a blocking call and the RunAction command will wait for the scan to complete
	// Defaults to false
	ScanWaitForComplete bool
	// If ScanWaitForComplete is set to true, use this to put a limit on how long to wait for the scan to complete
	// Defaults to 5 minutes
	ScanWaitTimeoutMins time.Duration

	// Fields only for tracking but not for saving
	ipScanState        []bool        // A mapping of the IP index and whether the IP has been scanned
	scanUpdateInterval time.Duration // Represents how long to wait before saving scan results
	mux                sync.Mutex    // Ensure safe handling of this object

	orig *models.RawModel
}

func newRawModel() *models.RawModel {
	r := &models.RawModel{"Type": ipmiScanResult}
	r.Fill()
	return r
}

func (r *IpmiScanResult) Key() string {
	return r.Id
}

func (r *IpmiScanResult) KeyName() string {
	return "Id"
}

func (r *IpmiScanResult) Prefix() string {
	return ipmiScanResult
}

// Clone the ipmi scan result
func (r *IpmiScanResult) Clone() *IpmiScanResult {
	r2 := &IpmiScanResult{}
	buf := bytes.Buffer{}
	enc, dec := json.NewEncoder(&buf), json.NewDecoder(&buf)
	if err := enc.Encode(r); err != nil {
		log.Panicf("Failed to encode ipmi scan result:%s: %v", r.Id, err)
	}
	if err := dec.Decode(r2); err != nil {
		log.Panicf("Failed to decode ipmi scan result:%s: %v", r.Id, err)
	}
	return r2
}

func (r *IpmiScanResult) Fill() {
	if r.Meta == nil {
		r.Meta = models.Meta{}
	}
	if r.Errors == nil {
		r.Errors = []string{}
	}
	if r.ScanErrors == nil {
		r.ScanErrors = []string{}
	}
	if r.Params == nil {
		r.Params = map[string]interface{}{}
	}

	if r.CreatedUuids == nil {
		r.CreatedUuids = []string{}
	}

	if r.State == "" {
		r.State = StateCreated
	}

	if r.Type == "" {
		r.Type = ipmiScanResult
	}
}

func (p *Plugin) getIpmiScanResultFromRaw(r *models.RawModel) (*IpmiScanResult, *models.Error) {
	result := &IpmiScanResult{}
	result.Fill()
	if rerr := utils2.Remarshal(r, &result); rerr != nil {
		return nil, utils.ConvertError(400, rerr)
	}
	result.orig = r
	return result, nil
}

func (p *Plugin) getIpmiScanResultFromInterface(i interface{}) (*IpmiScanResult, *models.Error) {
	r := &models.RawModel{"Type": ipmiScanResult}
	if rerr := models.Remarshal(i, &r); rerr != nil {
		return nil, utils.ConvertError(400, rerr)
	}
	return p.getIpmiScanResultFromRaw(r)
}

func (p *Plugin) getIpmiScanResult(id string) (*IpmiScanResult, *models.Error) {
	r := newRawModel()
	if perr := p.session.FillModel(r, id); perr != nil {
		return nil, utils.ConvertError(400, perr)
	}

	return p.getIpmiScanResultFromRaw(r)
}

func (p *Plugin) getIpmiScanResults() ([]*IpmiScanResult, *models.Error) {
	alist, perr := p.session.ListModel(ipmiScanResult)
	if perr != nil {
		return nil, utils.ConvertError(400, perr)
	}
	answer := []*IpmiScanResult{}
	for _, r1 := range alist {
		nr1, nerr := p.getIpmiScanResultFromInterface(r1)
		if nerr != nil {
			return nil, nerr
		}
		answer = append(answer, nr1)
	}
	return answer, nil
}

func (p *Plugin) updateIpmiScanResult(orig, updated *IpmiScanResult) *models.Error {
	updated.Fill()
	r1 := newRawModel()
	if rerr := models.Remarshal(orig, &r1); rerr != nil {
		return utils.ConvertError(400, rerr)
	}

	r2 := newRawModel()
	if rerr := models.Remarshal(updated, &r2); rerr != nil {
		return utils.ConvertError(400, rerr)
	}

	res := models.Clone(r1)
	if err := p.session.Req().PatchTo(r1, r2).Do(&res); err != nil {
		// We could do better error testing
		return utils.ConvertError(400, err)
	}
	return nil
}

func (p *Plugin) createIpmiScanResult(newO *IpmiScanResult) *models.Error {
	emptyO := newRawModel()
	if rerr := utils2.Remarshal(newO, emptyO); rerr != nil {
		return utils.ConvertError(400, rerr)
	}
	return utils.ConvertError(400, p.session.CreateModel(newO))
}

func (p *Plugin) destroyIpmiScanResult(r1 *IpmiScanResult) *models.Error {
	_, e := p.session.DeleteModel(ipmiScanResult, r1.Id)
	return utils.ConvertError(400, e)
}

func (p *Plugin) clearIpmiScanResults() (err *models.Error) {
	err = &models.Error{}
	results, verr := p.getIpmiScanResults()
	if verr != nil {
		return utils.ConvertError(500, verr)
	}

	for _, res := range results {
		e := p.destroyIpmiScanResult(res)
		err.AddError(e)
	}
	return utils.ConvertError(400, err)
}
