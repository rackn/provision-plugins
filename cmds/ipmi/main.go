package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml Version=$version"
//go:generate sh -c "drpcli contents document content.yaml > ipmi.rst"

import (
	_ "embed"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	utils2 "github.com/VictorLowther/jsonpatch2/utils"
	dhcp "github.com/krolaw/dhcp4"
	"github.com/pborman/uuid"
	"github.com/stmcginnis/gofish"
	"gitlab.com/rackn/provision-plugins/v4/utils"

	"gitlab.com/rackn/logger"
	v4 "gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
	"gitlab.com/rackn/ring"
)

var (
	//go:embed content.yaml
	content string
	version = v4.RSVersion
	def     = models.PluginProvider{
		Name:          "ipmi",
		Version:       version,
		PluginVersion: 4,
		AutoStart:     true,
		AvailableActions: []models.AvailableAction{
			{Command: "scan-range",
				Model: "plugins",
				RequiredParams: []string{
					"ipmi/discover/scan-start-address",
					"ipmi/discover/scan-end-address",
				},
			},
			{Command: "poweron",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "poweroff",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "powercycle",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "nextbootcd",
				Model: "machines",
				RequiredParams: []string{
					"detected-bios-mode",
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "nextboothttp",
				Model: "machines",
				RequiredParams: []string{
					"detected-bios-mode",
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "nextbootpxe",
				Model: "machines",
				RequiredParams: []string{
					"detected-bios-mode",
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "nextbootdisk",
				Model: "machines",
				RequiredParams: []string{
					"detected-bios-mode",
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "forceboothttp",
				Model: "machines",
				RequiredParams: []string{
					"detected-bios-mode",
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "forcebootpxe",
				Model: "machines",
				RequiredParams: []string{
					"detected-bios-mode",
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "forcebootdisk",
				Model: "machines",
				RequiredParams: []string{
					"detected-bios-mode",
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "identify",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
					"ipmi/identify-duration",
				},
			},
			{Command: "powerstatus",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "status",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getInfo",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getBios",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getMemory",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getStorage",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getSimpleStorage",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getProcessor",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getNetworkInterfaces",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getEthernetInterfaces",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getSecureBoot",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "getBoot",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "mountVirtualMedia",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
					"ipmi/virtual-media-url",
					"ipmi/virtual-media-boot",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "unmountVirtualMedia",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
			{Command: "statusVirtualMedia",
				Model: "machines",
				RequiredParams: []string{
					"ipmi/username",
					"ipmi/password",
					"ipmi/address",
					"ipmi/mode",
				},
				OptionalParams: []string{
					"ipmi/port-ipmitool",
					"ipmi/port-racadm",
					"ipmi/port-redfish",
					"ipmi/port-lpar",
					"ipmi/lpar-id",
				},
			},
		},
		Content: content,
		StoreObjects: map[string]interface{}{
			"ipmi_scan_results": map[string]interface{}{
				"ScanStartAddress": map[string]interface{}{
					"type": "string",
				},
				"ScanEndAddress": map[string]interface{}{
					"type": "string",
				},
				"LowestScannedIndex": map[string]interface{}{
					"type": "integer",
				},
				"LowestScannedAddress": map[string]interface{}{
					"type": "string",
				},
				"CreatedUuids": map[string]interface{}{
					"type": "array",
					"items": map[string]interface{}{
						"type": "string",
					},
				},
				"ScanErrors": map[string]interface{}{
					"type": "array",
					"items": map[string]interface{}{
						"type": "string",
					},
				},
				"ScanSize": map[string]interface{}{
					"type": "integer",
				},
				"State": map[string]interface{}{
					"type": "string",
				},
				"ScanActionJobId": map[string]interface{}{
					"type": "string",
				},
				"ScanStartTime": map[string]interface{}{
					"type": "string",
				},
				"ScanEndTime": map[string]interface{}{
					"type": "string",
				},
				"ScanUpdatedTime": map[string]interface{}{
					"type": "string",
				},
				"ScanWaitForComplete": map[string]interface{}{
					"type": "boolean",
				},
				"ScanWaitTimeoutMins": map[string]interface{}{
					"type": "integer",
				},
			},
		},
	}
)

const (
	defaultBufferSize              = 20000
	defaultMaxParallel             = 200
	defaultRetryCount              = 3
	retryInterval                  = 1 * time.Minute
	DefaultRangeScanUpdateInterval = 10 * time.Second
	DefaultTimeoutMins             = 5
	DefaultWaitForComplete         = false
)

type driver interface {
	Name() string
	Probe(l logger.Logger, address string, port int, username, password string) *models.Error
	Action(l logger.Logger, ma *models.Action) (supported bool, res interface{}, err *models.Error)
}

type Plugin struct {
	discoverEnabled          bool
	discoverFromLease        bool
	discoverRateLimit        time.Duration
	discoverMaxParallel      int64
	discoverBufferSize       int
	discoverWorkflow         string
	discoverContext          string
	oobmCredentials          []map[string]string
	userProvidedCredentials  []map[string]string
	events                   []string
	discoverDrivers          []driver
	session                  *api.Client
	pitOfPerpetualIgnorance  map[string]struct{}
	oobMux                   *sync.Mutex
	eventsInProgress         map[string]struct{}
	once                     *sync.Once
	probeConsumer            *ring.Consumer[ProbeInput]
	redfishConnectionRetries int
}

type ProbeInput struct {
	logger.Logger
	ipAddress  string          // Address to be probed
	ipIndex    int             // Index of the ip address in the ips list - this is used to look up the address later from the ipMap
	macaddr    string          // The mac address of the machine being probed
	scanResult *IpmiScanResult // Optional ID that is used to track IPMI range scan results
}

func (p *Plugin) ignoredLease(s string) (ok bool) {
	p.oobMux.Lock()
	defer p.oobMux.Unlock()
	_, ok = p.pitOfPerpetualIgnorance[s]
	return
}

func (p *Plugin) ignoreLease(s string) {
	p.oobMux.Lock()
	defer p.oobMux.Unlock()
	p.pitOfPerpetualIgnorance[s] = struct{}{}
}

func (p *Plugin) isEventAlreadyInProgress(s string) (ok bool) {
	p.oobMux.Lock()
	defer p.oobMux.Unlock()
	_, ok = p.eventsInProgress[s]
	return
}

func (p *Plugin) putEventInProgress(s string) {
	p.oobMux.Lock()
	defer p.oobMux.Unlock()
	p.eventsInProgress[s] = struct{}{}
}

func (p *Plugin) clearEvent(s string) {
	p.oobMux.Lock()
	defer p.oobMux.Unlock()
	delete(p.eventsInProgress, s)
}

func (p *Plugin) SelectEvents() []string {
	return p.events
}

func (p *Plugin) processError(errorCode int, errorString string, gofishClient *gofish.APIClient, l logger.Logger) *models.Error {
	// Log error
	l.Errorf(errorString)

	// Close client connection if passed in
	if gofishClient != nil {
		gofishClient.Logout()
	}

	// Build and return error
	return &models.Error{
		Code:     errorCode,
		Model:    "plugin",
		Key:      "ipmi",
		Type:     "rpc",
		Messages: []string{errorString},
	}
}

func (p *Plugin) checkGofishConnection(l logger.Logger, config gofish.ClientConfig) (gofishClient *gofish.APIClient, err error) {
	gofishClient, err = gofish.Connect(config)
	for i := 1; i <= defaultRetryCount && err != nil; i++ {
		l.Infof("Error: %v occurred. Retrying in %v seconds. Retry #%d\n", err, retryInterval.Seconds(), i)
		time.Sleep(retryInterval)

		// Call the function again
		gofishClient, err = gofish.Connect(config)
	}
	return
}

func (p *Plugin) trueUpIp(l logger.Logger, machines []*models.Machine, ipAddress string) *models.Error {
	// Let's make sure that the machine IP matches, if not we will need to update
	if machines[0].Address.String() != ipAddress {
		l.Infof("Machine %s IP address %s will be updated to %s", machines[0].Uuid, machines[0].Address.String(), ipAddress)
		ip := net.ParseIP(ipAddress) // We don't need to validate IP address here because it is already validated in range-scan and the publish functions
		updatedMachine := models.Clone(machines[0]).(*models.Machine)
		updatedMachine.Params["ipmi/address"] = ip.String()
		_, pErr := p.session.PatchTo(machines[0], updatedMachine)
		if pErr != nil {
			return p.processError(
				500,
				fmt.Sprintf("Unable to update machine's IP address. Error %v", pErr),
				nil,
				l,
			)
		}
	}
	return nil
}

func (p *Plugin) getBasicInfo(l logger.Logger, gofishClient *gofish.APIClient) (string, []string, *models.Error) {
	// At this point we have an authenticated gofish client
	// Use this to get the system and ethernet interfaces
	systems, err := gofishClient.Service.Systems()
	if err != nil {
		return "", nil, p.processError(
			500,
			fmt.Sprintf("Unable to get systems information from redfish. Error %v", err),
			gofishClient,
			l,
		)
	}

	if len(systems) == 0 {
		return "", nil, p.processError(
			500,
			fmt.Sprintf("No systems defined in redfish."),
			gofishClient,
			l,
		)
	}

	system := systems[0]
	l.Infof("Redfish System retrieved.")

	// Get the ethernet interfaces
	ethernetInterfaces, err := system.EthernetInterfaces()
	if err != nil {
		return "", nil, p.processError(
			500,
			fmt.Sprintf("Unable to get ethernet interfaces from redfish. Error %v", err),
			gofishClient,
			l,
		)
	}

	macAddrs := []string{}
	for _, ethernetInterface := range ethernetInterfaces {
		s := strings.ToLower(ethernetInterface.MACAddress)
		// Skip the bad macs
		if s == "00:00:00:00:00:00" {
			continue
		}
		if s == "ee:ee:ee:ee:ee:ee" {
			continue
		}
		if s == "b0:3a:f2:b6:05:9f" {
			continue
		}
		macAddrs = append(macAddrs, strings.ToLower(ethernetInterface.MACAddress))
	}
	l.Infof("MACAddresses retrieved")

	if len(macAddrs) == 0 {
		return "", nil, p.processError(
			500,
			"Unable to get mac addresses from redfish",
			gofishClient,
			l,
		)
	}

	return system.SerialNumber, macAddrs, nil
}

func (p *Plugin) createMachine(l logger.Logger, macAddrs []string, config gofish.ClientConfig, ipAddress string, macaddr, csn string) (*models.Machine, *models.Error) {
	// Build a machine object
	machine := &models.Machine{}
	createdMachine := &models.Machine{}
	machine.Fill()
	machine.HardwareAddrs = macAddrs
	machine.Workflow = p.discoverWorkflow
	machine.Context = p.discoverContext
	machine.Name = fmt.Sprintf("d%s", strings.ReplaceAll(macAddrs[0], ":", "-"))
	machine.Params["ipmi/username"] = config.Username
	machine.Params["ipmi/password"] = config.Password
	machine.Params["ipmi/mode"] = "redfish"
	machine.Params["ipmi/address"] = ipAddress
	machine.Params["ipmi/macaddr"] = macaddr
	machine.Params["ipmi/enabled"] = true
	machine.Params["inventory/SerialNumber"] = csn

	// Post to create the machine object
	if err := p.session.Req().Post(machine).UrlFor("machines").Do(createdMachine); err != nil {
		p.clearEvent(macaddr)
		return nil, p.processError(
			500,
			fmt.Sprintf("Error creating machine. Error %v", err),
			nil,
			l,
		)
	}

	return createdMachine, nil
}

func (p *Plugin) probeThisAddress(probe *ProbeInput) (error *models.Error) {
	// Check to see if we already have IPMI controller entries for the mac and/or address from the Lease.
	// If so, ignore this event.
	var machines []*models.Machine
	if err := p.session.Req().Filter("machines", "Params.ipmi/macaddr", "Eq", probe.macaddr).Do(&machines); err == nil {
		if len(machines) > 0 {
			p.ignoreLease(probe.macaddr)
			probe.Infof("Machine %s already created with OOB controller %s", machines[0].Uuid, probe.macaddr)
			error = p.trueUpIp(probe, machines, probe.ipAddress)
			return
		}
	}
	if err := p.session.Req().Filter("machines", "Params.ipmi/address-v4", "Eq", probe.ipAddress).Do(&machines); err == nil {
		if len(machines) > 0 {
			p.ignoreLease(probe.macaddr)
			probe.Infof("Machine %s already created with OOB controller %s", machines[0].Uuid, probe.macaddr)
			return
		}
	}
	if err := p.session.Req().Filter("machines", "Params.ipmi/address", "Eq", probe.ipAddress).Do(&machines); err == nil {
		if len(machines) > 0 {
			p.ignoreLease(probe.macaddr)
			probe.Infof("Machine %s already created with OOB controller %s", machines[0].Uuid, probe.macaddr)
			return
		}
	}

	// First we only want to check whether redfish is available on that address, so we don't need creds
	// Create a new instance of gofish client, ignoring self-signed certs
	gofishConfig := gofish.ClientConfig{
		Endpoint: fmt.Sprintf("https://%s", net.JoinHostPort(probe.ipAddress, "443")),
		Insecure: true,
	}
	gofishClient, err := p.checkGofishConnection(probe, gofishConfig)
	if err != nil {
		p.ignoreLease(probe.macaddr)
		probe.Infof("Not a redfish server, ignoring. redfish API response: %v", err)
		return
	}
	probe.Infof("Machine %s has redfish, proceeding with login", probe.macaddr)
	loginSuccessful := false

	// Try usernames and passwords to login and see if we can get into redfish
	// Try out any macaddress specific credentials provided
	if p.userProvidedCredentials != nil {
		for _, creds := range p.userProvidedCredentials {
			if creds["identifier"] == probe.macaddr || creds["identifier"] == probe.ipAddress {
				gofishConfig.Username = creds["username"]
				gofishConfig.Password = creds["password"]
				if gofishClient, err = gofish.Connect(gofishConfig); err == nil {
					loginSuccessful = true
					break
				}
			}
		}
	}
	if !loginSuccessful && p.oobmCredentials != nil { // Try default credentials to see if we can login with one
		probe.Infof("Login with user provided credentials was unsuccessful.. trying default creds")
		for _, creds := range p.oobmCredentials {
			gofishConfig.Username = creds["username"]
			gofishConfig.Password = creds["password"]
			if gofishClient, err = gofish.Connect(gofishConfig); err == nil {
				loginSuccessful = true
				break
			}
		}
	}

	// If we weren't able to login, then error
	if !loginSuccessful {
		error = p.processError(
			403,
			fmt.Sprintf("Unable to login to redfish client. Error %v", err),
			gofishClient,
			probe,
		)
		return
	}

	csn, macAddrs, e := p.getBasicInfo(probe, gofishClient)
	if e != nil {
		error = e
		return
	}

	createdMachine, e := p.createMachine(probe, macAddrs, gofishConfig, probe.ipAddress, probe.macaddr, csn)
	if err != nil {
		error = e
		return
	}

	// This is the created machine
	if createdMachine != nil {
		probe.Debugf("Machine created with UUID: %s\n", createdMachine.UUID())
		probe.scanResult.CreatedUuids = append(probe.scanResult.CreatedUuids, createdMachine.UUID())
	} else {
		error = p.processError(
			500,
			fmt.Sprintf("Machine object for %s was not created \n", probe.ipAddress),
			nil,
			probe,
		)
	}

	probe.Debugf("Completed probing address %s\n", probe.ipAddress)
	return
}

func (p *Plugin) scanRange(l logger.Logger, startAddress, endAddress, scanActionJobId, batchId string, waitForScanComplete bool, timeoutInMins time.Duration) (*IpmiScanResult, *models.Error) {
	// Validate IP range and get range size and startIP int - this is needed later on to calculate IPs as they are processed
	startIPInt, _, rangeSize, err := parseAndValidateIPRange(startAddress, endAddress)
	if err != nil {
		return nil, p.processError(
			400,
			fmt.Sprintf("Failed to validate IP range from %s to %s: %v", startAddress, endAddress, err),
			nil,
			l,
		)
	}

	// Make sure that a scan result entry doesn't already exist
	// scanActionJobId is the ID for the scan
	scanResult, serr := p.getIpmiScanResult(scanActionJobId)
	if serr != nil || scanResult == nil {
		scanResult = &IpmiScanResult{}
		scanResult.Fill()
		scanResult.ScanStartAddress = startAddress
		scanResult.LowestScannedIndex = -1 // This indicates that no ips have been scanned yet
		scanResult.ScanSize = rangeSize
		scanResult.ScanEndAddress = endAddress
		scanResult.ScanActionJobId = scanActionJobId
		scanResult.State = StateRunning
		scanResult.Id = scanActionJobId
		scanResult.ScanStartTime = time.Now()
		scanResult.ScanWaitForComplete = waitForScanComplete
		scanResult.ScanWaitTimeoutMins = timeoutInMins
		if batchId != "" {
			scanResult.Meta["workorder-owner-type"] = "batches"
			scanResult.Meta["workorder-action"] = "work"
			scanResult.Meta["workorder-owner"] = batchId
		}
		if cErr := p.createIpmiScanResult(scanResult); cErr != nil {
			return nil, p.processError(
				500,
				fmt.Sprintf("Error generating/saving IPMI scan result tracker. Error %v", cErr),
				nil,
				l,
			)
		}
	}

	scanResult.ipScanState = createInitialScanStateSlice(rangeSize, scanResult.LowestScannedIndex)
	scanResult.scanUpdateInterval = DefaultRangeScanUpdateInterval
	scanResult.ScanUpdatedTime = time.Now()

	// Use the range size to automatically create IPs upto the end IP on the go
	for i := 0; i < rangeSize; i++ {
		if !scanResult.ipScanState[i] {
			ip := intToIP(startIPInt + uint32(i)).String()
			p.pushToQueue(&ProbeInput{Logger: l, ipAddress: ip, ipIndex: i, macaddr: ip, scanResult: scanResult})
		}
	}

	if waitForScanComplete {
		timeout := time.NewTicker(timeoutInMins * time.Minute)
		polling := time.NewTicker(10 * time.Second)
		defer timeout.Stop()
		defer polling.Stop()

		for {
			select {
			case <-timeout.C:
				res, err := p.getIpmiScanResult(scanResult.Id)
				if err != nil {
					return nil, err
				}
				return res, p.processError(
					500,
					fmt.Sprintf("Timeout of %v reached waiting for range scan to complete. Navigate to the ux://ipmi_scan_results  page for more information.", timeout),
					nil,
					l,
				)
			case <-polling.C:
				// Get scan result from DB
				res, err := p.getIpmiScanResult(scanResult.Id)
				if err != nil {
					return nil, err
				}
				if res.State == StateFinished {
					return res, nil
				}
			}
		}
	}
	return scanResult, nil
}

func (p *Plugin) pushToQueue(input *ProbeInput) {
	if i := p.probeConsumer.Push(*input); i != 1 {
		log.Panic("Unable to push events into the queue!!")
	}
}

func (p *Plugin) Publish(l logger.Logger, e *models.Event) *models.Error {
	l.Debugf("Event: type: %s action: %s key: %s has been published", e.Type, e.Action, e.Key)
	res := &models.Error{Model: e.Type, Key: e.Key, Type: e.Action}
	if e.Type != "leases" || e.Action != "ack" {
		res.Errorf("Invalid event for %s.%s.%s: not a leases.ack.*", e.Type, e.Action, e.Key)
		return res
	}
	mod, err := e.Model()
	if err != nil {
		res.Errorf("Invalid saved object in %s: %v", e, err)
		return res
	}
	lease, ok := mod.(*models.Lease)
	if !ok {
		res.Errorf("Event did not contain a lease")
		return res
	}

	if p.isEventAlreadyInProgress(lease.Token) {
		res.Errorf("Event for machine %s is already being processed", lease.Token)
		return res
	}
	// Event is not in progress, so put it in progress
	p.putEventInProgress(lease.Token)

	if p.ignoredLease(lease.Token) {
		l.Infof("Ignored Lease for known machine %s, ignoring", lease.MachineUuid)
		return nil
	}
	if lease.MachineUuid != nil && !uuid.Equal(lease.MachineUuid, uuid.NIL) {
		p.ignoreLease(lease.Token)
		l.Infof("Lease for known machine %s, ignoring", lease.MachineUuid)
		return nil
	}
	hasPxe := false
	for _, opt := range lease.ProvidedOptions {
		if dhcp.OptionCode(opt.Code) == dhcp.OptionVendorClassIdentifier {
			hasPxe = strings.HasPrefix("PXEClient", opt.Value) || strings.HasPrefix("HTTPClient", opt.Value)
		} else if dhcp.OptionCode(opt.Code) == dhcp.OptionParameterRequestList {
			dec, _ := models.DHCPOptionParser(dhcp.OptionParameterRequestList)
			if bl, err := dec(opt.Value); err == nil {
				for _, code := range bl {
					if dhcp.OptionCode(code) == dhcp.OptionBootFileName {
						hasPxe = true
						break
					}
				}
			}
		}
		if hasPxe {
			break
		}
	}
	if hasPxe {
		p.ignoreLease(lease.Token)
		l.Infof("Lease %s is requesting PXE boot, cannot be a BMC", lease.Addr)
		return nil
	}

	// Whelp, now we get to try and probe the thingy at address to see if it is in fact an IPMI controller we know how
	// to interrogate.
	// Build and push the probe input to the probe consumer
	p.pushToQueue(&ProbeInput{Logger: l, ipAddress: lease.Addr.String(), macaddr: lease.Token})
	return nil
}

func (p *Plugin) saveRangeScanResult(pi *ProbeInput) {
	// get the current object
	currentObj, err := p.getIpmiScanResult(pi.scanResult.Id)
	if err != nil || currentObj == nil {
		pi.Errorf("ubable to get current scan object from the database for %s\n", pi.scanResult.Id)
		return
	}
	uuids := removeEmptyStrings(pi.scanResult.CreatedUuids)
	result := currentObj.Clone()
	result.CreatedUuids = append(result.CreatedUuids, uuids...)
	result.ScanErrors = append(result.ScanErrors, pi.scanResult.ScanErrors...)
	result.LowestScannedIndex = pi.scanResult.LowestScannedIndex
	result.LowestScannedAddress = incrementIPAddress(result.ScanStartAddress, result.LowestScannedIndex)

	result.ScanUpdatedTime = time.Now()

	if pi.scanResult.LowestScannedIndex == (len(pi.scanResult.ipScanState) - 1) {
		result.State = StateFinished
		result.ScanEndTime = time.Now()
	}

	if uErr := p.updateIpmiScanResult(currentObj, result); uErr != nil {
		pi.Errorf("There was an error updating the database for %s due to %v\n", currentObj.Id, uErr)
	}
	pi.scanResult.CreatedUuids = []string{}
	pi.scanResult.ScanErrors = []string{}
	pi.scanResult.ScanUpdatedTime = result.ScanUpdatedTime
}

func (p *Plugin) processRangeScanProbe(pi *ProbeInput) {
	pi.scanResult.mux.Lock()
	defer pi.scanResult.mux.Unlock()

	// update the tracking scan result
	pi.scanResult.ipScanState[pi.ipIndex] = true

	if ipToInt(net.ParseIP(pi.ipAddress).To4())+1 == uint32(pi.ipIndex) {
		pi.scanResult.LowestScannedIndex = pi.ipIndex
	} else {
		pi.scanResult.LowestScannedIndex = findLowestScannedIndex(pi.scanResult)
	}

	if pi.scanResult.LowestScannedIndex == (len(pi.scanResult.ipScanState)-1) || (time.Since(pi.scanResult.ScanUpdatedTime) >= pi.scanResult.scanUpdateInterval) {
		p.saveRangeScanResult(pi)
	}
}

func (p *Plugin) SetupConsumer() {
	// Build the probe function and set up consumer
	probeFunction := func(probeInput ProbeInput) {
		perr := p.probeThisAddress(&probeInput)
		if perr != nil {
			probeInput.Errorf("error processing event %s: %v", probeInput.ipAddress, perr)
		}
		if probeInput.scanResult != nil && probeInput.scanResult.Id != "" {
			if perr != nil {
				errorMessage := fmt.Sprintf("there was an error processing the scan for ip: %s due to %v", probeInput.ipAddress, perr)
				probeInput.scanResult.ScanErrors = append(probeInput.scanResult.ScanErrors, errorMessage)
			}
			p.processRangeScanProbe(&probeInput)
			probeInput.Infof("%s: finished scanning ip %s\n", probeInput.scanResult.Id, probeInput.ipAddress)
		}

		// Now we add the lease to the pit because it has already been processed
		p.clearEvent(probeInput.macaddr)
		p.ignoreLease(probeInput.macaddr)
	}
	p.probeConsumer = ring.NewConsumer[ProbeInput](p.discoverBufferSize, p.discoverMaxParallel, probeFunction)
}

func (p *Plugin) Config(l logger.Logger, session *api.Client, config map[string]interface{}) (err *models.Error) {
	p.oobMux = &sync.Mutex{}
	p.pitOfPerpetualIgnorance = map[string]struct{}{}
	p.eventsInProgress = map[string]struct{}{}
	if info, infoErr := session.Info(); infoErr == nil {
		found := false
		for _, feature := range info.Features {
			if feature == "secure-param-upgrade" {
				found = true
			}
		}
		if found == false {
			return &models.Error{
				Code:     500,
				Model:    "plugin",
				Key:      "ipmi",
				Type:     "rpc",
				Messages: []string{"dr-provision missing secure-param-upgrade"},
			}
		}
	} else {
		return infoErr.(*models.Error)
	}
	p.session = session
	if p.discoverEnabled, _ = config["ipmi/discover/enabled"].(bool); p.discoverEnabled {
		l.Debugf("Event discovery has been enabled")
		if f, ok := config["ipmi/discover/parallel"].(float64); ok {
			p.discoverMaxParallel = int64(f + 0.5)
		} else {
			p.discoverMaxParallel = defaultMaxParallel
		}
		if f, ok := config["ipmi/discover/buffer-size"].(float64); ok {
			p.discoverBufferSize = int(f + 0.5)
		} else {
			p.discoverBufferSize = defaultBufferSize
		}

		if f, ok := config["ipmi/discover/redfish-retry-count"].(float64); ok {
			p.redfishConnectionRetries = int(f + 0.5)
		} else {
			p.redfishConnectionRetries = defaultRetryCount
		}

		if r, ok := config["ipmi/discover/rate-limit"].(string); ok {
			dur, durErr := models.ParseDuration(r, "h")
			if durErr != nil || dur < 2*time.Hour {
				dur = 2 * time.Hour
			}
			p.discoverRateLimit = dur
		}

		if w, ok := config["ipmi/discover/workflow"].(string); ok {
			p.discoverWorkflow = w
		}
		if w, ok := config["ipmi/discover/context"].(string); ok {
			p.discoverContext = w
		}

		if p.discoverFromLease, _ = config["ipmi/discover/from-lease"].(bool); p.discoverFromLease {
			l.Debugf("Discover from lease is enabled..")
			p.events = []string{"leases.ack.*"}
		}
		p.discoverDrivers = []driver{&redfish{}}

		var oobmCredentials []map[string]string
		jerr := utils2.Remarshal(config["ipmi/discover/oobm-default-credentials"], &oobmCredentials)
		if jerr == nil {
			p.oobmCredentials = oobmCredentials
		}
		var macaddrCredentials []map[string]string
		jerr = utils2.Remarshal(config["ipmi/discover/oobm-user-provided-credentials"], &macaddrCredentials)
		if jerr == nil {
			p.userProvidedCredentials = macaddrCredentials
		}

		// Set up consumer - this is needed for the range scan action which is why doing it outside the
		// discover enabled condition
		p.once.Do(func() {
			p.SetupConsumer()

			// Check to see if there are any scan results lying around in progress
			scanResults, sErr := p.getIpmiScanResults()
			if sErr != nil {
				l.Errorf("There was an error get all scan results from the database due to %v", err)
				return
			}

			for _, result := range scanResults {
				batchId, _ := result.Meta["workorder_owner"]
				if result.State == StateRunning {
					// Get the job
					if job, sErr := utils.GetJobById(session, result.ScanActionJobId); sErr == nil {
						// Make the job logger
						l2, _ := plugin.MakeJobLogger(job, l)
						_, sErr := p.scanRange(
							l2,
							result.ScanStartAddress,
							result.ScanEndAddress,
							result.ScanActionJobId,
							batchId,
							result.ScanWaitForComplete,
							result.ScanWaitTimeoutMins)
						if sErr != nil {
							l.Errorf("There was an error scanning range %s - %s with job id: %s and lowest index : %v due to %v", result.ScanStartAddress, result.ScanEndAddress, result.ScanActionJobId, result.LowestScannedIndex, err)
						}
					}
				}
			}
		})
	}

	return
}

func isScanRangeAction(command string) bool {
	return command == "scan-range"
}

func (p *Plugin) Action(l logger.Logger, ma *models.Action) (answer interface{}, err *models.Error) {
	var ipmiDriver driver
	port := 0

	// Handle the range scan action, which at this time does not care about the ipmi driver
	if isScanRangeAction(ma.Command) {
		waitForFinish := utils.GetParamOrBoolean(ma.Params, "ipmi/discover/scan-wait-for-complete", DefaultWaitForComplete)
		timeout := utils.GetParamOrInt(ma.Params, "ipmi/discover/scan-timeout-minutes", DefaultTimeoutMins)
		batchId := utils.GetParamOrString(ma.Params, "ipmi/discover/batch-id", "")

		if !p.discoverEnabled {
			err = &models.Error{
				Code:     400,
				Model:    "plugin",
				Key:      "ipmi",
				Type:     "rpc",
				Messages: []string{"Enable IPMI discovery by setting `ipmi/discover/enabled` to `true` on the IPMI plugin before proceeding."},
			}
			return
		}

		answer, err = p.scanRange(
			l,
			ma.Params["ipmi/discover/scan-start-address"].(string),
			ma.Params["ipmi/discover/scan-end-address"].(string),
			ma.Job.String(),
			batchId,
			waitForFinish,
			time.Duration(timeout))
		return
	}

	switch ma.Params["ipmi/mode"].(string) {
	case "ipmitool":
		ipmiDriver = &ipmi{}
		port = int(ma.Params["ipmi/port-ipmitool"].(float64) + 0.5)
	case "racadmn", "racadm":
		ipmiDriver = &racadm{}
		port = int(ma.Params["ipmi/port-racadm"].(float64) + 0.5)
	case "redfish":
		ipmiDriver = &redfish{}
		port = int(ma.Params["ipmi/port-redfish"].(float64) + 0.5)
	case "lpar":
		ipmiDriver = &lpar{}
		port = int(ma.Params["ipmi/port-lpar"].(float64) + 0.5)
	default:
		err = &models.Error{Code: 404,
			Model:    "plugin",
			Key:      "ipmi",
			Type:     "rpc",
			Messages: []string{fmt.Sprintf("Invalid mode: %v", ma.Params["ipmi/mode"])},
		}
		return
	}
	if perr := ipmiDriver.Probe(l,
		ma.Params["ipmi/address"].(string),
		port,
		ma.Params["ipmi/username"].(string),
		ma.Params["ipmi/password"].(string)); perr != nil {
		err = &models.Error{
			Code:     perr.Code,
			Model:    "plugin",
			Key:      "ipmi",
			Type:     "rpc",
			Messages: []string{fmt.Sprintf("Unavailable ipmi driver: %s", ipmiDriver.Name())},
		}
		err.AddError(perr)
		return
	}
	supported := false
	supported, answer, err = ipmiDriver.Action(l, ma)
	if !supported {
		err = &models.Error{
			Code:     404,
			Model:    "plugin",
			Key:      "ipmi",
			Type:     "rpc",
			Messages: []string{fmt.Sprintf("Action %s not supported on ipmi driver: %s", ma.Command, ipmiDriver.Name())},
		}
	}
	return
}

func main() {
	p := &Plugin{
		once: &sync.Once{},
	}
	plugin.InitApp("ipmi", "Provides out-of-band IPMI controls", version, &def, p)
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
