package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml Version=$version"
//go:generate sh -c "drpcli contents document content.yaml > blancco-lun-eraser.rst"
//go:generate go run ../../tools/package-assets/package-assets.go embedded.zip embedded

import (
	_ "embed"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/models"
	"os"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/plugin"
)

var (
	//go:embed content.yaml
	content string
	//go:embed embedded.zip
	embedded []byte
	version  = v4.RSVersion
	def      = models.PluginProvider{
		Name:          "blancco-lun-eraser",
		Version:       version,
		PluginVersion: 4,
		AutoStart:     true,
		HasPublish:    false,
		Content:       content,
	}
)

type Plugin struct {
}

func (p *Plugin) Config(l logger.Logger, session *api.Client, data map[string]interface{}) *models.Error {
	return nil
}

func (p *Plugin) Unpack(thelog logger.Logger, path string) error {
	return utils.Unpack(embedded, path)
}

func main() {
	plugin.InitApp("blancco-lun-eraser", "Add Blancco LUN Eraser capabilities.", version, &def, &Plugin{})
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
