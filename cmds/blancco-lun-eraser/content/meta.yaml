---
Name: blancco-lun-eraser
Author: RackN
CodeSource: https://gitlab.com/rackn/provision-plugins
Color: orange
Copyright: RackN
Description: Plugin for using the external Blancco LUN Eraser (BLE) commercial disk wiping tool.
DisplayName: Blancco LUN Eraser
DocUrl: https://docs.rackn.io/stable/redirect/?ref=rs_content_blancco-lun-eraser
Icon: window close
License: RackN
Tags: rackn,blancco,decommission,enterprise
Prerequisites: drp-community-content,task-library,universal:>=4.12.1
Documentation: |-
  !!! warning
      This Plugin Provider implements non-recoverable disk wipe/erasure capabilities.
      Running the workflows/workorders defined in this Plugin will completely and
      irrevocably destroy all data as defined by the `ble/wipe-disks` Param.  There
      is absolutely no way of recovering data from drives wiped with this tooling.

  This plugin implements the commercially licensed industrial grade Disk
  and LUN wiping tool from [Blancco Technology Group](https://blancco.com).
  The tool requires additional licensing direct from Blancco.  The integrated
  product is [Blancco LUN Eraser](https://www.blancco.com/products/lun-eraser/) (BLE).

  !!! note
      RackN licenses do not entitle use of the Blancco LUN Eraser product. You must
      obtain use licenses directly from Blancco Technologies Group.

  The product originates loosely from the open source Darik's Boot and Nuke (DBAN)
  disk wiping solution.

  A short video on setting up a DRP Endpoint specifically to use the Blancco LUN
  Eraser, and then wiping systems with this integration can be found on Youtube.

  [Youtube video](https://www.youtube.com/watch?v=Aq_NT-t6y-E):

  ## Overview of Usage

  This plugin requires external third party licenses to be purchased from Blancco
  to operate correctly.  Product information and purchasing details can be found
  on Blancco's website at:

    * <https://www.blancco.com/products/lun-eraser/>

  By default this plugin utilizes the Blancco Cloud services to validate and consume
  licenses, and upload completed Reports.  The operator must have a valid Blancco
  Cloud account username and password with license entitlements.  The Blancco Management
  Console can be found at:

    * <https://cloud.blancco.com/>

  It is possible to install a local (eg for "_airgap_" requirements) Blancco License
  and Console management service internally.  Contact Blancco for further details.


  ### Basic Usage

  By default this content will not wipe any disks without a number of Params being
  set correctly on a target Machine.  An example Profile can be found under Profiles,
  which provides a sample of Params that should be set.  The example profile is named:

    * `EXAMPLE-blancco-lun-eraser`

  Specifically, the following required Params must be set with values:

    * `ble/username` - The Blancco Cloud or local Console server user account
    * `ble/password` - The Blancco Cloud or local Console server user password
    * `ble/disks` - an array of disk targets to wipe, each disk consumes 1 (one) Blancco license entitlement
    * `ble/wipe-disks` - set to `true` to enable disk wiping

  Optional configuration Params can be set; if no value is specified, the default value
  will be used:

    * `ble/noop-test-mode` - test the automation without running the wipe processes, for usage validation and disks selection verification
    * `ble/custom-report-fields` - adds custom fields to the completed wipe report, can use DRP based Golang template expansion in these, see the example Profile
    * `ble/license-server` - defaults to `https://cloud.blancco.com:443`
    * `ble/wipe-report-profile-tag` - if specified, collect DRP based combined report information in to a single Profile (see Profile Documentation for more details)
    * `ble/wipe-level` - defaults to `16: NIST 800-88 Clear`
    * `ble/wipe-report-profile-tag` - no default - if set, enables aggregate Machine wipe reports written to a single Profile
    * `ble/wipe-speedlimit` - defines a maximum I/O throughput in MB for wiping processes, see the BLE PDF documentation in `files/plugin_providers/blancco-lun-eraser` for more details
    * `ble/wipe-buffersize` - I/O buffer size to use for wipping, can dramatically increase/decrease overall wipe process time, see the BLE PDF documentation in `files/plugin_providers/blancco-lun-eraser` for more details
    * `ble/disks-target-param` - an alternate Param to use for the list of disks to wipe (for example `inventory/Disks`)
    * `ble/disks-suppression-list` - a String List of disks to NOT wipe even if they have been explicitly specified to be wiped
    * `ble/url-BlanccoLUNEraser-tool` - altnernate download location for the BLE binary
    * `ble/url-license-dat` - altnernate download location for the `license.dat`
    * `ble/url-shasums` - altnernate download location for the `ble-sha256sums.txt` file, empty value turns off SHA hash checking

  The Blancco LUN Eraser tooling supports several standards for different wipe
  level requirements.  See the `ble/wipe-level` Param documentation for specific
  standards supported wipe levels and setting values.

  Note that the list of Optional/configuration params will grow and expand over
  time.  Please see the individual Params defined in this Content for complete
  configuration capabilities and details.


  #### Disk Selection, Suppression, and ReadOnly Handling

  Normally the `ble/disks` Param is the input definition for which target devices
  should be wiped.  The operator may override this to use any other Param that is
   a List of Strings as an alternate input list.  For example, by setting:

    * `ble/disks-target-param: inventory/Disks`

  The system will use the disk devices identified in that alternate Param.  In
  this example, this allows the standard _Inventory_ collection processes in DRP
  to define the disk wipe target selection based on Inventory found devices.

  In addition to the input Param definition of what disk devices to wipe, the operator
  can additionally choose to specify a _Suppression_ (or "_blacklist_") of devices
  to NOT wipe.  The primary intention of the suppression list to prevent erasure
  processes on non-writable disks (eg cdroms).  However, the list can be used to
  ensure preservation of a specific devices from being wipe if desired.

  By default the disk wipe suppression list contains `/dev/sdr0`, `/dev/cdrom`,
  and `/dev/dvd`.

  In addition, any block device that are marked Read Only by the Linux Kernel
  will automatically be removed from the target wipe list.  Note that not all
  actual Read Only devices are marked by the kernal as such.  The system uses the
  `lsblk` device information output (which reads the kernel structures) to determine
  which disks are Read Only.

  Finally; the Task will remove any defined devices that do not have a block
  device entry in `/dev` correlating to the requested device.


  #### Testing Without Wiping/Consuming Blancco Licenses

  It is generally highly encouraged to test this automation prior to running
  any actual wipe/erasure operations on live disks.  Each disk erasure or
  wipe process requires license entitlement and consumption from the Blancco
  Cloud management console or on-premises _airgap_ Blancco Management Console.

  To support test validations without actually wiping disks or consuming
  licenses, set the following Param:

    * `ble/noop-test-mode: true`

  !!! info
      The `ble/wipe-disks` Param is a safety control setting.  If set to
      `false` (the default value), then the erasure Tasks will completely
      exit without running any subsequent code.  This will NOT test the
      tasks appropriately.

  Most of all of the automation tooling will run, with the following exceptions:

    * The `BlanccoLUNEraser` binary tool will not be executed
    * No reports will be generated and the report aggregation Profile writing will not be tested


  #### Defining What to Erase

  The BlanccoLUNEraser tool operates on LUNs (Logical Unit Numbers).  LUNs may
  represent a group of drives configured as a single volume, a partition on a
  drive, or the entire drive itself.

  The operator must provide an explicit list of LUNs that should be destroyed
  and wiped by setting the string list type Param to the names of the LUNs as
  will be found by the Sledgehammer operating system.

  Typically, an operator can determine the LUN names by use of the `lsblk`
  command while in the Sledgehammer environment (this may also be run on other
  Linux variants, however device names are not guaranteed to match).  Network
  based LUNs can potentially be mounted in Sledgehammer to be presented for
  wiping operations.  These operations may be run via the Flexiflow task injection
  pattern of the `universal-discover` workflow.

  At this time (as of v4.13.0) the system will not automatically wipe all
  drives on a system.  It is possible that a new Task could be constructed
  to automatically determine a systems attached drives and build up the
  Param to support full system automatic wipe operations.

  See the `ble/wipe-disks` Params Documentation field for additional details.


  #### License Availability Check

  A limited attempt to determine if the defined Blancco Management Console (BMC)
  (as defined by `ble/license-server`) has valid licenses for consumption.  If
  no licenses are available, the process will exit with an error.  This is
  also true when `ble/noop-test-mode` is enabled.

  The license availability check does not (as of v4.13) attempt to validate
  licenses based on License Expire Date.

  The tooling uses a fairly basic `curl` call to the BMC to determine license
  availability.  It is possible this check may fail in a way that obscures
  the failure reason.  Typically this can be re-run with `rs-debug-enable`
  set to `true`, and reviewing the debug job log output for more clues.


  ### Workflows

  The Blancco LUN Eraser (BLE) tasks must be run in Sledgehammer.  The following
  Workflow enforces that the machine is in Sledgehammer, then runs the Blancco
  LUN Eraser tooling Tasks:

    * `blancco-lun-eraser`


  ### Workorders and Blueprints

  The Blancco LUN Eraser (BLE) tasks must be run in Sledgehammer.  The following
  Blueprint enforces that the machine is in Sledgehammer, then runs the Blancco
  LUN Eraser tooling Tasks:

    * `blancco-lun-eraser`


  ### 'universal-decommission' Integration

  The Universal Decommission process (both an Infrastructure Pipeline and a
  Workflow) can be flexibily extended to replace the standard drive erase
  processes with the Blancco LUN Eraser tool.  To do so, set the Flexiflow
  Param that modifies the `universal-decommission` Workflow stage named
  `universal-decommission-during-flexiflow` to inject the BlanccoLUNEraser
  Tasks.

  Doing this will replace the current Task list that typically runs in the
  Stage by default.  Other desired Tasks can be added along with this.

  Example usage by setting the following Param:

    * `universal-decommission-during-flexiflow: [ "blancco-lun-eraser" ]`

  See the example Profile named `EXAMPLE-BlanccoLUNEraser-universal-decommission`
  for further details.


  ### Reporting for Disk Wipes

  When utilizing the Blancco Cloud Management Console, completed wipe reports
  may be uploaded to the Console for review and certification proof of disk wipe
  operations.  From time-to-time, the reports may fail to upload.  In the case of
  a report upload failure, Digital Rebar Platform (DRP) will not fail the Task.
  The Job Log for the task will record the failure as a "500" error in the log.

  Additionally, DRP records wipe results in the Blancco native Reporting format (XML)
  as Params on the Machine, and *optionally*, in a Profile with an aggregate report
  grouped together by an operator specified `tag` value.  This allows multiple
  Machines to execute wipe operations, and aggregate the wipe reports together
  in a single Profile.

  A completed Wipe Report will be added to each Machine object of each Machine
  that completes a successful wipe.  The wipe report will be recorded in the
  Param:

    * `ble/wipe-report`

  The Optional Profile based aggregate reports must be explicitly enabled, if desired.
  To enable the Profile based reports, you must set the following Param with a
  "*tag*" value to identify this group of wipe operations.  The tag is an arbitrary
  string, but should not contain any special characters.  An example value setting:

    * `ble/wipe-report-profile-tag`: "20240317-green"

  In the above example a Profile will be created and the "*tag*" appended, creating
  a report Profile named:

    * `ble-wipe-report-20240317-green`

  This Profile will be added to each machine.  Each machines specific wipe report
  will be written as a separate Param in the Profile, with the Machines *UUID* value
  as the Param name, prefixed with `machine-`.

  !!! note
      Reports are in Blannco's native reporting file format, which is XML.

  To remove the report, you must remove the Profile from the Machine(s), then remove
  the Profile.

  !!! warning
      Subsequent re-runs of the `blancco-lun-eraser` Workflow, with the
      **same** Profile *tag* will cause older wipe records for a given Machine
      (if they exist), to be deleted prior to the new Wipe Report being recorded.


  ### Virtual Machines

  If you are using Virtual Machines and wiping disk volumes, note that any of the erase
  levels implemented by Blancco LUN Eraser will cause the disk to inflate to its
  fully specified size.  This is an important consideration if you are backing your
  disk volumes as Sparse type disks.  You must ensure that your backing storage for
  your Virtual Machine volumes is large enough for the disks to be fully inflated.


  ## Customizing the BlanccoLUNEraser Tool Operation

  The `BlanccoLUNEraser` (BLE) tool requires a separate configuration file which defines
  the operational capabilities and enablement of the tools features. In addition,
  the DRP implementation allows the operator to override the command line flags
  and arguments for calling the tool.  See the below sections for further details.


  ### Configuring the BlanccoLUNEraser 'config.txt'

  The `BlanccoLUNEraser` tool utilizes a file with the name of `config.txt` which
  carries the configuraiton for the tool. By default, DRP will render the config
  file as part of the operation of the tool, utilizing the Template named
  `blancco-lun-eraser-config.txt.tmpl`.  This configuration file utilizes several
  of the Params to create a customized configuration for each operational run of
  the BLE tool.

  The operator can construct and specify an alternative configuration file to be
  utilized by the BLE tool.  To do so, set the DRP configuration Param to the
  name of an alternate Template with the desired customizations.  An example
  is as follows:

    * `ble/config-template: my-blanccoluneraser-config.txt.tmpl`

  This file is not validated for correctness by DRP.  Any customizations to it
  must be validated against the BLE documentation.


  ### BlanccoLUNEraser Binary Runtime Arguments

  The BlanccoLUNEraser tool operates with a congirutaion file (defined above),
  in conjunction with a set of shell arguments.  From time to time, Blancco
  may choose to update or change the arguments that are used.  Subsequently,
  this would necessitate the update of the Plugin contents to adjust the
  arguments appropriately.

  To ensure rapid updating of how the tooling is executed, it is possible to
  specify alternative command line arguments to the binary tool (`BlanccoLUNEraser`).
  Note that ONLY the argument flags can be specified; and the Binary command
  name may not (and SHOULD NOT) be specified in the Param.

  The Param is defined as `ble/wipe-command-arguments`.  See the Params
  Documentation field for additional customization information.


  ### BlanccoLUNEraser Tool Documentation

  For convenience the BlanccoLUNEraser tool documentation PDF is embedded in the
  DRP Plugin Provider.  It will be installed and is available for review on the
  DRP Endpoint in the HTTP file server space.  It can be found at:

    * http://<DRP_ENDPOINT>:8091/files/plugin_providers/blancco-lun-eraser/
    * or https://<DRP_ENDPOINT>:8090/files/plugin_providers/blancco-lun-eraser/

  Note that RackN does not guarantee that the documentation in the PDF exactly
  matches the binary operation or usage.


  ### Binary File Download Integrity Validation

  If the Param `ble/url-shasums` contains a file with sha256sums for the downloaded
  assets (namely `BlanccoLUNEraser` and `license.dat`), then the files will be
  validated after download.

  If the Param is empty, the SHA hash checking will be skipped.
