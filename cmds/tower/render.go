package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/url"
	"path"
	"strings"
	"text/template"

	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	yaml "gopkg.in/yaml.v2"
)

type rMachine struct {
	*models.Machine
	renderData *RenderData
	currMac    string
}

// RenderData is the struct that is passed to templates as a source of
// parameters and useful methods.
type RenderData struct {
	Machine   *rMachine // The Machine that the template is being rendered for.
	info      *models.Info
	drpClient *api.Client
	l         logger.Logger
}

func Render(l logger.Logger, drpClient *api.Client, data string, machine *models.Machine, info *models.Info) (string, error) {
	tmpl, err := template.New("tmp").Funcs(models.DrpSafeFuncMap()).Parse(data)
	if err != nil {
		return "", err
	}

	currMac := ""
	if len(machine.HardwareAddrs) > 0 {
		currMac = machine.HardwareAddrs[0]
	}

	// GREG: fix info.Address to non-loopback

	rd := &RenderData{
		l:         l,
		Machine:   &rMachine{Machine: machine, currMac: currMac},
		info:      info,
		drpClient: drpClient,
	}
	rd.Machine.renderData = rd

	buf := &bytes.Buffer{}
	if err := tmpl.Execute(buf, rd); err != nil {
		return "", err
	}
	return string(buf.Bytes()), nil
}

// HexAddress returns Address in raw hexadecimal format, suitable for
// pxelinux and elilo usage.
func (n *rMachine) HexAddress() string {
	return models.Hexaddr(n.Address)
}

// ShortName returns the Hostanme part of an FQDN or the Name if not dot is present
func (n *rMachine) ShortName() string {
	idx := strings.Index(n.Name, ".")
	if idx == -1 {
		return n.Name
	}
	return n.Name[:idx]
}

// Path return the file space path for the machine
// e.g. machines/133-355-3-36-36 <vaild UUID>
func (n *rMachine) Path() string {
	return path.Join(n.Prefix(), n.UUID())
}

// HasProfile returns true if the machine has the specified profile.
func (n *rMachine) HasProfile(name string) bool {
	for _, e := range n.Profiles {
		if e == name {
			return true
		}
	}
	return false
}

func (n *rMachine) Url() string {
	return fmt.Sprintf("http://%s:%d/%s", n.renderData.info.Address, n.renderData.info.FilePort, n.Path())
}

func (n *rMachine) MacAddr(params ...string) string {
	format := "raw"
	if len(params) > 0 {
		format = params[0]
	}
	switch format {
	case "pxelinux":
		return "01-" + strings.Replace(n.currMac, ":", "-", -1)
	case "rpi4":
		return strings.Replace(n.currMac, ":", "-", -1)
	default:
		return n.currMac
	}
}

// ProvisionerAddress returns the IP address to access
// the Provisioner based upon the requesting IP address.
func (r *RenderData) ProvisionerAddress() string {
	return r.info.Address.String()
}

// ProvisionerURL returns a URL to access the
// file server part of the server using the
// requesting IP address as a basis.
func (r *RenderData) ProvisionerURL() string {
	return fmt.Sprintf("http://%s:%d", r.info.Address, r.info.FilePort)
}

// ApiURL returns a URL to access the
// api server part of the server using the
// requesting IP address as a basis.
func (r *RenderData) ApiURL() string {
	return fmt.Sprintf("http://%s:%d", r.info.Address, r.info.ApiPort)
}

// Info returns a *models.Info structure
func (r *RenderData) Info() *models.Info {
	i := *r.info
	return &i
}

// of the specified URL as a string.
func (r *RenderData) ParseUrl(segment, rawURL string) (string, error) {
	parsedURL, err := url.Parse(rawURL)
	if err != nil {
		return "", err
	}
	switch segment {
	case "scheme":
		return parsedURL.Scheme, nil
	case "host":
		return parsedURL.Host, nil
	case "path":
		return parsedURL.Path, nil
	}
	return "", fmt.Errorf("No idea how to get URL part %s from %s", segment, rawURL)
}

// Param is a helper function for extracting a parameter from Machine.Params
func (r *RenderData) Param(key string) (interface{}, error) {
	if r.Machine != nil {
		v, ok := r.Machine.Params[key]
		if ok {
			return v, nil
		}
	}
	res := &models.Param{}
	rr := r.drpClient.Req().UrlFor("params", key)
	if derr := rr.Do(&res); derr != nil {
		return nil, nil
	}
	v, _ := res.DefaultValue()
	return v, nil
}

// ParamAsJSON will return the specified parameter as a JSON
// string or an error.
func (r *RenderData) ParamAsJSON(key string) (string, error) {
	v, err := r.Param(key)
	if err != nil {
		return "", err
	}
	buf := &bytes.Buffer{}
	enc := json.NewEncoder(buf)
	err = enc.Encode(v)
	return buf.String(), err
}

// ParamAsYAML will return the specified parameter as a YAML
// string or an error.
func (r *RenderData) ParamAsYAML(key string) (string, error) {
	v, err := r.Param(key)
	if err != nil {
		return "", err
	}
	b, e := yaml.Marshal(v)
	if e != nil {
		return "", e
	}
	return string(b), nil
}

// ParamExists is a helper function for determining the existence of a machine parameter.
func (r *RenderData) ParamExists(key string) bool {
	_, err := r.Param(key)
	return err == nil
}

// ParamExpand does templating on the contents of a parameter before returning it
// If the parameter is an array or an object, string elements will be expanded
// recursively.
func (r *RenderData) ParamExpand(key string) (interface{}, error) {
	sobj, err := r.Param(key)
	if err != nil {
		return nil, err
	}
	answer, err := r.ObjectExpand(sobj)
	if err != nil {
		return nil, fmt.Errorf("Error ParamExpand %s: %v", key, err)
	}
	return answer, nil
}

// ObjectExpand takes an arbitrary object and does an expand on it recursively.
// If the object does not understand expansion, it will be returned as is.
// If the object is an array or slice or map, the system will recurse on
// each element.
func (r *RenderData) ObjectExpand(ii interface{}) (interface{}, error) {
	switch val := ii.(type) {
	case string:
		return r.StringExpand(val)
	case []interface{}:
		answer := []interface{}{}
		for kk, jj := range val {
			nj, err := r.ObjectExpand(jj)
			if err != nil {
				return nil, fmt.Errorf("Error ObjectExpand: element %d failed to expand: %v", kk, err)
			}
			answer = append(answer, nj)
		}
		return answer, nil
	case map[string]interface{}:
		answer := map[string]interface{}{}
		for k, j := range val {
			nj, err := r.ObjectExpand(j)
			if err != nil {
				return nil, fmt.Errorf("Error ObjectExpand: key %s failed to expand: %v", k, err)
			}
			answer[k] = nj
		}
		return answer, nil
	}
	return ii, nil
}

// StringExpand does templating on the contents of a string value
func (r *RenderData) StringExpand(s string) (interface{}, error) {
	unmarshalJson := false
	if strings.HasPrefix(s, "jsonobject:") {
		unmarshalJson = true
		s = s[11:]
	}

	res := &bytes.Buffer{}
	tmpl, err := template.New("machine").Funcs(models.DrpSafeFuncMap()).Parse(s)
	if err != nil {
		return nil, fmt.Errorf("Error compiling string %s: %v", s, err)
	}
	tmpl = tmpl.Option("missingkey=error")
	if err := tmpl.Execute(res, r); err != nil {
		return nil, fmt.Errorf("Error rendering string %s: %v", s, err)
	}
	var answer interface{}
	if unmarshalJson {
		if jerr := json.Unmarshal(res.Bytes(), &answer); jerr != nil {
			return nil, fmt.Errorf("error rendering jsonobject override: %v", jerr)
		}
	} else {
		answer = res.String()
	}
	return answer, nil
}
