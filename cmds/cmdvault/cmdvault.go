package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml Version=$version"
//go:generate sh -c "drpcli contents document content.yaml > cmdvault.rst"

import (
	_ "embed"
	"fmt"
	"os"
	"time"

	utils2 "github.com/VictorLowther/jsonpatch2/utils"
	"gitlab.com/rackn/logger"
	v4 "gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

const (
	DefaultCacheTimeout = 5 * time.Minute
)

var (
	//go:embed content.yaml
	content string
	version = v4.RSVersion
	def     = models.PluginProvider{
		Name:          "cmdvault",
		Version:       version,
		AutoStart:     true,
		PluginVersion: 4,
		AvailableActions: []models.AvailableAction{
			{Command: "decrypt",
				Model: "machines",
				RequiredParams: []string{
					"decrypt/lookup-uri",
					"cmdvault/commands",
				},
				OptionalParams: []string{
					"cmdvault/cache-timeout",
					"cmdvault/commands",
				},
			},
			{Command: "clearMachineCache",
				Model: "machines",
			},
			{Command: "clearCache",
				Model: "system",
			},
		},
		Content: content,
	}
)

type Plugin struct {
	// cache represents the temporary cmdvault cache
	cache *utils.Cache

	// cacheTimeout represents the amount of time for which the value retrieved
	// from cmdvault would be cached in memory
	cacheTimeout time.Duration

	// commands contains the commands to run against
	commands map[string]string
}

// VaultOptions is a set of options that can be passed in as
// query parameters into the lookup URI
type VaultOptions struct {
	// format is the format in which the secret is stored
	// for example the lookup uri could be
	// cmdvault://command?param1=p1&format=<format>
	// this would mean the secret is stored as json and the plugin
	// would read it and convert it before returning the secret
	// default which nothing is passed is just `string`
	format string

	// command denotes the command to run
	// example: cmdvault://<command>?param1=<p1>
	command string

	// options to template on the command string
	// example: cmdvault://<command>?param1=<p1>
	// param1 is a key and p1 is its value
	options map[string]string
}

func (p *Plugin) Config(l logger.Logger, session *api.Client, config map[string]interface{}) *models.Error {
	var ok bool
	timeoutSeconds, ok := config["cmdvault/cache-timeout"].(int)
	if !ok {
		p.cacheTimeout = DefaultCacheTimeout
	} else {
		p.cacheTimeout = time.Duration(timeoutSeconds) * time.Second
	}

	cmds := map[string]string{}
	if rerr := models.Remarshal(config["cmdvault/commands"], &cmds); rerr != nil {
		err := &models.Error{Type: "plugin", Model: "plugins", Key: "cmdvault"}
		err.AddError(rerr)
		return err
	}
	p.commands = cmds

	// Set up Cache
	p.cache = utils.CreateCache(p.cacheTimeout, p.fetchSecretFromVault)

	return nil
}

func (p *Plugin) Action(l logger.Logger, ma *models.Action) (answer interface{}, err *models.Error) {
	switch ma.Command {
	case "decrypt":
		answer, err = p.decryptAction(l, ma.Model, ma.Params)
	case "clearMachineCache":
		err = p.clearMachineCache(l, ma.Model)
	case "clearCache":
		err = p.clearCache(l)
	default:
		err = &models.Error{Code: 404,
			Model:    "Plugin",
			Key:      "cmdvault",
			Type:     "rpc",
			Messages: []string{fmt.Sprintf("Unknown command: %s", ma.Command)}}
	}
	return
}

func (p *Plugin) decryptAction(l logger.Logger, model interface{}, params map[string]interface{}) (interface{}, *models.Error) {
	lookupUri := utils.GetParamOrString(params, "decrypt/lookup-uri", "")
	l.Infof("Decrypting cmdvault secret")

	// Get the machine details
	machine := &models.Machine{}
	machine.Fill()
	if err := utils2.Remarshal(model, &machine); err != nil {
		return "", utils.ConvertError(400, err)
	}

	// Check the cache
	secret, err := p.cache.Get(machine.Uuid.String(), lookupUri)
	if err != nil {
		return nil, utils.ConvertError(500, err)
	}

	return secret, nil
}

func (p *Plugin) clearMachineCache(l logger.Logger, model interface{}) *models.Error {
	l.Infof("Clearing machine cache")
	// Grab the machine
	machine := &models.Machine{}
	machine.Fill()
	if err := utils2.Remarshal(model, &machine); err != nil {
		return utils.ConvertError(400, err)
	}

	// clear all machine data from cache
	p.cache.ClearMachineCache(machine.UUID())

	return nil
}

func (p *Plugin) clearCache(l logger.Logger) *models.Error {
	l.Infof("Clearing system cache")

	// clear all machine data from cache
	p.cache.Empty()

	return nil
}

func main() {
	plugin.InitApp("cmdvault", "Retrieve secret from command-line", version, &def, &Plugin{})
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}
