package main

import (
	"bytes"
	"encoding/json"
	"html/template"
	"net/url"
	"os/exec"

	"gitlab.com/rackn/provision/v4/models"
)

func (p *Plugin) fetchSecretFromVault(lookupUri string, machineId string) (interface{}, *models.Error) {
	err := &models.Error{Type: "plugin", Model: "", Key: "cmdvault"}

	// Parse the lookup URI and get the scheme and host
	// example: "plugin-name://secret-key?path=path-to-secret"
	uri, verr := url.ParseRequestURI(lookupUri)
	if verr != nil {
		err.Code = 400
		err.Errorf("fetch secret: invalid lookupUri provided")
		return "", err
	}

	var vault VaultOptions
	vault.command = uri.Host
	qs := uri.Query()
	if qs["format"] != nil {
		vault.format = qs["format"][0]
	}
	vault.options = map[string]string{}
	for k, v := range qs {
		if k == "format" {
			continue
		}
		vault.options[k] = v[0]
	}

	command, ok := p.commands[vault.command]
	if !ok {
		err.Code = 400
		err.Errorf("fetch command: comnand %s not found", vault.command)
		return "", err
	}

	tmpl, terr := template.New("tmp").Funcs(models.DrpSafeFuncMap()).Parse(command)
	if terr != nil {
		err.Code = 400
		err.Errorf("template command: comnand %s error %v", vault.command, terr)
		return "", err
	}

	buf := &bytes.Buffer{}
	if rerr := tmpl.Execute(buf, vault.options); rerr != nil {
		err.Code = 400
		err.Errorf("render command: comnand %s error %v", vault.command, rerr)
		return "", err
	}

	bs, cerr := exec.Command("/usr/bin/env", "bash", "-c", buf.String()).CombinedOutput()
	if cerr != nil {
		err.Code = 400
		err.Errorf("run command: %s error %v", vault.command, cerr)
		return "", err
	}
	value := string(bs)

	var finalResult interface{}
	// If a format is specified, then we will also need convert data
	switch vault.format {
	case "json":
		verr = json.Unmarshal([]byte(value), &finalResult)
		if verr != nil {
			// There was an error converting the value to json, most like it is not valid json so just return
			return value, nil
		}
	default:
		finalResult = value
	}

	return finalResult, nil
}
