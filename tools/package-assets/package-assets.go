package main

import (
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"log"
	"os"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatalf("Need 2 args, not %d", len(os.Args))
	}
	if err := utils.Package(os.Args[1], os.Args[2]); err != nil {
		log.Fatalf("Error creating %s from %s: %v", os.Args[1], os.Args[2], err)
	}
}
