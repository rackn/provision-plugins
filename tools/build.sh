#!/usr/bin/env bash
set -e

# Work out the GO version we are working with:
GO_VERSION=$(go version | awk '{ print $3 }' | sed 's/go//')
WANTED_VER=(1 12)
if ! [[ "$GO_VERSION" =~ ([0-9]+)\.([0-9]+) ]]; then
    echo "Cannot figure out what version of Go is installed"
    exit 1
elif ! (( ${BASH_REMATCH[1]} > ${WANTED_VER[0]} || ${BASH_REMATCH[2]} >= ${WANTED_VER[1]} )); then
    echo "Go Version needs to be ${WANTED_VER[0]}.${WANTED_VER[1]} or higher: currently $GO_VERSION"
    exit -1
fi

export GO111MODULE=on
export CGO_ENABLED=0

mkdir -p tools/build
exepath="$PWD/tools/build"

parallel=${parallel:-"false"}
case $1 in
    --parallel|-p) parallel=true; BG='&'; shift 1 ;;
esac

if [[ $TRAVIS = true ]]; then
    # Sigh.  Work around some rate-limiting hoodoo, hopefully
    for i in 1 2 3 4 5; do
        go mod download && break
        sleep $i
    done
fi

for f in drpcli; do
    [[ -x $exepath/$f ]] && continue
    go build -o "$exepath/$f" tools/drpcli.go
done
[[ -x $exepath/go-bindata ]] || go build -o "$exepath/go-bindata" github.com/kevinburke/go-bindata/go-bindata
export PATH="$PWD/tools/build:$PATH"

# set our arch:os build pairs to compile for
builds="amd64:linux amd64:darwin arm64:linux arm64:darwin arm:7:linux ppc64le:linux"

# anything on command line will override our pairs listed above
[[ $* ]] && builds="$*"

build() {
    unset GOOS GOARCH
    [[ -n "$PLUG" ]] && echo "Build plugins for $PLUG:" || true
    (cd "$tool"; eval go generate || :)
    [[ -n "$PLUG" ]] && printf "  " || true
    for build in ${builds}; do
        [[ $tool = */kvm-test && ${build##*:} = darwin ]] && continue
        export GOOS="${build##*:}" GOARCH="${build%%:*}"
	printf ' %s:%s' "$GOOS" "$GOARCH"
	tools/build-one.sh --no-generate "$tool" $BG
    done
    PLUG=""
    echo
}

[[ "$parallel" == "true" ]] && printf "Starting background parallel builds for:\n " || true

for tool in cmds/*; do
    [[ -d $tool ]] || continue
    if [[ "$parallel" == "true" ]]
    then
	PLUG=$(basename $tool)
	printf " %s" "$PLUG"
	T=$(mktemp /tmp/build-bg-log-XXXXXX.txt)
	LOGS+=" $T"
	PLUG_LOG=">> $T"
    else
	PLUG=""
        printf 'Building %s for' "$tool"
    fi
    eval build $PLUG_LOG $BG
done
if [[ "$parallel" == "true" ]]
then
    echo
    wait
    cat $LOGS
    rm -f $LOGS
fi

if drpcli contents --help | grep -q " document-md "
then
    DOC_VERSION=""
    if [[ "$CI_COMMIT_BRANCH" != "" ]] ; then
      CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%%-*}
      CI_COMMIT_BRANCH=${CI_COMMIT_BRANCH%.*}
      DOC_VERSION=$CI_COMMIT_BRANCH
    fi
    if [[ "$CI_COMMIT_TAG" != "" ]] ; then
      CI_COMMIT_TAG=${CI_COMMIT_TAG%%-*}
      CI_COMMIT_TAG=${CI_COMMIT_TAG%.*}
      DOC_VERSION=$CI_COMMIT_TAG
    fi
    if [[ "$DOC_VERSION" == "v4" ]] ; then
      DOC_VERSION=tip
    fi
    if [[ "$VERSION" == "" ]] ; then
      DOC_VERSION=tip
    fi

    for file in cmds/*/content.yaml ; do
        drpcli contents document-md $file rackn-base-docs/$DOC_VERSION || :
    done
else
    echo "FATAL: Can't build docs,"
    echo "       '$(which drpcli)' missing 'contents document-md' sub command"
    echo "       Upgrade your 'drpcli' binary version"
    exit 1
fi

echo "To run tests, run: tools/test.sh"
