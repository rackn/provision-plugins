#!/usr/bin/env bash
. tools/version.sh >/dev/null
[[ $GOOS ]] || export GOOS=$(go env GOOS)
[[ $GOARCH ]] || export GOARCH=$(go env GOARCH)
[[ $GOARCH = arm ]] && export GOARM=7

if [[ "$1" == "--no-generate" ]] ; then
    shift
else
    (cd "$1"; eval go generate || :)
fi

binpath="$PWD/bin/$GOOS/${GOARCH}${GOARM+v${GOARM}}"
exepath="$PWD/bin/$(go version | awk '{ print $4 }')"
export PATH="$exepath:$PATH"
exename="${1##*/}"
[[ $GOOS = "windows" ]] && exename="${exename}.exe"
[[ -x $exepath/go-bindata ]] || GOOS= go build -o "$exepath/go-bindata" github.com/kevinburke/go-bindata/go-bindata
export PATH="$PWD/tools/build:$PATH"

mkdir -p "$binpath"
export GO111MODULE=on
export CGO_ENABLED=0
export VERFLAGS="-w \
          -X gitlab.com/rackn/provision-plugins/v4.RSMajorVersion=$MajorV \
          -X gitlab.com/rackn/provision-plugins/v4.RSMinorVersion=$MinorV \
          -X gitlab.com/rackn/provision-plugins/v4.RSPatchVersion=$PatchV \
          -X gitlab.com/rackn/provision-plugins/v4.RSExtra=$Extra \
          -X gitlab.com/rackn/provision-plugins/v4.BuildStamp=`date -u '+%Y-%m-%d_%I:%M:%S%p'` \
          -X gitlab.com/rackn/provision-plugins/v4.GitHash=$GITHASH"
set -e
cd "$1"
if [[ $TRAVIS = true ]]; then
    # Sigh.  Work around some rate-limiting hoodoo, hopefully
    for i in 1 2 3 4 5; do
        go mod download && break
        sleep $i
    done
fi
if [[ -d content ]] ; then
        rm -f content/._Version.meta
        export version=$(printf 'v%s.%s.%s%s' $MajorV $MinorV $PatchV $Extra)
fi
go build -trimpath -ldflags "$VERFLAGS" -o "$binpath/$exename"
