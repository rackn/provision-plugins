#!/usr/bin/env bash

set -eu
set -o pipefail

[[ -x $HOME/go/bin/govulncheck ]] || go install golang.org/x/vuln/cmd/govulncheck@latest
govulncheck ./... | tee /tmp/vuln.log
if ! grep -q 'No vulnerabilities found' /tmp/vuln.log; then
  rm  /tmp/vuln.log
  exit 1
fi
